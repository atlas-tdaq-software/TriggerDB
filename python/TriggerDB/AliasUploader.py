#!/usr/bin/env python3

from __future__ import annotations
from copy import deepcopy
from functools import total_ordering
import json
from pathlib import Path
import re
from typing import Any, Literal, Optional, TYPE_CHECKING

from TriggerDB.Logger import log
from TriggerDB.PrescaleSetUploader import PrescaleSetUploader
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBUploader import DBUploaderBase
@total_ordering
class AliasRow:
    def __init__(self, lmin: float, lmax: float, lbase: int):
        self.lmin: float = float(lmin)
        self.lmax: float = float(lmax)
        self.lbase: int = int(lbase)
        self.l1pskey: Optional[int] = None
        self.hltpskey: Optional[int] = None
        self.l1file: Optional[Path] = None
        self.hltfile: Optional[Path] = None

    @staticmethod
    def baseFromUnit(lumiunit:str) -> int:
        if (m:=re.match(r"e(\d\d)",lumiunit)) is None:
            raise RuntimeError(f"Lumi unit {lumiunit} can not be interpreted. It must be of form 'e24', 'e32', ..")
        return int(m.group(1))

    def __lt__(self, other:AliasRow):
        return self.lmin < other.lmin # Sort by low lumi range

    def __eq__(self, other):
        return self.lmin == other.lowlumi

    def __repr__(self):
        return f"AliasRow(lowlumi={self.lmin}, highlumi={self.lmax}, unit={self.lbase}, l1psk={self.l1pskey}, hltpsk={self.hltpskey})"

class Alias:
    def __init__(self, alias_type:str, alias_name:str, comment:str, bgk:int) -> None:
        self.id: int = 0
        self.type = alias_type
        self.name: str = alias_name
        self.tp_name: str = ""
        self.comment: str = comment
        self.smk: int = 0
        self.bgk: int = bgk
        self.isHidden: bool = False
        self.rows: list[AliasRow] = []
    def __repr__(self):
        rep = f"Alias(type={self.type}, name={self.name})"
        for row in self.rows:
            rep += f"\n  {row}"
        return rep

class AliasUploader(DBUploaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
        self.alias: Optional[Alias] = None

    @property
    def table(self) -> str:
        return 'PRESCALE_ALIAS'

    @property
    def prefix(self) -> str:
        return 'PSA'

    @property
    def fields(self) -> list[str]:
        return ["ID", "TYPE", "NAME", "COMMENT", "DEFAULT", 'TP_NAME', 'SMT_ID', 'BGK_ID', 'HIDDEN']

    @property
    def expected_filetype(self) -> str:
        return "alias"

    def _readAliasFile(self,alias_ps_file_dir: str) -> Optional[Alias]:

        def _get_file_from_row(row: dict, level:Literal['l1file','hltfile']):
            filename =row.get(level)
            if filename is None or filename == '':
                return None
            if filename not in ps_files: 
                log.error(f"Can not find file {row['l1file']}, which is specified in {alias_file}, row {rc+1}")
                raise RuntimeError("L1 prescale file not found")

        alias_dir_path = Path(alias_ps_file_dir)
        if not alias_dir_path.exists() or not alias_dir_path.is_dir():
            log.error(f"Location {alias_ps_file_dir} does not exist or is not a directory")
            return
        
        alias_file_pattern = re.compile(r"(Set|Alias)_.*\.json")
        def is_alias_file(file:Path) -> bool:
            if not alias_file_pattern.match(file.name):
                return False
            with file.open() as fp:
                content = json.load(fp)
            return content['filetype'] == 'alias'

        # Iterate through the directory
        log.debug(f"Scanning for alias information directory {alias_dir_path}")
        alias_files: list[Path] = [item for item in alias_dir_path.iterdir() if is_alias_file(item)]
        if len(alias_files) == 0:
            log.error(f"No alias file found in {alias_ps_file_dir}.")
            return
        if len(alias_files) > 1:
            log.error(f"More than one alias file found in {alias_ps_file_dir}")
            return
        
        ps_file_pattern: re.Pattern[str] = re.compile(r"(L1|HLT)(.*)?Prescale_.*\.json")
        ps_files: dict[str, Path] = {item.name:item for item in alias_dir_path.iterdir() if ps_file_pattern.match(item.name)}
        # read the file
        alias_file: Path = alias_files[0]
        with alias_file.open() as fp:
            adata = json.load(fp)

        # create an internal Alias object that holds the information
        alias = Alias(alias_type = adata['type'], alias_name = adata['name'],
                      comment=adata['comment'], bgk=adata.get('bgk',0))
        for rc, row in enumerate(adata['aliasrows']):
            if 'bgk' in row:
                alias.bgk = row['bgk']
            ar = AliasRow(lmin=row['lmin'], lmax=row['lmax'], lbase=AliasRow.baseFromUnit(row['lbase']))

            # if L1 prescale file name is specified, then it is required to exist
            l1fn = row.get('l1file')
            if l1fn == '':
                l1fn = None
            if l1fn is None:
                if int(row.get('l1psk',0))==0:
                    log.error(f"In {alias_file} row {rc+1}, neither L1 presacle file nor key are specified")
                    raise RuntimeError("Neither L1 prescale file nor key are specified")
                ar.l1pskey = row.get('l1psk')
            else:
                if l1fn not in ps_files: 
                    log.error(f"Can not find file {l1fn}, which is specified in {alias_file}, row {rc+1}")
                    raise RuntimeError("L1 prescale file not found")
                ar.l1file = ps_files[l1fn]

            hltfn = row.get('hltfile')
            if hltfn == '':
                hltfn = None
            if hltfn is None:
                if int(row.get('hltpsk',0))==0:
                    log.error(f"In {alias_file} row {rc+1}, neither HLT presacle file nor key are specified")
                    raise RuntimeError("Neither HLT prescale file nor key are specified")
                ar.hltpskey = row.get('hltpsk')
            else:
                if hltfn not in ps_files: 
                    log.error(f"Can not find file {hltfn}, which is specified in {alias_file}, row {rc+1}")
                    raise RuntimeError("HLT prescale file not found")
                ar.hltfile = ps_files[row['hltfile']]
            alias.rows.append(ar)
        # sort by low lumi boundary
        alias.rows = sorted(alias.rows)
        return alias

    def _recordExists(self, alias: Alias) -> bool:
        # first compare the alias to the entries in the alias table
        # using smk, name, type and tp_name
        query: str = (
            f"SELECT {self.prefix}_ID FROM {self.dbschema}.{self.table} WHERE "
            f"{self.prefix}_SMT_ID=:a_smk AND {self.prefix}_TYPE=:a_type AND "
            f"{self.prefix}_NAME=:a_name AND {self.prefix}_TP_NAME=:a_tpname "
            f"order by {self.prefix}_ID"
        )
        bindvars: dict[str, Any] = {
            "a_smk": alias.smk,
            "a_type": alias.type,
            "a_name": alias.name,
            "a_tpname": alias.tp_name,
        }
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            result: list[tuple[Any]] = cursor.fetchall()
        alias_ids = [r[0] for r in result]
        if len(alias_ids)==0:
            return False
        
        # second for the found matches compare the linked rows
        to_compare_with = [(row.l1pskey, row.hltpskey, row.lmin, row.lmax, row.lbase) for row in alias.rows]
        query: str = (
            f"SELECT PSR_L1PS_ID, PSR_HPS_ID, PSR_LMIN, PSR_LMAX, PSR_LBASE "
            f"FROM {self.dbschema}.PRESCALE_ALIAS_ROW WHERE PSR_PSA_ID=:psaid ORDER BY PSR_LMIN"
        )
        found_match = False
        with self.cursor() as cursor:
            for psa_id in alias_ids:
                bindvars: dict[str, Any] = { "psaid": psa_id }
                cursor.execute(query, **bindvars)
                result: list[tuple[Any]] = cursor.fetchall()
                if result == to_compare_with:
                    found_match = True
                    self._key = psa_id
                    self._is_duplicate = True
                    self._dbrecord = self.getTableRecord(psa_id)
                    self._dbrecord["rows"] = []
                    for arow in result:
                        self._dbrecord["rows"] += [
                            dict(zip(["L1PS_ID", "HPS_ID", "LMIN", "LMAX", "LBASE"], arow))
                        ]
                    log.info(f"The alias is already contained in the database with index {self._key}. Using existing entry.")
                    break

        return found_match

    def _defaultExists(self, alias: Alias) -> bool:
        query: str = (
            f"SELECT {self.prefix}_ID FROM {self.dbschema}.{self.table} WHERE "
            f"{self.prefix}_SMT_ID=:a_smk AND {self.prefix}_TYPE=:a_type "
            f"ORDER BY {self.prefix}_ID"
        )
        bindvars: dict[str, Any] = {
            "a_smk": alias.smk,
            "a_type": alias.type
        }
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            result: list[tuple[Any]] = cursor.fetchall()
        alias_ids = [r[0] for r in result]
        if len(result)>0:
            log.warning(f"Default for this type {alias.type} already exists, therefor this upload will not become the default")
        return len(result)>0

    def _uploadAlias(self):
        if self.alias is None:
            return
    
        default_exists: bool = self._defaultExists(self.alias)


        bindvars: dict[str, Any] = {
            "idx": self.getLastTableIndex() + 1,
            "a_type": self.alias.type,
            "a_name": self.alias.name,
            "a_default": 0 if default_exists else 1,
            "a_comment": self.alias.comment,
            "a_smk": self.alias.smk,
            "a_user": self.getUser(),
            "a_hidden": self.alias.isHidden,
            "a_tpname": self.alias.tp_name,
            "a_bgk": self.alias.bgk
        }
        fields: list[str] = [
            "ID", "TYPE", "NAME", "DEFAULT", "COMMENT", "SMT_ID", "USERNAME", 
            "MODIFIED_TIME", "HIDDEN", "TP_NAME", "BGK_ID"
            ]
        field_str: str =  ', '.join([f"{self.prefix}_{f}" for f in fields])
        values: str = (
            f":idx, :a_type, :a_name, :a_default, :a_comment, :a_smk, :a_user, "
            f"CURRENT_TIMESTAMP, :a_hidden, :a_tpname, :a_bgk"
        )
        query:str = f"INSERT INTO {self.dbschema}.{self.table}({field_str}) VALUES({values})"
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get and return the index of the new entry that has been created
        self._key, self._dbrecord = self.getLastTableRecord()
        self.alias.id = self._key
        log.info(f"Inserted {self.alias.type} alias '{self.alias.name}' into the table with index {self._key}, linked to SMK {self.alias.smk}.")

        # update upload record

    def _uploadAliasRows(self):
        if self.alias is None:
            return
        current_last_index: int = self.getLastTableIndex(table='PRESCALE_ALIAS_ROW', prefix='PSR')
        fields: list[str] = [
            "ID", "PSA_ID", "L1PS_ID", "HPS_ID", "LMIN", "LMAX", "COMMENT", 
            "USERNAME", "MODIFIED_TIME", "LBASE"
            ]
        field_str: str =  ', '.join([f"PSR_{f}" for f in fields])
        values: str = (
            f":r_idx, :r_psaid, :r_l1psk, :r_hltpsk, :r_lmin, :r_lmax, :r_comment, "
            f":r_user, CURRENT_TIMESTAMP, :r_lbase"
        )
        query:str = f"INSERT INTO {self.dbschema}.PRESCALE_ALIAS_ROW({field_str}) VALUES({values})"
        with self.cursor() as cursor:
            self._dbrecord["rows"] = []
            for rc, row in enumerate(self.alias.rows):
                bindvars: dict[str, Any] = {
                    "r_idx": current_last_index + 1 + rc,
                    "r_psaid": self.alias.id,
                    "r_l1psk": row.l1pskey,
                    "r_hltpsk": row.hltpskey,
                    "r_lmin": row.lmin,
                    "r_lmax": row.lmax,
                    "r_comment": self.alias.comment,
                    "r_user": self.getUser(),
                    "r_lbase": row.lbase
                }
                cursor.execute(query, **bindvars)
                cursor.connection.commit()
                # add to the dbrecord
                self._dbrecord["rows"] += [{
                    "L1PS_ID": row.l1pskey,
                    "HPS_ID": row.hltpskey,
                    "LMIN": row.lmin,
                    "LMAX": row.lmax,
                    "LBASE": row.lbase
                }]

    def _upload_prescales(self, alias: Alias, upload_record: list[dict]) -> None:
        # upload the prescales
        with PrescaleSetUploader(existingConnection=self.dbconnection) as uploader:
            for row in alias.rows:
                uploader.upload(l1ps=row.l1file.as_posix() if row.l1file is not None else None, 
                                hltps=row.hltfile.as_posix() if row.hltfile is not None else None,
                                smk=alias.smk, comment=alias.comment)
                if row.l1file is not None:
                    if uploader.l1_psk == 0:
                        log.error(f"L1 prescale upload failed for {row.l1file}")
                        raise RuntimeError("L1 prescale upload failure")
                    row.l1pskey = uploader.l1_psk
                if row.hltfile is not None:
                    if uploader.hlt_psk == 0:
                        log.error(f"HLT prescale upload failed for {row.hltfile}")
                        raise RuntimeError("HLT prescale upload failure")
                    row.hltpskey = uploader.hlt_psk
                uploader.createUploadRecord(write_file=False)
                l1_record = deepcopy(uploader.upload_record["l1"])
                hlt_record = deepcopy(uploader.upload_record["hlt"])
                upload_record.append({ "l1": l1_record, "hlt": hlt_record})

    def upload(self, alias_ps_file_dir: str, smk: int, tp_name: Optional[str]=None, comment: Optional[str]=None) -> None:
        self.reset()
        # load the alias file into an alias object
        self.alias = self._readAliasFile(alias_ps_file_dir)
        if self.alias is None:
            return
        # set smk, and optionally comment and triggerpanel name
        self.alias.smk = smk
        if comment is not None and len(comment)>0:
            self.alias.comment = comment
        if tp_name is not None and len(tp_name)>0:
            self.alias.tp_name = tp_name
        else:
            self.alias.tp_name = self.alias.name

        # upload prescales (will update the alias object with the l1 and hlt psks)
        prescale_upload_record = []
        self._upload_prescales(self.alias, prescale_upload_record)

        # check if alias record exists om the database
        if not self._recordExists(self.alias):
            # upload alias table
            self._uploadAlias()

            # upload rows
            self._uploadAliasRows()

        self.createUploadRecord(prescale_upload_record)

    def createUploadRecord(self, prescale_upload_record: list[dict]) -> None:
        if self.alias is None:
            return
        self._upload_record = {
            "key": self._dbrecord['ID'],
            "type": self._dbrecord["TYPE"],
            "name": self._dbrecord["NAME"],
            "tpname": self._dbrecord["TP_NAME"],
            "history": "existed" if self._is_duplicate else "new",
            "comment": self.alias.comment,
            "exectime": self.exec_time_str,
            "username": self.getUser(),
            "dbrecord": self._dbrecord
        }
        self._upload_record["prescales"] = prescale_upload_record

    def print_record(self) -> None:
        if self._dbrecord is None:
            print("No dbrecord")
            return
        print(f"Summary of alias {self._dbrecord['TYPE']}: {self._dbrecord['NAME']} (ID={self._dbrecord['ID']}, entry {'existed' if self._is_duplicate else 'is new'})")
        print(f"Default for type {self._dbrecord['TYPE']}: {'yes' if self._dbrecord['DEFAULT']=='1' else 'no'}")
        print(f"Comment: {self._dbrecord['COMMENT']}")
        print(f"SMK: {self._dbrecord['SMT_ID']}")
        print(f"BGK: {self._dbrecord['BGK_ID']}")
        print(f"Hidden: {self._dbrecord['HIDDEN']}")
        print(f"Name on TriggerPanel: {self._dbrecord['TP_NAME']}")
        print("Lumi range        | L1 Key | HLT Key")
        print("------------------------------------")
        for row in self._dbrecord['rows']:
            print(f"{row['LMIN']:5} - {row['LMAX']:5} e{row['LBASE']} | {row['L1PS_ID']:6} | {row['HPS_ID']:6}")

