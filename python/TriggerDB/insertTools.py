from datetime import date, datetime
from pathlib import Path
from typing import Any
import json

class DateTimeEncoder(json.JSONEncoder):
    # override the default method
    def default(self, o: Any) -> Any:
        if isinstance(o, datetime):
            return o.strftime("%Y-%m-%dT%H:%M:%S")
        if isinstance(o, date):
            return o.isoformat()
        if isinstance(o, Path):
            return str(o)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, o)


