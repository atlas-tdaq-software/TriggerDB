#!/usr/bin/env python

from __future__ import annotations
from typing import Any, Optional, TYPE_CHECKING, Union
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

import json
import os

from TriggerDB.MenuDownloader import L1MenuDownloader
from TriggerDB.DBDownloader import DBDownloaderBase
from TriggerDB.L1CTHelper import FT, SMKInfo, getAllKeysInfo, getFileTableStructure
from TriggerDB.Logger import log


class L1CTDownloader(DBDownloaderBase):
    """
    Download a bunch group set file from a trigger configuration databae.
    """

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Args:
            dbalias (str): the trigger db connection alias
            existingConnection (DBConnection | None): if one wants to use an existing DBConnection
        """
        super().__init__(dbalias, existingConnection=existingConnection)
        self.l1menu_downloader = L1MenuDownloader(existingConnection=self.dbconnection)

    @property
    def prefix(self) -> str:
        raise NotImplementedError("L1CTDownloader covers multiple tables")

    @property
    def table(self) -> str:
        raise NotImplementedError("L1CTDownloader covers multiple tables")

    def download(self, key: int, outputdir:Optional[str] = None) -> dict[Union[int,str],Any]:
        # get the table layout
        self.conf: dict[FT | str, dict[str, Any]] = getFileTableStructure()

        # create the top directory where to save the downloads
        if outputdir is None:
            downloadDir: str = f"l1files_{key}"
        else: 
            downloadDir = outputdir
        os.makedirs(downloadDir, exist_ok=True)

        self.downloadL1Menu(key, downloadDir)
        self.downloadL1CTP(key, downloadDir)
        return {}

    def downloadL1Menu(self, smk: int, downloadDir: str) -> None:
        l1menu_id, _, _ = self.getLinkedIDsFromSMK(smk=smk)
        if l1menu_id is None:
            log.error(f"SMK {smk} has no L1 menu attached")
            return
        self.l1menu_downloader.download(key=l1menu_id)
        targetDir: str = f'{downloadDir}/{self.conf["menu"]["directory"]}'
        os.makedirs(targetDir, exist_ok=True)
        self.l1menu_downloader.writeFile(f"{targetDir}/L1Menu.json")


    def printL1KeyInfo(self, smk=0):
        with self.cursor() as cursor:
            allInfo: dict[int, SMKInfo] = getAllKeysInfo(cursor, smk=smk)
        for smk, info in sorted(allInfo.items()):
            print(info)

    def downloadL1CTP(self, smk: int, downloadDir: str) -> None:
        with self.cursor() as cursor:
            keyInfo: SMKInfo = getAllKeysInfo(cursor, smk)[smk]
            for fset in FT:
                ftc: dict[str, Any] = self.conf[fset]
                targetDir: str = f"{downloadDir}/{ftc['directory']}"
                os.makedirs(targetDir, exist_ok=True)
                filesFieldsMap = list( ftc["files"].items() )
                fieldList: list[str] = [ftc["prefix"]+'.'+f[1] for f in filesFieldsMap]
                tableId: int|None = keyInfo.getID(fset)
                log.info(f"Downloading files from table {ftc['table']} with entry ID {tableId}")
                query: str = (
                    f"SELECT {', '.join(fieldList)} FROM {ftc['table']} {ftc['prefix']}"
                    f" WHERE {ftc['prefix']}.{ftc['prefix']}_ID={tableId}"
                )
                log.debug(query)
                try:
                    cursor.execute(query)
                except Exception as exc:
                    log.error(f"Failed query: {query}\nFailure: {exc}")
                    continue

                res = cursor.fetchall()[0]
                for idx in range(len(filesFieldsMap)):
                    configblob = res[idx]
                    outfn = f"{targetDir}/{filesFieldsMap[idx][0]}"
                    with open(outfn, "w") as fh:
                        if type(configblob) == dict:
                            json.dump(configblob, fh, indent=4)
                        else:
                            fh.write(configblob.read().decode("utf-8")) # oracledb.LOB
                    log.info(f"    wrote {outfn}")


if __name__=="__main__":
    smk = 3347
    with L1CTDownloader("TRIGGERDB_RUN3") as downloader:
        downloader.printL1KeyInfo(smk)
        downloader.download(key=smk)

