#!/usr/bin/env python
"""
This module contains code to return the mD5 hash of a string, dict, or 
files (txt, json) using the MD5 (Message Digest 5) hashing algorithm.
"""
import hashlib
import json
from pathlib import Path

def getHashFromString(string:str) -> str:
    """
    Compute the hash of a string
    """
    hashObject = hashlib.md5(string.encode("utf-8"))
    return hashObject.hexdigest()

def getHashFromStringForUpdate(string:str) -> bytes:
    """
    @see getHashFromString
    (this can be used to update an external hash object)
    """
    hashObject = hashlib.md5(string.encode("utf-8"))
    return hashObject.digest()

def getDictHash(fileDict:dict) -> str:
    """
    Computes the hash of a python dictionary using the MD5 hashing algorithm. 
    
    Before computing the hash, the dictionary is ordered and read into string 
    to ensure that changes in whitespace or key ordering result in the same hash,
    and different hashes really mean changes in the contents of the key-value pairs.
    """
    # Convert the dictionary to an string, with the keys sorted
    fileOrderedString = json.dumps(
        fileDict, sort_keys=True, indent=4, separators=(",", ": ")
    )
    # Compute and get the hash using the MD5 algorithm
    return getHashFromString(fileOrderedString)

def getDictHashForUpdate(fileDict) -> bytes:
    """
    @see getDictHash
    (this can be used to update an external hash object)
    """
    # Convert the dictionary to an string, with the keys sorted
    fileOrderedString = json.dumps(
        fileDict, sort_keys=True, indent=4, separators=(",", ": ")
    )
    return getHashFromStringForUpdate(fileOrderedString)

def getFileHash(filepath:str|Path) -> str:
    """
    Computes the hash of a JSON or text file using the MD5 algorithm. 
    """
    if str(filepath).lower().endswith(".json"):
        with open(filepath) as jsonfile:
            # Get dictionary from jsonfile
            fileDict = json.load(jsonfile)
            # Get hash from dictionary
            fileHash: str = getDictHash(fileDict)
    else:
        with open(filepath) as txtfile:
            string = txtfile.read()
            fileHash = getHashFromString(string)
    return fileHash

def getFileHashForUpdate(filepath:str|Path) -> bytes:
    """
    @see getFileHash
    this can be used to update an external hash object
    """
    if str(filepath).lower().endswith(".json"):
        with open(filepath) as jsonfile:
            fileDict: dict = json.load(jsonfile)
            fileHash: bytes = getDictHashForUpdate(fileDict)
    else:
        with open(filepath) as txtfile:
            string: str = txtfile.read()
            fileHash = getHashFromStringForUpdate(string)
    return fileHash
