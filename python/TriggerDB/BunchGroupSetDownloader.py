#!/usr/bin/env python

from __future__ import annotations
from typing import Optional, TYPE_CHECKING
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBDownloader import DBDownloaderBase

class BunchGroupSetDownloader(DBDownloaderBase):
    """
    Download a bunch group set file from a trigger configuration databae.
    """

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Args:
            dbalias (str): the trigger db connection alias
            existingConnection (DBConnection | None): if one wants to use an existing DBConnection
        """
        super().__init__(dbalias, existingConnection=existingConnection)

    @property
    def prefix(self) -> str:
        return 'L1BGS'

    @property
    def table(self) -> str:
        return 'L1_BUNCH_GROUP_SET'

if __name__=="__main__":
    bgsk = 2861
    with BunchGroupSetDownloader("TRIGGERDB_RUN3") as downloader:
        downloader.download(key=bgsk)
        downloader.writeFile(f"BunchGroupSet_{bgsk}.json")

