#!/usr/bin/env python3

from __future__ import annotations
from typing import Any, Optional, TYPE_CHECKING, Union
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBDownloader import DBDownloaderBase


class L1MenuDownloader(DBDownloaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)

    # table name
    @property
    def table(self) -> str:
        return 'L1_MENU'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'L1TM'

    def getPrescaleKeys(self, menuid: int) -> list[tuple[int,str,str]]:
        """
        Get the list of L1 prescale keys, names, and comments connected to a given L1 menu.

        Parameters:
            menuid (int): The menu id for which prescale keys are to be read.

        Returns:
            list[int,str,str]: The list of L1 prescale keys, with name and comment
        """
        query: str = f"SELECT L1PS_ID, L1PS_NAME, L1PS_COMMENT FROM {self.dbschema}.L1_PRESCALE_SET WHERE L1PS_L1_MENU_ID={menuid}"
        # Execute query and fetch result
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        # Matching prescale keys found
        return list(result)

class HLTMenuDownloader(DBDownloaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
    
    # table name
    @property
    def table(self) -> str:
        return 'HLT_MENU'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'HTM'

    def getPrescaleKeys(self, menuid: int) -> list[tuple[int,str,str]]:
        """
        Get the list of HLT prescale keys, names, and comments connected to a given HLT menu.

        Parameters:
            menuid (int): The menu id for which prescale keys are to be read.

        Returns:
            list[int,str,str]: The list of HLT prescale keys, with name and comment
        """
        query: str = f"SELECT HPS_ID, HPS_NAME, HPS_COMMENT FROM {self.dbschema}.HLT_PRESCALE_SET WHERE HPS_HLT_MENU_ID={menuid}"
        # Execute query and fetch result
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        return list(result)

class HLTJODownloader(DBDownloaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
    
    # table name
    @property
    def table(self) -> str:
        return 'HLT_JOBOPTIONS'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'HJO'


class MenuDownloader(DBDownloaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
        self.l1menu_downloader = L1MenuDownloader(existingConnection=self.dbconnection)
        self.hltmenu_downloader = HLTMenuDownloader(existingConnection=self.dbconnection)
        self.joboptions_downloader = HLTJODownloader(existingConnection=self.dbconnection)

    # table name
    @property
    def table(self) -> str:
        return 'SUPER_MASTER_TABLE'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'SMT'

    def download(self, key: int) -> dict[Union[int,str],Any]:
        l1menu_id, hltmenu_id, jo_id = self.getLinkedIDsFromSMK(key)
        if l1menu_id is not None:
            self.l1menu_downloader.download(key=l1menu_id)
        if hltmenu_id is not None:
            self.hltmenu_downloader.download(key=hltmenu_id)
        if jo_id is not None:
            self.joboptions_downloader.download(key=jo_id)
        return {}


if __name__=="__main__":
    smk = 3347
    with MenuDownloader("TRIGGERDB_RUN3") as downloader:
        downloader.download(key=smk)
        downloader.l1menu_downloader.writeFile(f"L1Menu_{smk}.json")
        downloader.hltmenu_downloader.writeFile(f"HLTMenu_{smk}.json")
        downloader.joboptions_downloader.writeFile(f"HLTJO_{smk}.json")
