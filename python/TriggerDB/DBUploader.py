#!/usr/bin/env python3

from __future__ import annotations
from pathlib import Path
from typing import Any, Optional, TypeVar, Union
import oracledb

from abc import ABC, abstractmethod
import re
from datetime import datetime
import io
import json
import os


from TriggerDB.Logger import log
from TriggerDB.DBConnection import DBConnection
from TriggerDB.insertTools import DateTimeEncoder

JsonData = TypeVar('JsonData', bound=dict[str, Any])

class DBTableCommunicator(ABC):
    """ Abstract class for communication with a database table
    """
    # table name
    @property
    @abstractmethod
    def table(self) -> str: ...

    # common prefix for all fields
    @property
    @abstractmethod
    def prefix(self) -> str: ...

    # fields of interest in the table without prefix
    @property
    @abstractmethod
    def fields(self) -> list[str]: ...

class DBUploader(ABC):
    @property
    @abstractmethod
    def expected_filetype(self) -> str:...

class DBDownloader(ABC):
    pass

class DBUploaderBase(DBTableCommunicator, DBUploader):

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Input
        -----
        dbalias: str the alias of the trigger DB connection
        existingConnection (oracledb.Connection): The existing connection if the class
        instance is to be called from within a with statement for another uploader class
        (defaults to None).
        """
        self._own_connection: bool = existingConnection is None
        if existingConnection is None:
            self.dbconnection: DBConnection = DBConnection(dbalias, readwrite=True)
        else:
            # use existing connection if provided
            if not existingConnection.readwrite:
                raise RuntimeError("DB uploads require a read-write connection")
            self.dbconnection = existingConnection

        # internal and upload data
        self._name: str = "unset"
        self._comment: str = ""
        self._upload_record: dict[str,Any] = {}
        self._exec_time: Optional[datetime] = None
        self._filename: Optional[str] = None
        self._is_duplicate: bool = False
        self._dbrecord: dict[str,Any] = {}
        self._key: int = 0

    def __enter__(self):
        return self
    
    def __exit__(self, a, b, traceback) -> None:
        if self._own_connection:
            self.dbconnection.close()

    def reset(self):
        self._name = "unset"
        self._comment = ""
        self._upload_record = {}
        self._exec_time = None
        self._filename = None
        self._is_duplicate = False
        self._dbrecord = {}
        self._key = 0

    def cursor(self) -> oracledb.Cursor:
        return self.dbconnection.cursor()

    @property
    def dbschema(self) -> str:
        return self.dbconnection.dbschema

    @property
    def exec_time(self) -> datetime:
        if self._exec_time is None:
            self._exec_time = datetime.now()
        return self._exec_time

    @property
    def exec_time_str(self) -> str:
        return self.exec_time.strftime("%Y-%m-%dT%H:%M:%S")

    @property
    def upload_record(self) -> dict[str,Any]:
        return self._upload_record

    @property
    def key(self) -> int:
        return self._key

    @property
    def comment(self) -> str:
        return self._comment

    @property
    def name(self) -> str:
        return self._name

    def parseComment(self, comment: str, max_length: int = 0) -> str:
        now = datetime.now()
        comment = re.sub("(?i)%u%", os.getenv("USER",""), comment)
        comment = re.sub("(?i)%d%", now.strftime("%a %d %b %Y"), comment)
        comment = re.sub("(?i)%t%", now.strftime("%T"), comment)
        comment = re.sub("(?i)%n%", self.name, comment)
        comment = re.sub("(?i)%fn%", self._filename if self._filename is not None else self.name, comment)
        if max_length>0:
            comment = comment[:max_length]
        return comment

    def getBlobFromFile(self, file: str|Path) -> oracledb.DB_TYPE_BLOB:
        """
        Turn the text content of a file into an ORACLE DB BLOB (binary large object). More see getBlobFromString

        Parameters:
            filename (str): the filename

        Returns:
            blob (oracledb.BLOB): The BLOB object.
        """
        with open(file, "rb") as fh:
            return self.getBlobFromString(fh.read())

    def getBlobFromString(self, text_string: Union[str, bytes]) -> oracledb.DB_TYPE_BLOB:
        """
        Turn the text string into an ORACLE DB BLOB (binary large object).

        Parameters:
            byteString (bytes): the text that will be converted

        Returns:
            blob (oracledb.BLOB): the BLOB object.
        """
        byteString: bytes = b""
        if type(text_string) is bytes:
            byteString = text_string
        elif type(text_string) is str:
            byteString = bytes(text_string, 'utf-8')
        mem_file = io.BytesIO(byteString)
        mem_file.seek(0, os.SEEK_END)
        file_size = mem_file.tell()
        blob: oracledb.DB_TYPE_BLOB = self.cursor().var(oracledb.BLOB, file_size)
        blob.setvalue(0, mem_file.getvalue())
        return blob

    def checkJsonContent(self, *, expected_filetype: str, file: str | Path = "", textString: str = "") -> bool:
        """check text (file or string) is in json format and contains expected filetype flag

        Args:
            expected_filetype (str): expected content type
            filename (str, optional): name of file to check if given. Defaults to "".
            textString (str, optional): text string to check if given. Defaults to "".

        Returns:
            bool: if input is json format of expected content
        """
        if file != "":
            with open(file) as fh:
                try:
                    content = json.load(fh)
                except ValueError as err:
                    log.error(f"The provided file {file} can not be interpreted as json file.")
                    log.error(err)
                    return False

            filetype: str = content["filetype"] if 'filetype' in content else 'missing'
            if filetype != expected_filetype:
                log.error(f"The provided file {file} has filetype '{filetype}', but should have filetype '{expected_filetype}'.")
                return False
        elif textString != "":
            try:
                content = json.loads(textString)
            except ValueError as err:
                log.error(f"The provided string '{textString[:20]}...' can not be interpreted as json file.")
                log.error(err)
                return False

            filetype = content["filetype"] if 'filetype' in content else 'missing'
            if filetype != expected_filetype:
                log.error(f"The provided file {file} has filetype '{filetype}', but should have filetype '{expected_filetype}'.")
                return False
        return True

    def getFiletype(self, fn: str) -> str:
        """
        Get the content type of a trigger configuration json file.

        Parameters:
            fn (str): The filename of the json file.

        Returns:
            str: The content type of the json file.
        """
        with open(fn) as fp:
            data = json.load(fp)
            try:
                filetype = data["filetype"]
            except KeyError:
                filetype = 'missing'
        return filetype

    def getUser(self) -> str:
        user: str = "unknown"
        # get the user from the environment
        if 'USER' in os.environ:
            user = os.environ["USER"]
        elif 'TDAQ_APPLICATION_OBJECT_ID' in os.environ:
            # Will be used by the AutoPrescaler
            user = os.environ["TDAQ_APPLICATION_OBJECT_ID"]
        return user

    def isValidJson(self, fn: str) -> bool:
        """
        Checks if a file is a valid json-formatted file.

        Parameters:
            fn (str): the filename of the json file.

        Returns:
            bool: True if file is valid JSON.
        """
        # If invalid JSON, try clause will fail
        try:
            with open(fn) as fp:
                json.load(fp)
        except ValueError:
            return False
        return True

    def isValidSMK(self, smk: int) -> bool:
        """
        Check if a given SMK specifies a valid entry in the super-master table.

        Parameters:
            smk (int): The SMK to be checked.

        Returns:
            bool: True if valid SMK.
        """
        query: str = f"SELECT SMT_ID FROM {self.dbschema}.SUPER_MASTER_TABLE WHERE SMT_ID = {smk}"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        if len(result) == 0:
            # No entry found that matches SMK
            isValidSMK = False
        else:
            # Entry found
            isValidSMK = True
        return isValidSMK

    def isValidIndexInTable(self, index) -> bool:
        """
        Check if a given index specifies a valid entry in a given DB table.

        Args:
            index (int): The table index to be checked.

        Returns:
            bool: True if the index exists in the table.
        """
        # Define SQL query to get entry matching provided index
        query: str = f"SELECT {self.prefix}_ID FROM {self.dbschema}.{self.table} WHERE {self.prefix}_ID = {index} "
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        if len(result) == 0:
            # No entry found that matches index
            isValidIndex = False
        else:
            # Entry found
            isValidIndex = True
        return isValidIndex

    def getTableRecord(self, idx: int) -> dict[str,Any]:
        """Get the record from the DB table for the given idx.

        Returns:
            dict[str, Any]: the record in the table
        """
        fields_with_pref: list[str] = [f'{self.prefix}_{f}' for f in self.fields]
        query: str = f"SELECT {', '.join(fields_with_pref)} FROM {self.dbschema}.{self.table} WHERE {self.prefix}_ID = :id"
        with self.cursor() as cursor:
            cursor.execute(query, id = idx)
            row = cursor.fetchone()
        if row is None:
            return {}
        return dict(zip(self.fields,row))

    def getLastTableIndex(self, prefix = None, table = None) -> int:
        """Get the latest index in the table.

        Returns:
            int: The latest index in the table.
        """
        if prefix is None:
            prefix = self.prefix
        if table is None:
            table = self.table

        # Define SQL query to get index
        query = f"SELECT MAX({prefix}_ID) FROM {table}"     
        # Execute query and fetch result
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        lastIndex = result[0][0]
        # Set lastIndex to 0 if no index found
        if lastIndex is None:
            lastIndex = 0
        return lastIndex

    def getLastTableRecord(self) -> tuple[int, dict[str,Any]]:
        """Get the latest index and record of the DB table.

        Returns:
            tuple[int, dict[str, Any]]: 
                the latest index in the table, 
                the last record in the table
        """
        fields_with_pref: list[str] = [f'{self.prefix}_{f}' for f in self.fields]
        query: str = (
            f"SELECT {', '.join(fields_with_pref)} FROM {self.table} WHERE {self.prefix}_ID = "
            f"(SELECT MAX({self.prefix}_ID) FROM {self.table})"
        )
        with self.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchone()
        lastIndex: int = 0
        lastRecord: dict[str,Any] = {}
        if row is not None:
            lastRecord = dict(zip(self.fields,row))
            lastIndex: int = lastRecord['ID']
        return lastIndex, lastRecord

    def getLastVersionOfName(self, name: str) -> int:
        """
        Get the latest version number for a given name from the DB table.
        If the name does not exist in the table, returns 0

        Parameters:
            name (str): The name of the entry        
        Returns:
            int: The last version of this name
        """
        # Define SQL query to get latest version
        query: str = (
            f"SELECT MAX({self.prefix}_VERSION) FROM {self.dbschema}.{self.table} where {self.prefix}_NAME = '{name}'"
        )
        with self.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchone()
        if row[0] is None:
            return 0 # return 0 if the name wasn't found
        return row[0]

    def getMenuIndex(self, smk: int, level: str) -> int:
        """Get the L1 or HLT menu table ID for a given SMK. 
        
        If the SMK doesn't exist returns -1. If no L1 or HLT menu is linked, returns -2.

        Args:
            smk (int): The SMK to be checked.
            level (str): the level (should be L1 or HLT)
        Returns:
            int: The menu table index.
        """
        query: str = f"SELECT smt.SMT_{level}_MENU_ID FROM {self.dbschema}.SUPER_MASTER_TABLE smt WHERE smt.SMT_ID = {smk}"
        with self.cursor() as cursor:
            cursor.execute(query)
            result: list[tuple[Any]] = cursor.fetchall()
        if len(result) == 0:
            # SMK does not exist
            return -1
        menu_id = result[0][0]
        if menu_id is None:
            # no menu attached to the  SMK
            return -2
        return int(menu_id)

    def getName(self, blob: oracledb.BLOB) -> str: # type: ignore
        """Extract the name from the configuration object (json-blob)

        Args:
            blob (oracledb.BLOB): the trigger configuration (json object string as BLOB)

        Returns:
            str: the configuration name
        """
        jstr = json.loads(blob.getvalue().read())
        return jstr["name"]

    def getComment(self, key: int) -> str:
        """
        Get the comment associated to a given index 'key'.

        Parameters:
            key (int): The index key for which to get the comment
        Returns:
            The comment associated to the key
        """
        # Define SQL query to get comment from a BGSK
        query = f"SELECT {self.prefix}_COMMENT FROM {self.dbschema}.{self.table} WHERE {self.prefix}_ID={key}"
        # Execute query and fetch result
        with self.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchone()
        if row is None:
            print(f"key {key} does not exist, when accessing comment table {self.table}")
            return ""
        return row[0]

    def getIDsFromSMK(self, smk: int) -> tuple[Optional[int], Optional[int], Optional[int]]:
        """
        Get the L1 menu, HLT menu, and HLT joboptions keys from the SMK.

        Parameters:
            smk (int): The super-master key for which files are to be extracted.

        Returns:
            tuple of
            int | None: L1 menu table entry id, or None, if none is attached
            int | None: HLT menu table entry id, or None, if none is attached
            int | None: Job option table entry id, or None, if none is attached
        """
        # Get L1 menu, HLT menu, and HLT joboptions keys from a SMK
        fields: list[str] = ["SMT_NAME", "SMT_VERSION", "SMT_L1_MENU_ID", "SMT_HLT_MENU_ID", "SMT_HLT_JOBOPTIONS_ID"]
        query: str = (
            f"SELECT {', '.join(fields)} FROM {self.dbschema}.{self.table} WHERE SMT_ID={smk}"
        )
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()

        if len(result)==0:
            raise RuntimeError(f"The provided SMK {smk} does not exist in the database {self.dbconnection.dbalias}")

        menuName, menuVersion, l1menuId, hltmenuId, joId = result[0]

        if l1menuId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no L1 menu attached.")
        if hltmenuId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no HLT menu attached.")
        if joId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no job options attached.")

        return l1menuId, hltmenuId, joId

    def getSchemaVersion(self) -> int:
        with self.cursor() as cursor:
            query: str = f"SELECT ts_tag, ts_modified_time FROM {self.dbschema}.trigger_schema ORDER BY ts_modified_time"
            cursor.execute(query)
            res = cursor.fetchall()
            if len(res) == 0: # trigger schema does not exist
                raise RuntimeError("ERROR: no trigger schema found")

        schemaString = str(res[-1][0]) # get the latest entry
        schemaVersion = -1
        if (m := re.match(r".*?(\d*)$",schemaString)) is not None:
            schemaVersion = int(m.groups()[0])
        log.info(f"Trigger schema version is {schemaVersion} (extracted from {schemaString})")
        return schemaVersion

    def writeUploadRecordFile(self, base:str):
        outfn: str = f"upload_record_{base}_{self.exec_time_str}.json"
        with open(outfn, "w") as fp:
            json.dump(self._upload_record, fp, indent=4, cls=DateTimeEncoder)
        log.info(f"Created {outfn}")
