#!/usr/bin/env python3

from __future__ import annotations
from typing import Union, Any, Optional, TYPE_CHECKING, cast

from TriggerDB.BunchGroupSetDownloader import BunchGroupSetDownloader
from TriggerDB.PrescaleSetDownloader import L1PrescaleSetDownloader, HLTPrescaleSetDownloader
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBDownloader import DBDownloaderBase

class AliasDownloader(DBDownloaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
        self._smk: int = 0
        self._alias_description: dict[Union[int,str], Any] = {}
        self._l1_prescales: dict[int,tuple[str,str]] = {}
        self._hlt_prescales: dict[int,tuple[str,str]] = {}
        self._download_prescales: bool = False
        self._l1ps_downloader: Optional[L1PrescaleSetDownloader] = None
        self._hltps_downloader: Optional[HLTPrescaleSetDownloader] = None
        self._bgs_downloader: Optional[BunchGroupSetDownloader] = None
        self._bginfo: dict[int, int] = {} # number of bunches in the bunchgroup set bgsk

    # table name
    @property
    def table(self) -> str:
        return 'PRESCALE_ALIAS'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'PSA'

    @property
    def l1ps_downloader(self) -> L1PrescaleSetDownloader:
        if self._l1ps_downloader is None:
            self._l1ps_downloader = L1PrescaleSetDownloader(existingConnection=self.dbconnection)
        return self._l1ps_downloader

    @property
    def hltps_downloader(self) -> HLTPrescaleSetDownloader:
        if self._hltps_downloader is None:
            self._hltps_downloader = HLTPrescaleSetDownloader(existingConnection=self.dbconnection)
        return self._hltps_downloader

    @property
    def bgs_downloader(self) -> BunchGroupSetDownloader:
        if self._bgs_downloader is None:
            self._bgs_downloader = BunchGroupSetDownloader(existingConnection=self.dbconnection)
        return self._bgs_downloader

    def getAliasKeys(self, smk: int) -> list[int]:
        """
        Get the list of entries in the Prescale Alias table for a given SMK.

        Parameters:
            smk (int): The smk for which aliases are to be read.

        Returns:
            list[int]: The list of prescale alias keys
        """
        query: str = f"SELECT {self.prefix}_ID FROM {self.dbschema}.{self.table} WHERE {self.prefix}_SMT_ID=:smk"
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            result = cursor.fetchall()
        return [r[0] for r in result]

    def download(self, key: int) -> dict[Union[int,str],Any]:
        """download alias table entry specified by key

        Args:
            key (int): the ID alias table entry

        Returns:
            str: json file string
        """

        schema_version: int = self.getSchemaVersion()

        alias_fields: list[str] = ["ID", "NAME", "TYPE", "COMMENT", "DEFAULT"]
        if schema_version >= 8:
            alias_fields += ['TP_NAME', 'BGK_ID']
        selection: str = ', '.join([f"{self.prefix}_{f}" for f in alias_fields])
        query: str = f"SELECT {selection} FROM {self.dbschema}.{self.table} WHERE {self.prefix}_ID=:psak"
        with self.cursor() as cursor:
            cursor.execute(query, psak=key)
            result = cursor.fetchone()
        resdict = dict(zip(alias_fields, result))

        tp_name: str = ""
        bgs_id: int = 0
        if schema_version >= 8:
            tp_name = resdict['TP_NAME']
            bgs_id = resdict['BGK_ID']

        alias_data: dict[str,Any] = {
            'filetype': 'alias',
            'type': resdict['TYPE'],
            'name': resdict['NAME'],
            'comment': resdict['COMMENT'],
            'bgk': bgs_id,
            'aliasrows': [],
            # 'default': resdict['DEFAULT'],
            # 'tpname': tp_name,
        }

        # fill the alias rows from the table PRESCALE_ALIAS_ROW
        row_fields: list[str] = ["ID", "L1PS_ID", "HPS_ID", "LMIN", "LMAX"]
        if schema_version >= 8:
            row_fields += ['LBASE']
        selection = ', '.join([f"PSR_{f}" for f in row_fields])
        query: str = f"SELECT {selection} FROM {self.dbschema}.PRESCALE_ALIAS_ROW WHERE PSR_PSA_ID=:psak"
        with self.cursor() as cursor:
            cursor.execute(query, psak=key)
            result = cursor.fetchall()
        rows: list[dict[str,Any]] = sorted([dict(zip(row_fields,r)) for r in result], key=lambda x: x['LMIN'])
        for row in rows:
            lb_base: int = row['LBASE'] if schema_version>=8 else 32
            alias_data['aliasrows'] += [{
                'lmin': row['LMIN'],
                'lmax': row['LMAX'],
                'lbase': f"e{lb_base}",
                'l1psk': row['L1PS_ID'],
                'hltpsk': row['HPS_ID']
            }]

        self._alias_description[key] = alias_data
        return self._alias_description
    
    def downloadForSMK(self, *, smk: int, download_prescales: bool = False) -> None:
        self._smk = smk
        self._download_prescales = download_prescales
        alias_keys: list[int] = self.getAliasKeys(smk=smk)
        self._alias_description = {
            'smk': smk,
            'aliasIDs': alias_keys
        }
        for key in alias_keys:
            self.download(key = key)

        if download_prescales:
            for key in alias_keys:
                alias_data = self._alias_description[key]
                nbunches: int = self.getFilledBunches(alias_data['bgk'])
                nbstr: str = f"_{nbunches}b" if nbunches>0 else ""
                for row in alias_data['aliasrows']:
                    name_ext: str = f"{row['lmin']}_{row['lmax']}{row['lbase']}{nbstr}"
                    self.l1ps_downloader.download(row['l1psk'])
                    data: str = cast(str,self.l1ps_downloader._downloaded_data)
                    fn: str = f"L1Prescale_{name_ext}.json"
                    row['l1file'] = fn
                    self._l1_prescales[row['l1psk']] = (data, fn)
                    self.hltps_downloader.download(row['hltpsk'])
                    data: str = cast(str,self.hltps_downloader._downloaded_data)
                    fn = f"HLTPrescale_{name_ext}.json"
                    row['hltfile'] = fn
                    self._hlt_prescales[row['hltpsk']] = (data, fn)

    def getFilledBunches(self, bgk: int) -> int:
        if bgk<=0:
            return -1
        if bgk not in self._bginfo:
            data = self.bgs_downloader.download(bgk)
            self._bginfo[bgk] = sum([tr['length'] for tr in data["bunchGroups"]["BGRP1"]["bcids"]])
        return self._bginfo[bgk]

    def writeFiles(self, outdir: str = "") -> None:
        if outdir != "" and not outdir.endswith('/'):
            outdir += "/"
        for aliasID in self._alias_description['aliasIDs']:
            alias_data = self._alias_description[aliasID]
            # write the alias file
            alias_name: str = alias_data['name'].replace(' ','_')
            alias_type: str = alias_data['type']
            outfn: str = f"Alias_{alias_type}_name{alias_name}_ak{aliasID}.json"
            self._downloaded_data = alias_data
            self.writeFile(f"{outdir}{outfn}")
            # write the prescale files
            if self._download_prescales:
                ps_outdir: str = outdir # f"{outdir}pss_{alias_type}_name{alias_name}_ak{aliasID}/"
                for row in alias_data['aliasrows']:
                    data, fn = self._l1_prescales[row['l1psk']]
                    self.l1ps_downloader.writeFile(f"{ps_outdir}{fn}", data=data)
                    data, fn = self._hlt_prescales[row['hltpsk']]
                    self.hltps_downloader.writeFile(f"{ps_outdir}{fn}", data=data)
