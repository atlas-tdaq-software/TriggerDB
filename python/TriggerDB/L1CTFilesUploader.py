#!/usr/bin/env python3

from __future__ import annotations
from typing import Any, Generator, Optional

import hashlib
from itertools import filterfalse
import os
from pathlib import Path
import re
import time
from enum import Enum, auto
import json
import oracledb

from TriggerDB.hashHelper import getFileHash, getFileHashForUpdate
from TriggerDB.L1CTHelper import FT, SMKInfo, getAllKeysInfo, getFileTableStructure
from TriggerDB.DBUploader import DBUploaderBase
from TriggerDB.Logger import log

class LevelEnum(Enum):
    L1 = auto()
    HLT = auto()
    def __str__(self) -> str:
        return self.name

class L1CTFilesUploader(DBUploaderBase):
    
    def __init__(self, dbalias: str, **kwArgs) -> None:
        super().__init__(dbalias=dbalias)
        self.forceUpdate: dict[FT, bool] = { ft : False for ft in FT }
        if "force" in kwArgs:
            for f in kwArgs["force"]:
                self.setForceUpdate(f)
        self.skipTargetCheck: bool = "skipTargetCheck" in kwArgs and kwArgs["skipTargetCheck"]

    @property
    def table(self) -> str:
        raise NotImplementedError()

    @property
    def prefix(self) -> str:
        raise NotImplementedError()

    @property
    def fields(self) -> list[str]: ...

    @property
    def expected_filetype(self) -> str:...

    def setForceUpdate(self, which, on=True):
        if which == "all":
            for fset in FT:
                self.forceUpdate[fset] = on
            return
        tr = {
            'ctp' : FT.CTP,
            'smx' : FT.SMX,
            'muctpi' : FT.MUC,
            'tmc' : FT.TMC
        }
        if which not in tr:
            raise RuntimeError("Can't set forceUpdate for %s. Must be one of %s " % (which, "' ".join(tr)))
        self.forceUpdate[tr[which]] = on

    def checkMenuHashMatches(self, smk:int, topdir:str) -> None:
        """Compares the L1 menu file (under @param topdir) with the L1 menu in the database by hash value

        Args:
            smk (int): SMK with the L1 menu to be compared
            topdir (str): top directory under which to find the L1 menu file to be compared

        Raises:
            RuntimeError: when no L1 menu is attached to the SMK
            RuntimeError: _description_
            RuntimeError: _description_
            RuntimeError: when the L1 menus from file and database are different
        """
        # hash from the database for SMK
        query: str = (
            f"SELECT l1tm.l1tm_hash FROM super_master_table sm, l1_menu l1tm "
            "WHERE sm.smt_l1_menu_id = l1tm.l1tm_id AND sm.smt_id=:smk"
        )
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            res = cursor.fetchall()
        if len(res) == 0: # L1 menu not found for given SMK
            raise RuntimeError(f"ERROR: no l1menu found for SMK {smk}")
        menuHashDBStr=res[0][0]
        log.info(f"Menu hash from DB: {menuHashDBStr}")

        # hash from L1Menu file
        directory = os.path.join(topdir, self.conf["menu"]["directory"])
        entries = [fn for fn in os.listdir(directory) if re.match(r"(L1Menu).*(\.json)$",fn)]
        if len(entries)==0:
            raise RuntimeError(f"Failed to validate that the files match the target menu. Did not find an L1Menu file in {directory}. Please provide it or skip the menu check with --skipTargetCheck.")
        if len(entries)>1:
            raise RuntimeError(f"Failed to validate that the files match the target menu. Found multiple L1Menu files in {directory}. Please provide only one or skip the menu check with --skipTargetCheck.")

        fullFN: str = os.path.join(directory, entries[0])
        menuHashFileStr: str = getFileHash(filepath=fullFN)
        log.info(f"Menu hash of file: {menuHashFileStr}")
        if menuHashDBStr != menuHashFileStr:
            raise RuntimeError(
                f"Failed to validate that the files match the target menu. L1Menu hash of SMK {smk} is {menuHashDBStr} and of file {fullFN} is {menuHashFileStr}. "
                "Please investige this difference or if you want to proceed, skip the menu check with --skipTargetCheck."
                )
        log.info(f"Hashes match, will continue with uploading L1CT and MUCTPI files to this smk")

    def upload(self, topdir:str, smk:int) -> bool:

        if not self.isValidSMK(smk):
            log.error(f"ERROR: SMK {smk} does not exist")
            return False

        self.conf = getFileTableStructure()

        # compare the hash of L1 menu in the database and in the file
        # raises RuntimeError if they don't match
        if not self.skipTargetCheck:
            self.checkMenuHashMatches(smk, topdir)
        log.info(f"Going to upload files from {topdir} to SMK {smk}")

        with self.cursor() as cursor:
            smkinfo: SMKInfo = getAllKeysInfo(cursor, smk)[smk]
            log.info(f"To SMK {smk} the following entries are attached:")
            log.info(f"   CTP Files   : {smkinfo.getID(FT.CTP)} (#{smkinfo.tableHash[FT.CTP]})")
            log.info(f"   CTP SMX     : {smkinfo.getID(FT.SMX)} (#{smkinfo.tableHash[FT.SMX]})")
            log.info(f"   MUCTPI File : {smkinfo.getID(FT.MUC)} (#{smkinfo.tableHash[FT.MUC]})")
            log.info(f"   TMC File    : {smkinfo.getID(FT.TMC)} (#{smkinfo.tableHash[FT.TMC]})")

        # check if all needed files are present under the topdir directory for the different tables
        allFilesInfo: dict[FT, tuple[bool, str]] = self.checkIfAllFilesArePresent(topdir)

        # some required adjustments of the TMC json file (remove user, creation time, ...)
        self.checkAndFixTMCFile(topdir)

        # to upload to a table these requirements must be met
        # - all files must exist and 
        # - there must be no entry attached or the update must be forced
        doUpdate: dict[FT, bool] = {}
        updateMsg: dict[FT, str] = {}
        for fset in FT:
            allFilesPresent, filesCombinedHash = allFilesInfo[fset]
            if allFilesPresent:
                if smkinfo.getID(fset) is None: # no entry exists yet
                    doUpdate[fset] = True
                    updateMsg[fset] = "YES"
                elif self.forceUpdate[fset]: # entry exists, but overwrite is forced
                    doUpdate[fset] = True
                    updateMsg[fset] = "YES (entry exists but update is forced)"
                else: # entry exists, will not be overwritten
                    doUpdate[fset] = False
                    if filesCombinedHash == smkinfo.tableHash[fset]:
                        updateMsg[fset] = "NO (entry exists and matches the new files)"
                    else:
                        updateMsg[fset] = f"NO (entry exists, but the new files have different hash #{filesCombinedHash}). Use --force ... to update."
            else: # missing files, no update
                doUpdate[fset] = False
                updateMsg[fset] += "NO (missing files)"
            
        log.info(f"Plan the following update of SMK {smk}:")
        log.info(f"   CTP Files   : {updateMsg[FT.CTP]}")
        log.info(f"   CTP SMX     : {updateMsg[FT.SMX]}")
        log.info(f"   MUCTPI File : {updateMsg[FT.MUC]}")
        log.info(f"   TMC File    : {updateMsg[FT.TMC]}")

        if not any(doUpdate.values()):
            log.info("No update of SMK will be done")
            return True

        # upload to the 4 individual tables
        success: bool = True
        newIndexes: dict[FT, Optional[int]] = { fset:None for fset in FT }
        for fset in FT:
            if not doUpdate[fset]:
                continue
            log.debug(f"Uploading {fset.name} table {self.conf[fset]['table']}")
            uploadIndex: Optional[int] = self.uploadFilesToSingleTable(fset, topdir, all_file_hash=allFilesInfo[fset][1])
            if uploadIndex is None:
                log.error(f"ERROR: could not upload {fset.name} to table {self.conf[fset]['table']}")
                success = False
            else:
                newIndexes[fset] = uploadIndex

        if not success:
            log.error("Planned upload is incomplete")
            return False

        # update the SMK with the information
        log.info(f"Will update SMK {smk} / L1Menu with the following links:")
        if doUpdate[FT.CTP]:
            log.info(f"   CTP Files   : {newIndexes[FT.CTP]}")
        if doUpdate[FT.SMX]:
            log.info(f"   CTP SMX     : {newIndexes[FT.SMX]}")
        if doUpdate[FT.MUC]:
            log.info(f"   MUCTPI File : {newIndexes[FT.MUC]}")
        if doUpdate[FT.TMC]:
            log.info(f"   TMC File    : {newIndexes[FT.TMC]}")


        # connect to the table entries to the L1 Menu
        success = self.connectToL1Menu(smk=smk, indexes=newIndexes)
        if not success:
            return False

        # check result
        with self.cursor() as cursor:
            updatedSmkinfo: SMKInfo = getAllKeysInfo(cursor, smk)[smk]
            log.info(f"Final check: to SMK {smk} / L1Menu the following entries are attached:")
            log.info(f"   CTP Files   : {updatedSmkinfo.getID(FT.CTP)}")
            log.info(f"   CTP SMX     : {updatedSmkinfo.getID(FT.SMX)}")
            log.info(f"   MUCTPI File : {updatedSmkinfo.getID(FT.MUC)}")
            log.info(f"   TMC File    : {updatedSmkinfo.getID(FT.TMC)}")

        # create a summary upload record
        self._upload_record = {
            "smk": smk,
            "time" : time.asctime(time.gmtime(time.time())),
            "user" : os.getlogin(),
            "pwd" : os.getcwd()
        }
        for fset in FT:
            config = self.conf[fset]
            self._upload_record[config["table"]] = {
                "index" : updatedSmkinfo.getID(fset),
                "action" : "uploaded" if doUpdate[fset] else "existed"
            }
            if doUpdate[fset]:
                self._upload_record[config["table"]].update({
                    "location" : f"{topdir}/{config['directory']}",
                    "files" : [fn for fn in config["files"]]
                })
        return success

    def checkAndFixTMCFile(self, topdir:str) -> None:
        """
        Remove creation call, date, and user from the TMC file
        """
        conf_tmc: dict[str, Any] = self.conf[FT.TMC]
        tmcFile: Path = Path(topdir).joinpath(conf_tmc["directory"], list(conf_tmc["files"])[0])
        if not tmcFile.exists():
            return
        with tmcFile.open() as fh:
            tmc = json.load(fh)
            for k in ["time", "call", "user"]:
                tmc["TMC"].pop(k,None)
        with tmcFile.open('w') as fh:
            json.dump(tmc, fh, indent=4)
        
    def connectToL1Menu(self, smk:int, indexes: dict[FT,Optional[int]]) -> bool:
        """
        Connect single table indexes to L1 menu table
        """
        whereClause: str = (
            f"SELECT SMT.SMT_L1_MENU_ID FROM SUPER_MASTER_TABLE SMT WHERE SMT.SMT_ID=:smk"
        )
        x: dict[FT, str] = {
            FT.CTP : "L1TM_CTP_FILES_ID",
            FT.SMX : "L1TM_CTP_SMX_ID",
            FT.MUC : "L1TM_MUCTPI_FILES_ID",
            FT.TMC : "L1TM_TMC_SIGNALS_ID",
        }
        updateClause = ", ".join(
            [ "%s = %i" % (x[fset], idx) for (fset,idx) in indexes.items() if idx is not None ]
        )
        updateQuery = (
            f"UPDATE L1_MENU SET {updateClause} WHERE L1TM_ID = ({whereClause})"
        )
        with self.cursor() as cursor:
            try:
                cursor.execute(updateQuery, dict(smk=smk))
                cursor.connection.commit()
                return True
            except Exception as e:
                log.error("Failed query: %s\nFailure: %s" % (updateQuery, str(e)))
                return False

    def checkIfAllFilesArePresent(self, topdir: str) -> dict[FT, tuple[bool,str]]:
        allFilesPresent: dict[FT,tuple[bool,str]] = {}
        for fset in FT:
            conf = self.conf[fset]
            directory = os.path.join(topdir, conf["directory"])
            allFiles = (os.path.join(directory, fn) for fn in conf["files"])
            missingFiles = list(filterfalse(os.path.exists,allFiles))

            if len(missingFiles)>0: # missing files
                log.warning(f"Not all files available to upload to table {conf['table']}. Missing are {missingFiles}.")
                allFilesPresent[fset] = (False,"")
                continue

            log.info(f"Found all required files for table {conf['table']}.")
            combinedHashObj = hashlib.md5() # hash value across all files
            for fn in conf["files"]:
                log.info(f"    adding file {fn}")
                _hash: bytes = getFileHashForUpdate(filepath=os.path.join(directory,fn))
                combinedHashObj.update(_hash) # update hash
            combined_hash: str = combinedHashObj.hexdigest()
            allFilesPresent[fset] = (True, combined_hash)

        return allFilesPresent

    def uploadFilesToSingleTable(self, fset: FT, topdir: str, all_file_hash: str) -> Optional[int]:
        """Upload all files to a one of the tables L1_CTP_FILES, L1_CTP_SMX, L1_MUCTPI_FILES, L1_TMC_SIGNALS

        Args:
            fset (FT): the table enum
            topdir (str): the top directory under which the files are found
            hash (str): hash of all the files to be uploaded

        Returns:
            Optional[int]: Index of the uploaded entry or None if the upload was not successful
        """

        # get the configuration driving the upload of the table
        conf: dict[str, Any] = self.conf[fset]
        table = conf['table']
        prefix = conf['prefix']

        # check if all files listed in the configuration exist in the directory
        directory: str = os.path.join(topdir, conf["directory"])
        allFiles: Generator[str, None, None] = (os.path.join(directory, fn) for fn in conf["files"])
        allFilesExist: bool = all(map(os.path.exists, allFiles))
        if not allFilesExist:
            log.error(f"Not all files available to upload to table {table}")
            return None

        # check if entry with this hash already exists and return the ID if it does
        query: str = f"SELECT {prefix}_ID FROM {table} WHERE {prefix}_HASH='{all_file_hash}'"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        if len(result) > 0:
            # Duplicate found, get index of existing entry
            id: int = result[0][0]
            log.info(f"The content already exists in table {table} with index {id}. Will use this.")
            return id

        # load the files as BLOBs into a dictionary
        content: dict[str,oracledb.DB_TYPE_BLOB] = {}
        for fn, tableColumn in conf["files"].items():
            content[tableColumn] = self.getBlobFromFile(f"{directory}/{fn}")

        # find the next available index in the table
        newIdx: int = self.getLastTableIndex(prefix = prefix, table = table) + 1

        # assemble insert query and exexute it
        fields: list[str] = list(content)
        fieldStr: str = f"{prefix}_ID, {prefix}_HASH, {prefix}_MODIFIED_TIME, " + ", ".join(fields)
        valueStr: str= ":id, :hash, CURRENT_TIMESTAMP, " + ", ".join([f":{f}" for f in fields])
        query: str = f"INSERT INTO {table}({fieldStr}) VALUES ({valueStr})"
        bindvars = [newIdx, all_file_hash] + [content[f] for f in fields]
        with self.cursor() as cursor:
            cursor.execute(query, bindvars)
            cursor.connection.commit()
        log.info(f"Uploaded file(s) to table {table} with index {newIdx}")
        return newIdx


if __name__=="__main__":
    topdir="download_TRIGGERDB_RUN3_3347"
    smk=3004
    with L1CTFilesUploader("TRIGGERDBDEV2_I8") as uploader:
        uploader.upload(topdir=topdir, smk=smk)
