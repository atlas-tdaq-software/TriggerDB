#!/usr/bin/env python

from __future__ import annotations
from abc import abstractmethod
from pathlib import Path
from typing import Any, Literal, Optional, Union, TYPE_CHECKING

from TriggerDB.hashHelper import getFileHash

if TYPE_CHECKING:
    import oracledb

from enum import Enum, auto
import json
import os


from TriggerDB.insertTools import DateTimeEncoder
from TriggerDB.MonGroupUploader import MonGroupUploader
from TriggerDB.DBUploader import DBUploaderBase
from TriggerDB.DBConnection import DBConnection
from TriggerDB.Logger import log

class MenuEnum(Enum):
    L1 = auto()
    HLT = auto()
    JO = auto()
    def __str__(self) -> str:
        return self.name


class MenuUploaderBase(DBUploaderBase):

    def __init__(self, dbalias: str, menu: Literal[MenuEnum.L1, MenuEnum.HLT, MenuEnum.JO], 
                 existingConnection: Optional[DBConnection] = None) -> None:

        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
        self._menu: MenuEnum = menu
        self._is_duplicate: bool = False
        self._menu_name: str

    @abstractmethod
    def upload(self,menu:str|Path) -> Optional[int]: ...

    @property
    def is_duplicate(self) -> bool:
        return self._is_duplicate

    @property
    def pss_name(self) -> str:
        return self._menu_name

    def recordExists(self, pss_hash: str, menu_idx: int) -> bool:
        query = f"SELECT {self.prefix}_ID FROM {self.dbschema}.{self.table} WHERE {self.prefix}_HASH='{pss_hash}' AND {self.prefix}_{self._menu}_MENU_ID={menu_idx}"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        if len(result) == 0:
            # no existing entry found for the menu and with this hash
            return False
        # entry found for the menu and with this hash
        existingIdx: int = int(result[0][0])
        log.info(f"The prescale set is already contained in the database with index {existingIdx}. Using existing entry.")
        # set duplicate flag and index of existing entry and return
        self._is_duplicate = True
        self._key = existingIdx
        return True

class L1MenuUploader(MenuUploaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, menu=MenuEnum.L1, existingConnection=existingConnection)

    @property
    def expected_filetype(self) -> str:
        return "l1menu"

    # table name
    @property
    def table(self) -> str:
        return 'L1_MENU'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'L1TM'

    # fields of interest in the table without prefix
    @property
    def fields(self) -> list[str]:
        return ['ID', 'NAME', 'VERSION', 'CTP_FILES_ID', 'CTP_SMX_ID', 'MUCTPI_FILES_ID', 'TMC_SIGNALS_ID']

    def upload(self, menu:str|Path) -> Optional[int]:
        """
        Upload the L1 menu and return the index. If the menu is found to be a
        duplicate, return the existing menu index instead.

        Parameters:
            l1menu (str/None): The filename of the L1 menu JSON file,
            or None, indicating no L1 menu JSON file is to be uploaded.

        Returns:
            lastIdx/existingIdx (int): The index of the uploaded L1 menu/existing
            L1 menu.
        """
        if menu is None:
            return None
        # Set variables required for SQL query
        blob: oracledb.BLOB = self.getBlobFromFile(menu) # type: ignore
        newIdx = self.getLastTableIndex() + 1
        self._name = self.getName(blob)
        newVersion = self.getLastVersionOfName(self.name) + 1
        menuHash: str = getFileHash(menu)
        ctpFilesId = None
        ctpSmxId = None
        # Define SQL query to check for duplicate hash values
        query = f"SELECT L1TM_ID FROM {self.dbschema}.L1_MENU WHERE L1TM_HASH='{menuHash}'"
        # Execute query and fetch result
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        # Duplicate found
        if len(result) > 0:
            # Get index of existing entry
            existingIdx = result[0][0]
            log.info(f"The L1 menu {self.name} is already contained in the database with index {existingIdx}. Using existing entry.")
            # Set IsDuplicate flag and return index of existing entry
            self._is_duplicate = True
            self._key = existingIdx
            self._dbrecord = self.getTableRecord(idx=self._key)
            return

        fields = "L1TM_ID, L1TM_NAME, L1TM_DATA, L1TM_HASH, L1TM_VERSION, L1TM_CTP_FILES_ID, L1TM_CTP_SMX_ID"
        values: str = f"{newIdx}, :name, :blob, '{menuHash}', {newVersion}, :ctpFilesId, :ctpSmxId"
        bindvars: dict[str,Any] = {
            'name': self.name,
            'blob': blob,
            'ctpFilesId': ctpFilesId,
            'ctpSmxId': ctpSmxId
        }
        query: str = f"INSERT INTO {self.dbschema}.L1_MENU({fields}) VALUES({values})"
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get and return the index of the new entry that has been created
        self._key, self._dbrecord = self.getLastTableRecord()
        log.info(f"Inserted L1 menu {self.name} (version {newVersion}) into the L1 table with index {self._key}.")

class HLTMenuUploader(MenuUploaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, menu=MenuEnum.HLT, existingConnection=existingConnection)

    @property
    def expected_filetype(self) -> str:
        return "hltmenu"

    # table name
    @property
    def table(self) -> str:
        return 'HLT_MENU'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'HTM'

    # fields of interest in the table without prefix
    @property
    def fields(self) -> list[str]:
        return ['ID', 'NAME', 'VERSION']

    def upload(self, menu:str|Path) -> None:
        """
        Upload the HLT menu and return the index. If the menu is found to be a
        duplicate, return the existing menu index instead.

        Parameters:
            hltmenu (str/None): The filename of the HLT menu JSON file,
            or None, indicating no HLT menu JSON file is to be uploaded.

        Returns:
            lastIdx/existingIdx (int): The index of the uploaded HLT menu/existing
            HLT menu.
        """
        blob = self.getBlobFromFile(menu)
        newIdx: int = self.getLastTableIndex() + 1
        self._name = self.getName(blob)
        newVersion: int = self.getLastVersionOfName(self.name) + 1
        menuHash: str = getFileHash(menu)

        # check for duplicates using hash values
        query: str = f"SELECT HTM_ID FROM {self.dbschema}.HLT_MENU WHERE HTM_HASH='{menuHash}'"
        # Execute query and fetch results
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()

        if len(result) > 0: # duplicate found
            # Get index of existing entry
            self._key = result[0][0]
            self._is_duplicate = True
            log.info(f"The HLT menu {self.name} is already contained in the database with index {self._key}. Using existing entry.")
            self._dbrecord = self.getTableRecord(idx=self._key)
            return
        
        # Define values to be inserted, using bind variables to protect against SQL injection
        values: str = f"{newIdx}, :name, :blob, '{menuHash}', {newVersion}"
        bindvars: dict[str,Any] = {'name':self.name, 'blob':blob}

        # Insert entry in HLT menu table
        query = (f"INSERT INTO {self.dbschema}.HLT_MENU(HTM_ID, HTM_NAME, HTM_DATA, HTM_HASH, HTM_VERSION) VALUES({values})")
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get and return the index of the new entry that has been created
        self._key = self.getLastTableIndex()
        log.info(f"Inserted HLT menu {self.name} (version {newVersion}) into the HLT table with index {self._key}.")

class HLTJobOptionUploader(MenuUploaderBase):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, menu=MenuEnum.JO, existingConnection=existingConnection)

    @property
    def expected_filetype(self) -> str:
        return "joboptions"

    # table name
    @property
    def table(self) -> str:
        return 'HLT_JOBOPTIONS'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'HJO'

    # fields of interest in the table without prefix
    @property
    def fields(self) -> list[str]:
        return ['ID']

    def upload(self, menu:str|Path):
        """
        Upload the HLT joboptions and return the index. If the menu is found to
        be a duplicate, return the existing menu index instead.

        Parameters:
            hltjo (str/None): The filename of the HLT joboptions JSON file,
            or None, indicating no HLT joboptions JSON file is to be uploaded.

        Returns:
            lastIdx/existingIdx (int): The index of the uploaded HLT joboptions/existing
            HLT joboptions.
        """
        if menu is None:
            return None
        # Set variables required for SQL query
        blob = self.getBlobFromFile(menu)
        newIdx = self.getLastTableIndex() + 1
        menuHash: str = getFileHash(menu)
        string_menuHash = "'%s'" % menuHash
        # Define SQL query to check for duplicate hash values
        query: str = f"SELECT HJO_ID FROM {self.dbschema}.HLT_JOBOPTIONS WHERE HJO_HASH={string_menuHash}"
        # Execute query and fetch results
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        if len(result) > 0: # duplicate found
            self._key = result[0][0]
            self._is_duplicate = True
            log.info(f"The HLT job options config is already contained in the database with index {self._key}. Using existing entry.")
            self._dbrecord = self.getTableRecord(idx=self._key)
            return

        # Insert entry in HLT joboptions table
        values: str = f"{newIdx}, :blob, '{menuHash}'"
        bindvars = (blob,)
        query = (f"INSERT INTO {self.dbschema}.HLT_JOBOPTIONS(HJO_ID, HJO_DATA, HJO_HASH) VALUES({values})")
        with self.cursor() as cursor:
            cursor.execute(query, bindvars)
            cursor.connection.commit()

        # Get and return the index of the new entry that has been created
        self._key = self.getLastTableIndex()
        log.info(f"Inserted HLT joboptions into the HLT JO table with index {self._key}.")

class SuperMasterUploader(DBUploaderBase):

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)

    @property
    def expected_filetype(self) -> str:
        raise NotImplementedError("SuperMasterUploader does not have a filetype associated")

    # table name
    @property
    def table(self) -> str:
        return 'SUPER_MASTER_TABLE'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'SMT'

    # fields of interest in the table without prefix
    @property
    def fields(self) -> list[str]:
        return ['ID', 'NAME', 'VERSION', 'COMMENT', 'MODIFIED_TIME']

    def createSMK(self, l1Idx: Optional[int], hltIdx: Optional[int], hltjoIdx: Optional[int], menu_name: str, comment: str) -> None:
        """
        Create entry in the super-master table and return the corresponding SMK.
        If all files are found to be duplicates, return the existing SMK instead.

        Parameters:
            l1Idx (int): The index of the uploaded L1 menu JSON file.
            hltIdx (int): The index of the uploaded HLT menu JSON file.
            hltjoIdx (int): The index of the uploaded HLT joboptions JSON file.
            comment (str): The comment to be included with the upload.

        Returns:
            lastIdx/existingIdx (int): The index of the uploaded SMK.
        """
        # Set variables required for SQL query
        lastIdx: int = self.getLastTableIndex()
        newIdx: int = lastIdx + 1 if lastIdx > 0 else 3000
        newVersion = self.getLastVersionOfName(menu_name) + 1
        user = os.environ["USER"]
        isHidden = 0
        # Define SQL query to check for duplicate L1 menu, HLT menu, and HLT JO indexes
        query = f"SELECT SMT_ID FROM {self.dbschema}.SUPER_MASTER_TABLE WHERE "
        if l1Idx is not None:
            query += f"SMT_L1_MENU_ID={l1Idx} AND "
        else:
            query += "SMT_L1_MENU_ID IS NULL AND "
        if hltIdx is not None:
            query += f"SMT_HLT_MENU_ID={hltIdx} AND "
        else:
            query += "SMT_HLT_MENU_ID IS NULL AND "
        if hltjoIdx is not None:
            query += f"SMT_HLT_JOBOPTIONS_ID={hltjoIdx}"
        else:
            query += "SMT_HLT_JOBOPTIONS_ID IS NULL"

        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        # Duplicate found
        if len(result) > 0:
            # Get index of existing entry
            self._key = result[0][0]
            self._is_duplicate = True
            self._dbrecord = self.getTableRecord(idx=self._key)
            log.info(f"The files you are trying to upload are all in the database already with SMK {self._key}. Using existing entry.")
            return

        # Insert entry in super-master table
        values: str = f"{newIdx}, '{menu_name}', {newVersion}, :cmt, :l1Idx, :hltIdx, :hltjoIdx, '{user}', CURRENT_TIMESTAMP, {isHidden}"
        bindvars: dict[str,Any] = {
            'cmt': comment, 
            'l1Idx': l1Idx, 
            'hltIdx': hltIdx, 
            'hltjoIdx': hltjoIdx
        }
        query: str = f"INSERT INTO {self.dbschema}.SUPER_MASTER_TABLE(SMT_ID, SMT_NAME, SMT_VERSION, SMT_COMMENT, SMT_L1_MENU_ID, SMT_HLT_MENU_ID, SMT_HLT_JOBOPTIONS_ID, SMT_USERNAME, SMT_MODIFIED_TIME, SMT_HIDDEN) VALUES({values})"
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get and return the SMK of the new entry that has been created
        self._key, self._dbrecord = self.getLastTableRecord()
        log.info(f"Created SMK {self._key} ({menu_name} v{newVersion}) with {l1Idx=}, {hltIdx=}, {hltjoIdx=} and {comment=}.")

class MenuUploader(DBUploaderBase):
    """
    A class to upload trigger menu and job-option files to the TriggerDB and create an
    entry in the supermaster table
    """

    def __init__(self, dbalias: str) -> None:
        """
        Instantiate loader classes for all files. Uses a single connection

        Args:
            dbalias (str): The trigger configuration database alias
        """
        super().__init__(dbalias)
        reuse_connection: DBConnection = self.dbconnection
        self.l1_menu_uploader = L1MenuUploader(existingConnection=reuse_connection)
        self.hlt_menu_uploader = HLTMenuUploader(existingConnection=reuse_connection)
        self.joboptions_uploader = HLTJobOptionUploader(existingConnection=reuse_connection)
        self.smt_uploader = SuperMasterUploader(existingConnection=reuse_connection)
        self.mongrp_uploader = MonGroupUploader(existingConnection=reuse_connection)

        self.loaders: dict[MenuEnum, MenuUploaderBase] = {
            MenuEnum.L1: self.l1_menu_uploader,
            MenuEnum.HLT: self.hlt_menu_uploader,
            MenuEnum.JO: self.joboptions_uploader,
        }
        self._inputs: dict[MenuEnum, Union[int,str,Path,None]] = { menu: None for menu in MenuEnum }
        self._menu_keys: dict[MenuEnum, Optional[int]] = { menu: None for menu in MenuEnum }

    @property
    def l1key(self) -> Optional[int]:
        return self._menu_keys[MenuEnum.L1]

    @property
    def hltkey(self) -> Optional[int]:
        return self._menu_keys[MenuEnum.HLT]

    @property
    def jokey(self) -> Optional[int]:
        return self._menu_keys[MenuEnum.JO]

    @property
    def smk(self) -> Optional[int]:
        return self._key

    @property
    def l1MenuIsDuplicate(self) -> bool:
        return self.l1_menu_uploader.is_duplicate

    @property
    def hltMenuIsDuplicate(self) -> bool:
        return self.hlt_menu_uploader.is_duplicate

    @property
    def hltJoIsDuplicate(self) -> bool:
        return self.joboptions_uploader.is_duplicate

    @property
    def expected_filetype(self) -> str: ...

    @property
    def prefix(self) -> str:
        return "SMT"

    @property
    def table(self) -> str:
        return "SUPER_MASTER_TABLE"

    @property
    def fields(self) -> list[str]: ...

    def upload(self, *,
               l1menu: Union[int,str,Path,None],
               hltmenu: Union[int,str,Path,None], 
               hltjo: Union[int,str,Path,None], 
               mongroup: Union[str,Path,None], 
               comment: str) -> tuple[int, dict[str, Any]]:
        """
        Uploads the full configuration of menus, creates the SMK, and produces
        the upload record file.

        Parameters:
            l1menu (str/int/None): The filename of the L1 menu JSON file,
            or the index of an existing L1 menu entry,
            or None, indicating no L1 menu JSON file is to be uploaded.
            hltmenu (str/int/None): The filename of the HLT menu JSON file,
            or the index of an existing HLT menu entry,
            or None, indicating no HLT menu JSON file is to be uploaded.
            hltjo (str/int/None): The filename of the HLT joboptions JSON file,
            or the index of an existing HLT joboptions entry,
            or None, indicating no HLT joboptions JSON file is to be uploaded.
            mongroup (str/int/None): The filename of the MonitoringGroups JSON file,
            or the index of an existing MonitoringGroups entry,
            or None, indicating no MonitoringGroups JSON file is to be uploaded.
            comment (str): The comment to be included with the upload.

        Returns:
            smk (int): The super-master-key of the upload.
            uploadRecordDict (dict): The ordered dict form of the upload record
            that has been saved as a JSON file.
        """

        self._inputs[MenuEnum.L1] = l1menu
        self._inputs[MenuEnum.HLT] = hltmenu
        self._inputs[MenuEnum.JO] = hltjo

        def checkAndUploadInput(menu_type: MenuEnum) -> None:
            input: int | str | Path | None = self._inputs[menu_type]
            if type(input) is str and input.isdigit():
                input = int(input)
            if type(input) is int: # existing index
                if not self.loaders[menu_type].isValidIndexInTable(index=input):
                    raise RuntimeError(f"The provided index {input} does not specify a valid table entry for {menu_type}.")
                self._menu_keys[menu_type] = input
            elif type(input) is str or isinstance(input,Path): # file name
                if not self.loaders[menu_type].checkJsonContent(expected_filetype=self.loaders[menu_type].expected_filetype, file=input):
                    raise RuntimeError(f"The provided json file {input} is not a valid json file or has the wrong file type.")
                self.loaders[menu_type].upload(input)
                self._menu_keys[menu_type] = self.loaders[menu_type].key

        for menu in MenuEnum:
            checkAndUploadInput(menu)

        # Create SMK
        self._name = self.loaders[MenuEnum.L1].name
        self._comment = comment
        self.smt_uploader.createSMK(self.l1key, self.hltkey, self.jokey, self.name, self.comment)
        self._key = self.smt_uploader._key

        # Create upload record
        self.createUploadRecord()

        # now upload mon group and extend record
        if mongroup is not None:
            self.mongrp_uploader.upload(mongroup_file=mongroup, smk=self._key, comment=self.comment) 
    
        return self._key, self._upload_record

    def createUploadRecord(self) -> None:
        """
        Create a JSON file that provides a record of the upload, containing
        information on the SMK, which files have been uploaded, any existing
        files that have been linked, the comment, the upload time, and the
        username of the uploader.
        """

        # Create ordered dict
        self._upload_record = {
            "smk"       : self._key, # Always add SMK
            "comment"   : self.comment,
            "exectime"  : self.exec_time_str,
            "username"  : self.getUser(),
            "dbrecord"  : self.smt_uploader._dbrecord
        }

        # Add L1, HLT, and JO to existingEntriesLinked or filesUploaded (old style for backward compatiblilty)
        existing = []
        newly_uploaded = []
        for menu in MenuEnum:
            input: int | str | Path | None = self._inputs[menu]
            is_duplicate: bool = self.loaders[menu].is_duplicate
            id_field: str = {
                MenuEnum.L1:"l1TableId", MenuEnum.HLT: "hltTableId", MenuEnum.JO:"hltJoTableId"
                }[menu]
            record: dict[str,Any] = {
                "filetype": self.loaders[menu].expected_filetype,
                id_field: self.loaders[menu].key
            }
            if type(input) is int or is_duplicate:
                existing += [record]
            elif input is not None and not is_duplicate:
                record['filename'] = input
                newly_uploaded += [record]
        if existing:
            self._upload_record["existingEntriesLinked"] = existing
        if newly_uploaded:
            self._upload_record["filesUploaded"] = newly_uploaded

        for menu in MenuEnum:
            self._upload_record[str(menu).lower()] = {
                "key": self.loaders[menu].key,
                "history": "existed" if self.loaders[menu].is_duplicate else "new",
                "input": self._inputs[menu],
                "filetype": self.loaders[menu].expected_filetype,
                "dbrecord": self.loaders[menu]._dbrecord
            }

        # Create the JSON file from the upload record dictionary with filename based on the upload time
        outfn: str = f"upload_record_menu_{self.exec_time_str}.json"
        with open(outfn, "w") as fp:
            json.dump(self._upload_record, fp, indent=4, cls=DateTimeEncoder)
        log.info(f"Created {outfn}")

    def print_record(self) -> None:
        print("Key Summary:")
        print(" SMK | Comment")
        print("--------------")
        print(f"{self._key:4} | {self._upload_record['comment']}")
