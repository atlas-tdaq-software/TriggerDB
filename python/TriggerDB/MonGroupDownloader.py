#!/usr/bin/env python

from __future__ import annotations
from typing import Optional, Any, TYPE_CHECKING, Union
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBDownloader import DBDownloaderBase

class MonGroupDownloader(DBDownloaderBase):
    """
    Download a mon group set file from a trigger configuration databae.
    """

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Args:
            dbalias (str): the trigger db connection alias
            existingConnection (DBConnection | None): if one wants to use an existing DBConnection
        """
        super().__init__(dbalias, existingConnection=existingConnection)
        self._smk: int = 0

    @property
    def prefix(self) -> str:
        return 'HMG'

    @property
    def table(self) -> str:
        return 'HLT_MONITORING_GROUPS'

    def query(self) -> str:
        return f"SELECT HMG_DATA FROM {self.dbschema}.HLT_MONITORING_GROUPS WHERE HMG_ID=:key"

    def downloadMonGroupsLinkedToSMK(self, smk:int) -> dict[Union[int,str],Any]:
        """
        Download the active monitoring groups for a given super-master key

        Parameters:
            smk (int): The smk to match the monitoring groups

        Returns:
            str: The monitoring groups as a string
        """
        # get the list of active mongroups attached to the SMK through the HLT menu
        query: str = (
            f"SELECT HMG_ID FROM {self.dbschema}.SUPER_MASTER_TABLE SMT, {self.dbschema}.HLT_MONITORING_GROUPS HMG "
            f"WHERE HMG.HMG_IN_USE = 1 AND SMT.SMT_HLT_MENU_ID = HMG.HMG_HLT_MENU_ID AND SMT.SMT_ID={smk}"
        )
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()

        linked_mongroups: list[int] = [r[0] for r in result]

        if len(linked_mongroups) == 0:
            raise RuntimeError(f"The provided SMK {smk} has no active mongroups attached ")

        if len(linked_mongroups) > 1:
            raise RuntimeError(f"The provided SMK {smk} has more than one active mongroups attached")

        # Download the monitoring groups for the active key
        return self.download(key=linked_mongroups[0])
        
    def downloadMonGroupsLinkedToHltMenu(self, hltmenu: int) -> dict[Union[int,str],Any]:
        """
        Download the active monitoring group for a given hlt menu

        Parameters:
            hltmenu (int): The ID of the hlt menu for which to find the active monitoring group set

        Returns:
            str: The active monitoring groups as a string
        """
        # get the list of active mongroups attached to the HLT menu
        query: str = f"SELECT HMG_ID FROM {self.dbschema}.HLT_MONITORING_GROUPS WHERE HMG_HLT_MENU_ID={hltmenu} AND HMG_IN_USE=1"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        linked_active_mongroups: list[int] = [r[0] for r in result]

        if len(linked_active_mongroups) == 0:
            raise RuntimeError(f"The provided HLT Menu {hltmenu} has no active mongroups attached ")

        if len(linked_active_mongroups) > 1:
            raise RuntimeError(f"The provided HLT Menu {hltmenu} has more than one active mongroups attached")

        # Download the monitoring groups for the active key
        return self.download(key=linked_active_mongroups[0])

    def getMonGroups(self, smk: int) -> list[dict[str,Any]]:
        """
        Get the list of Mon Group ids, whether they are in use and comments from the SMK.

        Parameters:
            smk (int): The super-master key for which mon groups are to be read.

        Returns:
            listOfMonGroups (list(int)): The (perhaps empty) list of Monitoring groups records associated to the SMK
        """
        fields: list[str] =  ['ID', 'IN_USE', 'COMMENT']
        query: str = (
            f"SELECT {','.join([f'HMG.HMG_{f}' for f in fields])} FROM "
            f"{self.dbschema}.SUPER_MASTER_TABLE SMT, {self.dbschema}.HLT_MONITORING_GROUPS HMG "
            "WHERE SMT.SMT_HLT_MENU_ID = HMG.HMG_HLT_MENU_ID AND SMT.SMT_ID=:smk"
        )
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            result = cursor.fetchall()
        return [dict(zip(fields,entry)) for entry in result]

    def getMongroupInfo(self, mongroup:int) -> dict[str, Any]:
        """
        Get information about the monitoring group:
        - the in-use flag
        - the comment
        - the ID of the HLT menu to which the mongroup is pointing
        - the list of SMKs that share the HLT menu

        Parameters:
            mongroup (int): The monitoring group key is for which SMKs are to be read.

        Returns:
            list[int]: The list of SMK that contain the HLT menu which the monitoring group is associated with
        """
        fields = ['HMG_HLT_MENU_ID', 'HMG_NAME', 'HMG_VERSION', 'HMG_COMMENT', 'HMG_IN_USE', 'HMG_HIDDEN']
        query: str = (
            f"SELECT SMT_ID, {', '.join(fields)} "
            f"FROM {self.dbschema}.SUPER_MASTER_TABLE, {self.dbschema}.HLT_MONITORING_GROUPS "
            "WHERE SMT_HLT_MENU_ID = HMG_HLT_MENU_ID and HMG_ID=:mgk"
        )
        with self.cursor() as cursor:
            # Execute query and fetch result
            cursor.execute(query, mgk = mongroup)
            result = cursor.fetchall()
        
        if len(result) == 0:
            return {}

        smks: list[int] = [r[0] for r in result]
        info = dict(zip(fields,result[0][1:]))
        info.update({"SMKs": smks})
        return info


if __name__=="__main__":
    smk = 3347
    with MonGroupDownloader("TRIGGERDB_RUN3") as downloader:
        print(downloader.getMonGroups(smk))
        downloader.downloadMonGroupsLinkedToSMK(smk=smk)
        downloader.writeFile(f"MonGroups_smk{smk}.json")
