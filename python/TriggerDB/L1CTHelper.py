from datetime import datetime
from typing import Optional, Union, Any

from enum import Enum

FT = Enum('FT', "CTP SMX MUC TMC")

def getFileTableStructure() -> dict[Union[FT,str], dict[str,Any]]:
    fileTableStructure = {
        FT.CTP : {
            "directory" : "L1_CTP_FILES",
            "table" : "L1_CTP_FILES",
            "prefix" : "L1CF",
            "files" : {
                "lut.dat" : "L1CF_LUT",
                "cam.dat" : "L1CF_CAM",
                "mon_dec_CTPMON.dat" : "L1CF_MON_DEC_CTPMON",
                "mon_dec_SLOT7.dat" : "L1CF_MON_DEC_SLOT7",
                "mon_dec_SLOT8.dat" : "L1CF_MON_DEC_SLOT8",
                "mon_dec_SLOT9.dat" : "L1CF_MON_DEC_SLOT9",
                "mon_dmx_CTPMON.dat" : "L1CF_MON_DMX",
                "mon_sel_CTPMON.dat" : "L1CF_MON_SEL_CTPMON",
                "mon_sel_SLOT7.dat" : "L1CF_MON_SEL_SLOT7",
                "mon_sel_SLOT8.dat" : "L1CF_MON_SEL_SLOT8",
                "mon_sel_SLOT9.dat" : "L1CF_MON_SEL_SLOT9",
                "smx.dat" : "L1CF_SMX"
            }
        },
        FT.SMX : {
            "directory" : "L1_CTP_SMX",
            "table" : "L1_CTP_SMX",
            "prefix" : "L1SMX",
            "files" : {
                "smxo.dat" : "L1SMX_OUTPUT",
                "smx_SLOT7.vhd" : "L1SMX_VHDL_SLOT7",
                "smx_SLOT8.vhd" : "L1SMX_VHDL_SLOT8",
                "smx_SLOT9.vhd" : "L1SMX_VHDL_SLOT9",
                "ctpin_smx_slot7.svf" : "L1SMX_SVFI_SLOT7",
                "ctpin_smx_slot8.svf" : "L1SMX_SVFI_SLOT8",
                "ctpin_smx_slot9.svf" : "L1SMX_SVFI_SLOT9",
            }
        },
        FT.MUC : {
            "directory" : "L1_MUCTPI_FILES",
            "table" : "L1_MUCTPI_FILES",
            "prefix" : "L1MF",
            "files" : {
                "muctpi.json" : "L1MF_DATA"
            }
        },
        FT.TMC : {
            "directory" : "L1_TMC_SIGNALS",
            "table" : "L1_TMC_SIGNALS",
            "prefix" : "L1TMC",
            "files" : {
                "tmc.json" : "L1TMC_DATA"
            }
        },
        "menu" : {
            "directory" :  "L1_MENU"
        }
    }

    return fileTableStructure

class SMKInfo:
    def __bool__(self) -> bool:
        return self.smk != 0
    def __str__(self) -> str:
        return f"SMK {self.smk}: {self.name} v{self.version} ({self.date}), [#={self.menuHash}, {self.tableId[FT.CTP]}, {self.tableId[FT.SMX]}, {self.tableId[FT.MUC]}, {self.tableId[FT.TMC]}]"
    def __init__(self, record):
        self.smk: int = record[0]
        self.name: str = record[1]
        self.version: int = record[2]
        self.comment: Optional[str] = record[3]
        self.date: datetime = record[4]
        self.menuHash: str = record[5]
        self.tableId: dict[FT,int] = {
            FT.CTP: record[6],
            FT.SMX: record[7],
            FT.MUC: record[8],
            FT.TMC: record[9]
        }
        self.tableHash: dict[FT,bytes] = {fset:b"" for fset in FT}
    def noFilesAttached(self):
        return all([self.tableId[fset] is None for fset in FT])
    def allFilesAttached(self):
        return all([self.tableId[fset] is not None for fset in FT])        
    def hasMenuHash(self) -> bool:
        return self.menuHash is not None
    def getCreationDate(self) -> str:
        return "-" if (not self or self.date is None) else self.date.strftime("%Y-%M-%d %H:%M:%S") 
    def getComment(self):
        return "-" if (not self or self.comment is None) else self.comment
    def getAttachedFiles(self) -> str:
        if not self:
            return "-"
        if self.allFilesAttached():
            return "all"
        if self.noFilesAttached():
            return "none"
        attached = []
        if self.tableId[FT.CTP]: attached += ["ctp"]
        if self.tableId[FT.SMX]: attached += ["smx"]
        if self.tableId[FT.MUC]: attached += ["muctpi"]
        if self.tableId[FT.TMC]: attached += ["tmc"]
        return ",".join(attached)
    def getID(self, fset:FT) -> int:
        return self.tableId[fset]
    def getHash(self,fset) -> bytes:
        return self.tableHash[fset]

def getAllKeysInfo(cursor, smk: int = 0) -> dict[int, SMKInfo]:
    query: str = (
        "SELECT sm.smt_id, sm.smt_name, sm.smt_version, sm.smt_comment,"
        " sm.smt_modified_time, l1tm.l1tm_hash, l1tm.l1tm_ctp_files_id,"
        " l1tm.l1tm_ctp_smx_id, l1tm.l1tm_muctpi_files_id, l1tm.l1tm_tmc_signals_id"
        " FROM super_master_table sm, L1_MENU l1tm"
        " WHERE sm.smt_l1_menu_id = l1tm.l1tm_id"
    )
    if smk>0:
        query += " AND sm.smt_id=:smk"
    query += " ORDER BY sm.SMT_ID DESC"
    if smk>0:
        cursor.execute(query, smk=smk)
    else:
        cursor.execute(query)
    smks: list[SMKInfo] = [SMKInfo(record) for record in cursor.fetchall()]
    if len(smks)==1:
        info: SMKInfo = smks[0]
        conf = getFileTableStructure()
        for fset in FT:
            table: str = conf[fset]['table']
            prefix: str = conf[fset]['prefix']
            if (id:=info.getID(fset)) is not None:
                query: str = f"SELECT {prefix}_hash FROM {table} WHERE {table}.{prefix}_id = :id"
                cursor.execute(query, id=id)
                res = cursor.fetchone()
                info.tableHash[fset] = res[0]
    return {s.smk : s for s in smks}

