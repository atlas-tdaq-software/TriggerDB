#!/usr/bin/env python3

from __future__ import annotations
from typing import Optional, TYPE_CHECKING
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

from TriggerDB.DBDownloader import DBDownloaderBase

class PrescaleSetDownloader(DBDownloaderBase):
    def __init__(self, dbalias: str, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)

    def query(self) -> str:
        return f"SELECT {self.prefix}_DATA FROM {self.dbschema}.{self.table} WHERE {self.prefix}_ID=:key"

class L1PrescaleSetDownloader(PrescaleSetDownloader):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)

    # table name
    @property
    def table(self) -> str:
        return 'L1_PRESCALE_SET'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'L1PS'


class HLTPrescaleSetDownloader(PrescaleSetDownloader):
    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
    
    # table name
    @property
    def table(self) -> str:
        return 'HLT_PRESCALE_SET'

    # common prefix for all fields
    @property
    def prefix(self) -> str:
        return 'HPS'


if __name__=="__main__":
    l1psk = 13562
    hltpsk = 10454
    with L1PrescaleSetDownloader("TRIGGERDB_RUN3") as downloader:
        downloader.download(key=l1psk)
        downloader.writeFile(f"L1Prescale_{l1psk}.json")

    with HLTPrescaleSetDownloader("TRIGGERDB_RUN3") as downloader:
        downloader.download(key=hltpsk)
        downloader.writeFile(f"HLTPrescale_{hltpsk}.json")