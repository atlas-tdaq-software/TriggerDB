#!/usr/bin/env python3

__all__: list[str]=['log']

import argparse
import logging
import sys

class color:
    PURPLE =    '\033[95m'
    CYAN =      '\033[96m'
    DARKCYAN =  '\033[36m'
    BLUE =      '\033[94m'
    GREEN =     '\033[92m'
    YELLOW =    '\033[93m'
    RED =       '\033[91m'
    BOLD =      '\033[1m'
    UNDERLINE = '\033[4m'
    END =       '\033[0m'
    ITALIC =    '\033[3m'
    GREY =      '\033[38;20m'


class CustomFormatter(logging.Formatter):
    # _format='%(asctime)s L%(lineno)-4s in %(filename)-20s %(levelname)s %(message)s'
    _format: str='[%(filename)s:L%(lineno)s] (%(asctime)s)   %(levelname)s %(message)s'
    colors: dict[int, str] = {
        logging.DEBUG: color.GREY,
        logging.INFO: color.GREY,
        logging.WARNING: color.YELLOW,
        logging.ERROR: color.RED,
        logging.CRITICAL: color.BOLD+color.RED
    }

    def format(self, record) -> str:
        log_fmt: str = f"{self.colors[record.levelno]}{self._format}{color.END}"
        formatter = logging.Formatter(log_fmt, datefmt="%d %b %X") # other: datefmt="%x %X"
        return formatter.format(record)

log: logging.Logger = logging.getLogger("trigerdb")
log.setLevel(logging.INFO)

# create console handler with a higher log level
ch = logging.StreamHandler(sys.stdout)
# ch.setLevel(logging.INFO)
ch.setFormatter(CustomFormatter())
log.addHandler(ch)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()

    log.setLevel(args.loglevel)
    log.debug("debug message")
    log.info("info message")
    log.warning("warning message")
    log.error("error message")
    log.critical("critical message")
