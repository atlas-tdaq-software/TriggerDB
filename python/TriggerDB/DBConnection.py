#!/usr/bin/env python

import os
from typing import Literal, Optional, cast
import xml.etree.ElementTree as ET
from TriggerDB.Logger import log

# import cx_Oracle as oracledb
import oracledb
# enable python-oracledb Thick mode which is required because the 
# database has Native Network Encryption (NNE) enabled and we don't 
# use Transport Layer Security (TLS)
oracledb.init_oracle_client() 

class DBConnection:

    AUTH_PATH_READ_AFS = "/afs/cern.ch/user/a/attrgcnf/.dbauth/run3/read/"
    AUTH_PATH_READ_P1 = "/det/tdaq/hlt/trigconf/run3/read/"

    AUTH_PATH_WRITE_AFS = "/afs/cern.ch/user/a/attrgcnf/.dbauth/run3/write/"
    AUTH_PATH_WRITE_P1 = "/det/tdaq/hlt/trigconf/run3/write/"

    AUTH_PATH_ART = "./"

    def __init__(self, dbalias: str, *, readwrite: bool = False) -> None:
        """
        Input
        -----
        dbalias: str the alias of the trigger DB connection
        (see gitlab.cern.ch/atlas-tdaq-software/TriggerDB#database-alias-options for the available options)

    
        Attributes
        ----------
        dbschema (str): The DB schema used in the DB connection.
        _dbuser (str): The DB user account used in the DB connection.
        _dbpw (str): The DB user password used in the DB connection.
        _dbsid (str): The SID used in the DB connection. 
        """

        self.dbalias: str = dbalias # the trigger db connection alias
        self.readwrite: bool = readwrite # the trigger db connection alias
        self.dbschema: str = ""
        self._dbuser: str = ""
        self._dbpw: str = ""
        self._dbsid: str = ""
        self._connection: Optional[oracledb.Connection] = None
        if not self.setConnectionParameters():
            raise RuntimeError("Cannot establish TriggerDB connection. Connection credentials access failed.")

    @property
    def connection(self) -> oracledb.Connection:
        self.open()
        return cast(oracledb.Connection,self._connection)

    def cursor(self) -> oracledb.Cursor:
        # returns a new cursor
        return self.connection.cursor()

    def open(self) -> None:
        """Open the database connection."""
        if self._connection is None:
            log.debug(f"Opening DB connection {self._dbuser}@{self._dbsid}")
            self._connection = oracledb.Connection(f"{self._dbuser}/{self._dbpw}@{self._dbsid}")
            # setting the current schema of the connection avoids that we have to put it into all the queries
            self._connection.current_schema = self.dbschema

    def close(self) -> None:
        """Close the database connection"""
        if self._connection is not None:
            log.debug("Closing DB connection")
            self._connection.close()
            self._connection = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, a, b, traceback) -> None:
        self.close()

    def resolveDBAuthPath(self) -> str:
        dbauthPath:str = ""
        if 'CORAL_AUTH_PATH_DEV' in os.environ:
            dbauthPath = os.environ['CORAL_AUTH_PATH_DEV']
            log.info(f"Authentication path at for developments: {dbauthPath}")
        elif 'TDAQ_SETUP_POINT1' in os.environ:
            dbauthPath = DBConnection.AUTH_PATH_WRITE_P1 if self.readwrite else DBConnection.AUTH_PATH_READ_P1
            log.info(f"Authentication path at Point 1: {dbauthPath}")
        elif 'TRIGGER_DB_ART' in os.environ:
            dbauthPath = DBConnection.AUTH_PATH_ART 
            log.info(f"Authentication path for ART tests: {dbauthPath}")
            log.info(f"Note that the files dblookup.xml and authentication.xml must be copied locally before accessing the DB")
        else:
            dbauthPath = DBConnection.AUTH_PATH_WRITE_AFS if self.readwrite else DBConnection.AUTH_PATH_READ_AFS
            log.debug(f"Authentication path {dbauthPath}")
        return dbauthPath

    def checkFileAccess(self, dbauthPath: str) -> bool:
        def check(filepath:str) -> bool:
            try:
                with open(filepath, 'r') as fp:
                    pass
            except FileNotFoundError as exc:
                log.error(f"File {filepath} does not exist [{exc}].")
                return False
            except PermissionError as exc:
                log.error(f"You don't have  permissions to read {filepath} [{exc}].")
                return False
            return True
        
        c1 = check(dbauthPath+"/dblookup.xml")
        c2 = check(dbauthPath+"/authentication.xml")
        return c1 and c2

    def parseAuthFiles(self, path: str) -> bool:
        dblookup:str = f"{path}/dblookup.xml"
        et: ET.ElementTree = ET.parse(dblookup)
        conn_name: str = ""
        found_logical_svc: bool = False
        found_svc: bool = False
        access_mode: Literal['update','read'] = "update" if self.readwrite else "read"
        for log_svc in et.findall("logicalservice"):
            if log_svc.attrib['name'] != self.dbalias:
                continue
            found_logical_svc = True
            for svc in log_svc:
                if svc.attrib['accessMode'] != access_mode:
                    continue
                found_svc = True
                conn_name = svc.attrib['name']
                break
            if found_svc:
                break

        if not found_logical_svc:
            log.error(f'Did not find DB connection alias {self.dbalias} in {dblookup}.')
            trigdb_aliases = filter(lambda x: x.startswith('TRIGGERDB'),[ls.attrib['name'] for ls in et.findall("logicalservice")])
            log.error(f"Available trigger db aliases are {', '.join(trigdb_aliases)}")
            return False
        if not found_svc:
            log.error(f'Found DB connection alias {self.dbalias} in {dblookup}, but not with access mode {access_mode}.')
            return False
        log.debug(f"For DB connection alias {self.dbalias} found connection {conn_name}")

        # set database server and schema name
        self._dbsid = conn_name.split("//")[1].split("/")[0]
        self.dbschema = conn_name.split("//")[1].split("/")[1]

        dbauth:str = f"{path}/authentication.xml"
        et: ET.ElementTree = ET.parse(dbauth)
        credentials: dict[str,str] = {}
        for conn in et.findall("connection"):
            if conn.attrib['name'] != conn_name:
                continue
            for param in conn.findall('parameter'):
                credentials[param.attrib['name']] = param.attrib['value']
            break
        if len(credentials) != 2:
            log.error(f'Did not find user and password for {conn_name} in {dbauth}.')
            return False

        self._dbuser = credentials['user']
        self._dbpw = credentials['password']
        log.debug(f'For {conn_name} found user {self._dbuser} and password.')
        return True

    def setConnectionParameters(self) -> bool:
        dbauthPath: str = self.resolveDBAuthPath()

        if not self.checkFileAccess(dbauthPath):
            return False

        if not self.parseAuthFiles(dbauthPath):
            return False

        return True
            
if __name__=="__main__":
    with DBConnection(dbalias="TRIGGERDB_RUN3") as dbconn:
        with dbconn.cursor() as cursor:
            query: str = f"SELECT table_name FROM all_tables WHERE owner = '{dbconn.dbschema}'"
            for r in cursor.execute(query):
                print(r)
