#!/usr/bin/env python

from __future__ import annotations
from pathlib import Path
from typing import Any, Optional, TYPE_CHECKING

from TriggerDB.hashHelper import getFileHash
if TYPE_CHECKING:
    from TriggerDB.DBConnection import DBConnection

import json

from TriggerDB.insertTools import DateTimeEncoder
from TriggerDB.DBUploader import DBUploaderBase
from TriggerDB.Logger import log


class MonGroupUploader(DBUploaderBase):
    """
    A class to upload monitoring group files to a TriggerDB.
    """

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Args:
            dbalias (str): The alias for which instance of TriggerDB to connect to
                           (see gitlab.cern.ch/atlas-tdaq-software/TriggerDB#database-alias-options
                           for the available options)
            existingConnection (DBConnection, optional): reuse existing connection. Defaults to None.
        """
        super().__init__(dbalias=dbalias, existingConnection=existingConnection)
        self._smk: int = 0

    @property
    def expected_filetype(self) -> str:
        return "hltmonitoringsummary"

    @property
    def prefix(self) -> str:
        return 'HMG'

    @property
    def table(self) -> str:
        return 'HLT_MONITORING_GROUPS'

    @property
    def fields(self) -> list[str]:
        return ['ID', 'NAME', 'VERSION', 'COMMENT', 'MODIFIED_TIME']

    def getMonitoringGroupName(self, blob) -> str:
        """
        Extract the monitoring group name from the JSON monitoring group file.

        Parameters:
            blob (cx_Oracle.BLOB): The BLOB object of the JSON monitoring group file.

        Returns:
            MonitoringGroupName (str): The name of the monitoring group, extracted from the
            JSON file.
        """
        j = json.loads(blob.getvalue().read())
        MonitoringGroupName = j["name"]
        return MonitoringGroupName

    def isMonGroupInUse(self, mgk:int) -> bool:
        """
        Gets a single monitoring group's IN_USE column to see if in use or not.

        Parameters:
            mgk (str): The id of the monitoring group entry,

        Returns:
            bool: whether this monitoring group is in use or not
        """
        query = f"SELECT HMG_IN_USE FROM {self.dbschema}.{self.table} WHERE HMG_ID={mgk}"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        return int(result[0][0]) == 1

    def setMonGroupNotInUse(self, mongroupkey: str) -> None:
        """
        Sets a single monitoring group's IN_USE column off.

        Parameters:
            mongroupkey (str): The id of the monitoring group entry,
        """

        query = f"UPDATE {self.dbschema}.{self.table} SET HMG_IN_USE = 0 WHERE HMG_ID ={mongroupkey}"
        with self.cursor() as cursor:
            cursor.execute(query)
            cursor.connection.commit()
        print(f"Set Monitoring group {mongroupkey} to no longer be in use")

    def getMonGroupsInUse(self, smk) -> Optional[int]:
        """
        For a given SMK get the mongroups ID which is in use.

        Parameters:
            smk (int): The super-master key for which mon groups are to be read.

        Returns:
            Optional[in]: The mongroups id which is in use or None, when no mongroup ids are in_use
        """
        query: str = (
            f"SELECT HMG_ID FROM {self.dbschema}.SUPER_MASTER_TABLE SMT, {self.dbschema}.HLT_MONITORING_GROUPS HMG "
            "WHERE SMT.SMT_HLT_MENU_ID = HMG.HMG_HLT_MENU_ID AND SMT.SMT_ID=:smk AND HMG.HMG_IN_USE = 1"
        )
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            result = cursor.fetchall()
        
        # in_use mongroups found
        if len(result) > 0:
            in_use_idx = int(result[0][0])
            log.info(f"Monitoring Groups with ID {in_use_idx} found to be currently in use")
            if len(result) > 1:
                log.warning("You have multiple monitoring groups currently enabled. Please use listMonGroupsBySMK.py and setMonitoringGroupsInUse.py to check and clear up")
            return in_use_idx

        log.info("No Monitoring Groups found which are currently in use")
        return None

    def _upload(self) -> None:
        """
        Upload the monitoring group and return the index. If the monitoring group is found to
        be a duplicate, return the existing monitoring group index instead.

        Parameters:
            mongroup (str): The filename of the monitoring group JSON file,
            smk (int): The SMK to link the monitoring group set to.
            comment (str): The comment to be included with the upload.

        Returns:
            lastIdx/existingIdx (int): The index of the uploaded monitoring group/existing
            monitoring group.
        """

        self._is_duplicate = False
        blob = self.getBlobFromFile(self._input)
        newIdx: int = self.getLastTableIndex() + 1
        self._name = self.getMonitoringGroupName(blob)
        newVersion = self.getLastVersionOfName(self.name) + 1
        mghash: str = getFileHash(self._input)
        user: str = self.getUser()
        menuIdx = self.getMenuIndex(self._smk, "HLT")
        # Raise error if the SMK doesn't contain an HLT menu
        if menuIdx is None:
            raise RuntimeError(f"The provided SMK {self._smk} has no HLT menu, so the monitoring groups cannot be uploaded.")
        set_isHidden = 0
        set_isInUse = 1

        # check for existing entry by comparing hash values
        query: str = f"SELECT HMG_ID FROM {self.dbschema}.{self.table} WHERE HMG_HASH='{mghash}' AND HMG_HLT_MENU_ID={menuIdx}"
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        # Duplicate found
        if len(result) > 0:
            # Get index of existing entry
            self._key = result[0][0]
            in_use = bool(self.isMonGroupInUse(self._key))
            log.info(f"The Monitoring Group is already contained in the database with index {self._key} with In Use = {in_use}. Using existing entry.")
            log.info("If you need to change which Monitoring Group is currently In Use then please run setMonitoringGroupsInUse.py")
            # Set IsDuplicate flag and return index of existing entry
            self._is_duplicate = True
            self._dbrecord = self.getTableRecord(idx = self._key)
            return

        # Work out whether we already have Monitoring Groups for this SMK and if so set this upload so not in use   
        existing_inuse_mongroup = self.getMonGroupsInUse(self._smk)
        if existing_inuse_mongroup is not None:
            set_isInUse=0
            log.info(f"Found existing Mongroups to be in_use (index {existing_inuse_mongroup}) for this SMK. New mongroups set in_use=0")

        # Insert monitoring groups into database
        insert_fields: list[str] = ["ID", "NAME", "DATA", "HASH", "VERSION", "COMMENT", "HLT_MENU_ID", "USERNAME", "MODIFIED_TIME", "HIDDEN", "IN_USE"]
        fields: str = ', '.join([f'{self.prefix}_{f}' for f in insert_fields])
        values: str = f"{newIdx}, :name, :blob, '{mghash}', {newVersion}, :cmt, {menuIdx}, '{user}', CURRENT_TIMESTAMP, {set_isHidden}, {set_isInUse}"
        bindvars: dict[str,Any] = {
            'name': self.name,
            'blob': blob,
            'cmt': self.comment
        }
        query = f"INSERT INTO {self.dbschema}.{self.table}({fields}) VALUES({values})"        
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get the last record and its index (which is the new entry that has been created)
        self._key, self._dbrecord = self.getLastTableRecord()
        in_use: bool = self.isMonGroupInUse(self._key)
        log.info(f"Inserted Monitoring Group {self.name} (version {newVersion}) into the Monitoring Group table with index {self._key} and In Use = {in_use}, linked to SMK {self._smk}. ")
        log.info("If you need to change which Monitoring Group is currently In Use then please run setMonitoringGroupsInUse.py")

    def createUploadRecord(self, existing_upload_record: Optional[dict[str,Any]] = None) -> None:
        """
        Create a JSON file that provides a record of the upload, containing
        information on the SMK, which files have been uploaded, any existing
        files that have been linked, the comment, the upload time, and the
        username of the uploader.

        Parameters:
            existing_upload_record (Optional[dict[str,Any]], optional): The ordered dict form of the existing upload record
            if the class instance is to be called from within a with statement for
            another uploader class (defaults to None).

        Returns:
            dict[str,Any]: The upload record that has also been saved as a JSON file.
        """

        self._upload_record = existing_upload_record if existing_upload_record is not None else {}
        self._upload_record["smk"] = self._smk
        self._upload_record["comment"] = self.comment
        self._upload_record["exectime"] = self.exec_time_str
        self._upload_record["username"] = self.getUser()
        self._upload_record["mongroup"] = {
            "key" : self._key,
            "filetype" : self.expected_filetype,
            "history": "existed" if self._is_duplicate else "new",
            "input": self._input,
            "filetype": self.expected_filetype,
            "dbrecord": self._dbrecord
        }

        # Create/update the JSON file from the upload record dictionary with filename based on the upload time
        outfn: str = f"upload_record_mongroups_{self.exec_time_str}.json"
        with open(outfn, "w") as fp:
            json.dump(self._upload_record, fp, indent=4, cls=DateTimeEncoder)
        log.info(f"Created {outfn}")

    def upload(self, mongroup_file: str | Path, smk: int, comment: str, existing_upload_record=None):
        """
        Uploads the full configuration of monitoring groups, and produces
        the upload record file.

        Parameters:
            mongroup_filename (str): filename of the monitoring group JSON file
            smk (int): The super-master-key of the upload.
            comment (str): The comment to be included with the upload.
            existingDict (dict): The ordered dict form of the existing upload record
            if the class instance is to be called from within a with statement for
            another uploader class (defaults to None).

        Returns:
            MonitoringGroupKey (int): The key of the monitoring groups uploaded
            upload_record (dict): The ordered dict form of the upload record
            that has been saved as a JSON file.

        """

        self._input: str | Path = mongroup_file
        self._smk = smk
        self._comment = comment

        # check if provided SMK is valid
        if not self.isValidSMK(smk):
            raise RuntimeError(f"The provided SMK {smk} is invalid.")
        
        # check provided Monitoring Group file is a valid json file of the correct filetype
        if not self.checkJsonContent(expected_filetype=self.expected_filetype, file=self._input):
            raise RuntimeError(f"The provided file {self._input} is not a monitoring groups json file")

        # upload monitoring groups file
        self._upload()

        # create upload record
        self.createUploadRecord(existing_upload_record)
        return self._key, self._upload_record

    def activateMonGroup(self, *, mgk: int, smk: int = 0) -> bool:
        """
        Sets whether a single monitoring group's IN_USE column is set to 1:on or 0:off.

        Parameters:
            mgk (int): The monitoring group key
            smk (int, optional): The super master key as cross-check. Defaults to 0, and no cross-check is applied.
        """

        query: str = (
            f"select HMG_ID, HMG_IN_USE, SMT_ID from {self.dbschema}.{self.table}, {self.dbschema}.SUPER_MASTER_TABLE "
            f"where SMT_HLT_MENU_ID=HMG_HLT_MENU_ID and HMG_HLT_MENU_ID = (select HMG_HLT_MENU_ID from {self.dbschema}.{self.table} where HMG_ID=:mgk)"
        )
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            result: list[tuple[int,int,int]] = cursor.fetchall()

        if len(result)==0:
            log.error(f"Monitoring key {mgk} does not exist in the database {self.dbconnection.dbalias}. Nothing will be done.")
            return False

        if smk>0:
            # check if the specified smk is linked to the HLT menu for which this mgk applies 
            smks: set[int] = set([int(r[2]) for r in result])
            if smk not in smks:
                log.error(f"SMK {smk} is not using monitoring group {mgk}. Nothing will be done")
                return False

        # check if update is needed
        update_needed = False
        for mg_id, mg_inuse, _ in result:
            if mg_id == mgk and mg_inuse==0:
                update_needed = True
            if mg_id != mgk and mg_inuse==1:
                update_needed = True
        
        if not update_needed:
            log.info(f"Monitoring group key {mgk} is already the active one. No update needed.")
            return True

        # run the update
        query = f"UPDATE {self.dbschema}.{self.table} SET HMG_IN_USE = :inuse WHERE HMG_ID = :mgk"
        with self.cursor() as cursor:
            for mg_id, _, _ in result:
                set_inuse: bool = mg_id==mgk
                cursor.execute(query, mgk=mg_id, inuse = set_inuse)
                cursor.connection.commit()
                log.info(f"For monitoring group {mgk} set flag in-use to {'1' if set_inuse else '0'}")
        return True

    def deactivateMonGroups(self, *, smk: int) -> bool:
        query: str = (
            f"UPDATE {self.dbschema}.{self.table} SET HMG_IN_USE = 0 where HMG_ID in ("
            f"select HMG_ID from {self.dbschema}.{self.table}, {self.dbschema}.SUPER_MASTER_TABLE "
            "where SMT_HLT_MENU_ID=HMG_HLT_MENU_ID and HMG_HLT_MENU_ID and SMT_ID = :smk"
            ")"
        )
        with self.cursor() as cursor:
            cursor.execute(query, smk=smk)
            cursor.connection.commit()
            log.info(f"For smk {smk} set all monitoring groups to in-use = 0")



        return self.activateMonGroup(mgk = 0, smk = smk)

    def print_record(self) -> None:
        print("Key Summary:")
        print(" SMK | MGK | In Use? | Comment")
        print("-------------------------------")
        print(f"{self._smk:4} |{self._key:4} |{bool(self.isMonGroupInUse(self._key)):8} | {self._upload_record['mongroup']['dbrecord']['COMMENT']}")

