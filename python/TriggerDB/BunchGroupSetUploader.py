#!/usr/bin/env python

from __future__ import annotations
from typing import Any, Optional

import json
import os

from TriggerDB.DBUploader import DBUploaderBase
from TriggerDB.DBConnection import DBConnection
from TriggerDB.Logger import log
from TriggerDB.hashHelper import getDictHash
from .insertTools import DateTimeEncoder

class BunchGroupSetUploader(DBUploaderBase):
    """
    A class to upload a bunch group set file to a TriggerDB.
    """

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Args:
            dbalias (str): The alias for which instance of TriggerDB to connect to
                           (see gitlab.cern.ch/atlas-tdaq-software/TriggerDB#database-alias-options
                           for the available options)
        """
        super().__init__(dbalias, existingConnection=existingConnection)
        self._nbunches: int = 0

    @property
    def expected_filetype(self) -> str:
        return "bunchgroupset"

    @property
    def prefix(self) -> str:
        return 'L1BGS'

    @property
    def table(self) -> str:
        return 'L1_BUNCH_GROUP_SET'

    @property
    def fields(self) -> list[str]:
        return ['ID', 'NAME', 'VERSION', 'COMMENT', 'MODIFIED_TIME']


    def upload(self, filename: Optional[str] = None, *, textString: Optional[str] = None, 
               bgset: Optional[dict[str,Any]] = None, comment: str = "") -> tuple[int, dict[str, Any]]:
        """
        Checks if the provided text is a bunchgroup set description in json format,
        uploads the bunch group set, and produces the upload record file.

        Parameters:
            textString (str): the string of the bunch group set JSON file.
            comment (str): the comment to be included with the upload.
        Returns:
            dbkey (int): The bunch group set key of the upload.
            uploadRecord (dict): the upload record (that has been also saved as a JSON file)
        """
        self._filename = filename
        if bgset is None:
            byteString: bytes = b""
            if filename is not None:
                with open(filename, "rb") as fh:
                    byteString=fh.read()
            elif textString is not None:
                byteString=bytes(textString, 'utf-8')
            try:
                _bgset:dict[str,Any] = json.loads(byteString)
            except ValueError as err:
                log.error(f"The provided file or string can not be interpreted as json.")
                log.error(err)
                raise
        else:
            _bgset = bgset         

        filetype: str = _bgset["filetype"] if 'filetype' in _bgset else 'missing'
        if filetype != self.expected_filetype:
            msg = f"The provided information has filetype '{filetype}', but should have filetype '{self.expected_filetype}'."
            log.error(msg)
            raise RuntimeError(msg)

        # Upload bunch group set
        self._uploadBunchGroupSet(bgset=_bgset, comment=comment)

        # Create upload record
        self.createUploadRecord()
        return self._key, self._upload_record

    def recordExists(self, bgset: dict[str,Any]) -> bool:
        self._is_duplicate = False
        self._key = 0 
        record: dict[str,Any] = {}
        bgsetHash: str = getDictHash(bgset)

        fullFields: list[str] = [f'{self.prefix}_{f}' for f in self.fields]
        query: str = f"SELECT {', '.join(fullFields)} FROM {self.dbschema}.{self.table} WHERE {self.prefix}_HASH='{bgsetHash}'"
        with self.cursor() as cursor:
            cursor.execute(query)
            row = cursor.fetchone()
        if row is not None:
            # Duplicate found, return index and record of existing entry
            record: dict[str,Any] = dict(zip(self.fields, row))
            log.info(f"The provided bunch group set with name {self.name} is already in the database with index {record['ID']}, it was created {record['MODIFIED_TIME']}.")
            log.info(f"No upload will take place, instead the existing index {record['ID']} points to the entry.")
            self._is_duplicate = True
            self._key = int(record['ID'])
            self._dbrecord = record
        return self._is_duplicate

    def _uploadBunchGroupSet(self, bgset: dict[str, Any], comment: str) -> None:
        """
        Upload the bunch group set and return the index. If the bunch group set is found
        to be a duplicate, return the existing bunch group set index instead.

        Parameters:
            bgs (str): The filename of the bunch group set JSON file.
            comment (str): The comment to be included with the upload.

        Returns:
            int: the index of the uploaded bunch group set/existing bunch group set.
            dict[str,Any]: corresponding database record 
        """

        self._name = bgset["name"]
        self._nbunches: int = sum([train['length'] for train in bgset["bunchGroups"]["BGRP1"]["bcids"]])

        # check if record exists
        if self.recordExists(bgset=bgset):
            return

        bgsetHash: str = getDictHash(bgset)
        bgset_str: str = json.dumps(bgset, sort_keys = True, indent=4, separators=(",", ": "))
        blob = self.getBlobFromString(bgset_str)

        # Set variables required for SQL query
        newIdx: int = self.getLastTableRecord()[0] + 1
        comment = self.parseComment(comment, max_length=200)
        newVersion = self.getLastVersionOfName(self.name) + 1
        user = os.environ["USER"][:50]
        partition: int = 0
        isHidden: int = 0

        fields: list[str] = ["ID", "NAME", "DATA", "HASH", "VERSION", "COMMENT", "USERNAME", "MODIFIED_TIME", "PARTITION", "HIDDEN"]
        values: str = f"{newIdx}, :rec_name, :rec_blob, '{bgsetHash}', {newVersion}, :rec_comment, '{user}', CURRENT_TIMESTAMP, {partition}, {isHidden}"
        bindvars ={
            "rec_name": self.name,
            "rec_blob": blob,
            "rec_comment": comment
        }
        field_str: str = ', '.join([f"{self.prefix}_{field}" for field in fields])
        query: str = f"INSERT INTO {self.dbschema}.{self.table}({field_str}) VALUES({values})"
        
        with self.cursor() as cursor:
            cursor.execute(query, **bindvars)
            cursor.connection.commit()

        # Get and return the index of the new entry that has been created
        self._key, self._dbrecord = self.getLastTableRecord()
        log.info(f"Inserted bunch group set {self.name} (v{newVersion}) with index {self._dbrecord}")
        return

    def createUploadRecord(self) -> None:
        """
        Write a record of the upload (json file)

        Args:
            dbrecord (dict[str,Any]): the uploaded db entry

        Returns:
            dict[str,Any]: the upload record that also has been saved as a JSON file.
        """

        self._upload_record = {
            "bgsk" : self._key,
            "nfilled": self._nbunches,
            "history": "existed" if self._is_duplicate else "new",
            "filetype" : self.expected_filetype,
            "username": self.getUser(),
            "exectime": self.exec_time_str,
            "dbrecord": self._dbrecord
        }

        # Set filename based on the upload time and write json file with upload record
        outfn: str = f"upload_record_bgs_{self.exec_time_str}.json"
        with open(outfn, "w") as fp:
            json.dump(self._upload_record, fp, indent=4, cls=DateTimeEncoder)
        log.info(f"Created {outfn}")

    def print_record(self) -> None:
        print("Key Summary:")
        print(" BGSK | Comment")
        print("---------------")
        print(f"{self._key:5} | {self._dbrecord['COMMENT']}")


if __name__=="__main__":
    bgsk = 2861
    with BunchGroupSetUploader("TRIGGERDBDEV2_I8") as uploader:
        uploader.upload("BunchGroupSet_2861.json", comment="Upload of bg set 2861 from run 3 trigger DB")
