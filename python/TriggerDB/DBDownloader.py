#!/usr/bin/env python3

from __future__ import annotations
import json
import os
import re
from typing import Any, Optional, TypeVar, Union
import oracledb

from abc import ABC, abstractmethod

from TriggerDB.Logger import log
from TriggerDB.DBConnection import DBConnection

JsonData = TypeVar('JsonData', bound=dict[str, Any])

class DBDownloader(ABC):
    # table name
    @property
    @abstractmethod
    def table(self) -> str: ...

    # common prefix for all fields
    @property
    @abstractmethod
    def prefix(self) -> str: ...

    # download query
    @abstractmethod
    def query(self) -> str:...

class DBDownloaderBase(DBDownloader):

    def __init__(self, dbalias: str = "", *, existingConnection: Optional[DBConnection] = None) -> None:
        """
        Input
        -----
        dbalias: str the alias of the trigger DB connection
    
        Attributes
        ----------
        """
        self._downloaded_data: Optional[dict[Union[int,str],Any]] = None
        self._db_schema_version: Optional[int] = None
        self._ownConnection: bool = existingConnection is None

        if existingConnection is None:
            self.dbconnection: DBConnection = DBConnection(dbalias)
        else:
            # use existing connection if provided
            if existingConnection.readwrite:
                raise RuntimeError("DB downloads should only be done with a read-only connection")
            self.dbconnection = existingConnection

    def __enter__(self):
        return self
    
    def __exit__(self, a, b, traceback) -> None:
        if self._ownConnection:
            self.dbconnection.close()

    def query(self) -> str:
        return f"SELECT {self.prefix}_DATA FROM {self.table} WHERE {self.prefix}_ID=:key"

    def cursor(self) -> oracledb.Cursor:
        return self.dbconnection.cursor()

    @property
    def dbschema(self) -> str:
        return self.dbconnection.dbschema

    def writeFile(self, fn: str, *, data: Optional[str] = None) -> None:
        """
        Write json data to file. If data is not provided as argument, write the internally stored data

        Parameters:
            data (str): the data to be stored to file.
            fn (str): the name of the output file.
        """
        if data is None and self._downloaded_data is None:
            return
        
        outdir = os.path.dirname(fn)
        if outdir != "":
            os.makedirs(outdir, exist_ok=True)

        if data is not None:
            with open(fn, "w") as fp:
                if type(data)==dict:
                    json.dump(data, fp, indent=4, separators=(",", ": "))
                else:
                    fp.write(data)
                log.info(f"Wrote file {fn}")
        elif self._downloaded_data is not None:
            with open(fn, "w") as fp:
                json.dump(self._downloaded_data, fp, indent=4, separators=(",", ": "))
                log.info(f"Wrote file {fn}")

    def download(self, key: int) -> dict[Union[int,str],Any]:
        """
        Download the json data for a given key and stores it in self._downloaded_data

        Args:
            key (int): The ID for the table entry

        Raises:
            RuntimeError: when key does not exist in table or the _DATA entry is null

        Returns:
            dict[str,Any]: the json data dict
        """
        def prefetch_blobs(cursor, name, defaultType, size, precision, scale):
            if defaultType == oracledb.DB_TYPE_BLOB:
                return cursor.var(oracledb.BLOB, arraysize=cursor.arraysize)

        with self.cursor() as cursor:
            # cursor.outputtypehandler = prefetch_blobs
            cursor.execute(self.query(), key=key )
            if (result:=cursor.fetchone()) is None:
                raise RuntimeError(f"The provided key {key} is invalid for table {self.table}")
            self._downloaded_data = result[0]
        if self._downloaded_data is None:
            raise RuntimeError(f"For the provided key {key} in {self.table} the data is None")
        return self._downloaded_data

    def getSchemaVersion(self) -> int:
        if self._db_schema_version is not None:
            return self._db_schema_version
        with self.cursor() as cursor:
            query: str = f"SELECT ts_tag, ts_modified_time FROM trigger_schema ORDER BY ts_modified_time"
            cursor.execute(query)
            res = cursor.fetchall()
            if len(res) == 0: # trigger schema does not exist
                raise RuntimeError("ERROR: no trigger schema found")

        schemaString = str(res[-1][0]) # get the latest entry
        self._db_schema_version = -1
        if (m := re.match(r".*?(\d*)$",schemaString)) is not None:
            self._db_schema_version = int(m.groups()[0])
        log.info(f"Trigger schema version is {self._db_schema_version} (extracted from {schemaString})")
        return self._db_schema_version

    def getLinkedIDsFromSMK(self, smk: int) -> tuple[Optional[int], Optional[int], Optional[int]]:
        """
        Get the L1 menu, HLT menu, and HLT joboptions keys from the SMK.

        Parameters:
            smk (int): The super-master key for which files are to be extracted.

        Returns:
            tuple of
            int | None: L1 menu table entry id, or None, if none is attached
            int | None: HLT menu table entry id, or None, if none is attached
            int | None: Job option table entry id, or None, if none is attached
        """
        # Get L1 menu, HLT menu, and HLT joboptions keys from a SMK
        fields: list[str] = ["SMT_NAME", "SMT_VERSION", "SMT_L1_MENU_ID", "SMT_HLT_MENU_ID", "SMT_HLT_JOBOPTIONS_ID"]
        query: str = (
            f"SELECT {', '.join(fields)} FROM SUPER_MASTER_TABLE WHERE SMT_ID={smk}"
        )
        with self.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()

        if len(result)==0:
            raise RuntimeError(f"The provided SMK {smk} does not exist in the database {self.dbconnection.dbalias}")

        menuName, menuVersion, l1menuId, hltmenuId, joId = result[0]

        if l1menuId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no L1 menu attached.")
        if hltmenuId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no HLT menu attached.")
        if joId is None:
            log.info(f"SMK {smk} ({menuName} v{menuVersion}) has no job options attached.")

        return l1menuId, hltmenuId, joId

