

# Download menu, precales, bunchgroupset, and mongroups (from smk) 
# from the Run 3 Trigger DB and store it in a local directory
# source taken from run 477048, lb 54 on 4 June 2024

SOURCE_DB=TRIGGERDB_RUN3
DOWNLOAD_DIR="run_477048_lb_54"

SOURCE_SMK=3347
SOURCE_BGSK=2861
SOURCE_L1PSK=13595
SOURCE_HLTPSK=10454
SOURCE_L1PSK2=13596
SOURCE_HLTPSK2=10455

DOWNLOAD_PARAM=" --outdir ${DOWNLOAD_DIR}"

(

    testdir=download_test
    rm -rf ${testdir}
    mkdir -p ${testdir}
    cd ${testdir}

    extractMenu.py --db ${SOURCE_DB} --smk ${SOURCE_SMK} ${DOWNLOAD_PARAM}
    RETURN=$?
    echo -n "extractMenu.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    extractBunchGroupSet.py --db ${SOURCE_DB} --bgsk ${SOURCE_BGSK} ${DOWNLOAD_PARAM}
    RETURN=$?
    echo -n "extractBunchGroupSet.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    extractPrescales.py --db ${SOURCE_DB} --l1psk ${SOURCE_L1PSK} --hltpsk ${SOURCE_HLTPSK} ${DOWNLOAD_PARAM}
    RETURN=$?
    echo -n "extractPrescales.py L1 and HLT ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    extractPrescales.py --db ${SOURCE_DB} --l1psk ${SOURCE_L1PSK}
    RETURN=$?
    echo -n "extractPrescales.py L1 only ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    extractPrescales.py --db ${SOURCE_DB} --hltpsk ${SOURCE_HLTPSK}
    RETURN=$?
    echo -n "extractPrescales.py HLT only ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    extractMonitoringGroups.py --db ${SOURCE_DB} --smk ${SOURCE_SMK} ${DOWNLOAD_PARAM}
    RETURN=$?
    echo -n "extractMonitoringGroups.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo
 
)