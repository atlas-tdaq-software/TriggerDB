#!/usr/bin/env python

import json
from pprint import pprint
from typing import Any, Union
import oracledb

oracledb.init_oracle_client()  # Initializes Oracle Client libraries (thick mode)
print(f"{oracledb.is_thin_mode()=}")

def main():

    upload = False
    keep_lob_type = False

    connection = oracledb.connect("atlas_trig_stelzer/ruq.c0612@devdb19u")

    if upload:
        data = {"Hello": "Brave new World"}
        json_blob = json.dumps(data, sort_keys = True, indent=4, separators=(",", ": ")).encode("utf-8")
        query = "INSERT INTO test_blob(ID, BLOB_DATA) VALUES (1, :myblob)"
        bindvars = {"myblob": json_blob}
        cursor = connection.cursor()
        cursor.execute(query, bindvars)
        cursor.connection.commit()

    def prefetch_blobs(cursor, name, defaultType, size, precision, scale):
        if defaultType == oracledb.DB_TYPE_BLOB:
            return cursor.var(oracledb.BLOB, arraysize=cursor.arraysize)

    # Fetch the BLOB
    cursor = connection.cursor()
    if keep_lob_type:
        cursor.outputtypehandler = prefetch_blobs
    cursor.execute("SELECT blob_data FROM test_blob WHERE id = 2")
    result: Union[oracledb.LOB, dict[str,Any]] = cursor.fetchone()[0]
    print(type(result))  # with oracledb in thick mode: 'oracledb.LOB' for BLOB except if JSON constraint, then dict
    if type(result) == oracledb.LOB:
        data= json.loads(result.read())
    else:
        data = result
    pprint(data)

if __name__=="__main__":
    main()