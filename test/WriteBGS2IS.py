#!/usr/bin/env python

import argparse
import csv
from datetime import datetime
from itertools import groupby
import json
import os
import sys
import time
from typing import Optional
from ispy import * # type: ignore
from ipc import IPCPartition # type: ignore

def createBunchgroupset(filename):
    with open(filename, "rt") as fh:
        bgs = json.load(fh)
    return bgs

def createBunchgroupISObjects(partition, bgs, objName, writeBunchgroupKey):

    p = IPCPartition(partition)

    if not p.isValid():
        print(f"Partition {partition} is not valid")
        sys.exit(1)

    # defined in https://gitlab.cern.ch/atlas-tdaq-software/TTCInfo/-/blob/master/data/ttc_is_info.xml
    # https://gitlab.cern.ch/atlas-tdaq-software/ispy/-/blob/master/python/ispy/__init__.py#L147

    bgs_is = ISObject(p, f'Monitoring.{objName}', 'TrigConfL1BunchGroups') # type: ignore
    bgs_is["BunchGroupSet"] = bgs["name"]
    for bginfo in bgs['bunchGroups'].values():
        name, id, bcids = map(bginfo.get, ['name', 'id', 'bcids'])
        bgs_is[f"Name_Group_{id}"] = name
        for train in bcids:
            bgs_is[f"BCID_Group_{id}"] += range(train['first'], train['first'] + train['length'])
    bgs_is.checkin()
    print(f"Wrote bunchgroup set to partition {partition}, server Monitoring.{objName} and type TrigConfL1BunchGroups")

    if writeBunchgroupKey:
        bgk_is = ISObject(p, 'RunParams.TrigConfL1BgKey', 'TrigConfL1BgKey') # type: ignore
        bgk_is["L1BunchGroupKey"] = 42
        bgk_is["L1BunchGroupComment"] = "Test upload from file"
        bgk_is.checkin()

        print(f"Wrote bunchgroup key to partition {partition}, server RunParams, name TrigConfL1BgKey and type TrigConfL1BgKey")

def mockBunchgroupFromFillingScheme(fsFileName):

    def normalize(bcids):
        """ turn list of bunches into trains """
        normalized = []
        bunches = sorted(list(set(bcids))) # uniq and sort
        if any(map(lambda bcid : bcid<0 or bcid >= 3564, bunches)):
            raise RuntimeError("Found bcid outside range 0..3563 in bunchgroup")
        for k,g in groupby(enumerate(bunches), lambda x : x[1]-x[0]):
            train = list(g)
            normalized += [ (train[0][1], len(train)) ]
        return normalized

    bgsname = os.path.basename(os.path.realpath(filename=fsFileName)).rsplit('.',1)[0]


    with open(fsFileName, "rt") as fh:
        if fsFileName.endswith(".csv"):
            bunchArray = {
                "beam1":3564*[0],
                "beam2":3564*[0]
            }
            with open(fsFileName, "rt") as fh:
                reader = csv.reader(fh)
                for ring in [1,2]:
                    for line in reader:
                        if line and line[0].startswith(f"B{ring} bucket number"):
                            break
                    for line in reader:
                        if len(line)==0:
                            break
                        fields = [v.lstrip('\t') for v in line]
                        bunch = (int(fields[0])-1)//10+1
                        bunchArray[f"beam{ring}"][bunch]=1

        elif fsFileName.endswith(".json"):
            bunchArray = json.load(fh)
        else:
            raise RuntimeError(f"Can't interpret information in file {fsFileName} as a filling scheme")

        filledBunches = [bc for bc,bunchpair in enumerate(zip(bunchArray["beam1"], bunchArray["beam2"])) if bunchpair==(1,1)]



    trains = normalize(filledBunches)
    bgs = {}
    bgnames = [
        "BCRVeto", "Paired", "CalReq", "Empty", 
        "IsolatedUnpaired", "NonIsolatedUnpaired", "EmptyBeforeAfterPaired", "InTrain",
        "FirstInTrain", "AfterGlow", "ALFA", "EmptyBeforePaired",
        "AllWithoutCalreq", "UnpairedBeam1", "UnpairedBeam2", "BG15"
    ]
    bgs["BGRP0"] = {
        "name": bgnames[0],
        "id": 0,
        "info": "3543 bunches, 2 groups",
        "bcids": [{"first": 0,"length": 3540},{"first": 3561,"length": 3}]
    }
    bgs["BGRP1"] = {
        "name" : bgnames[1],
        "id"   : 1,
        "info" : "%i bunches, %i groups" % (len(filledBunches), len(trains)),
        "bcids": [ {"first": first, "length": length} for (first,length) in trains]
    }
    for bgid in range(2,16):
        bgs[f"BGRP{bgid}"] = {
            "name" : bgnames[bgid],
            "id"   : bgid,
            "info" : "0 bunches, 0 groups",
            "bcids": []
        }
    bgset = {
        "name": f"LHCFS_{bgsname}",
        "filetype": "bunchgroupset",
        "bunchGroups": bgs
        }
    return bgset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p', '--partition', dest="partition", default=None,
        help="Partition name"
    )
    parser.add_argument(
        '-f', '--file', dest="file", required=True,
        help="Read bunchgroup set from json file and store in IS"
    )
    parser.add_argument(
        '--target', dest="target", default="lhc",
        help="Specify 'file' or 'lhc'. IS location Monitoring.BunchGrouperApp/BunchGroups (lhc) or Monitoring.BunchGrouperApp_FS/BunchGroups_FS (file)"
    )
    parser.add_argument(
        '--loop', dest="loop", action="store_true", default=False,
        help="Continued runnning. Uploads a new bunchgroup to IS whenever the file is updated. One needs to specify '--target file' "
    )
    cmdline = parser.parse_args()

    if cmdline.partition is None:
        try:
            cmdline.partition = os.environ["TDAQ_PARTITION"]
        except KeyError:
            print("Error: please set env TDAQ_PARTITION or specify as parameter -p partion")
            return 1

    if cmdline.loop and cmdline.target == "lhc":
        print("With option '--loop' one needs to specify '-- target file'")
        parser.print_help()
        return 1

    if cmdline.target == "lhc":
        isObjName = "BunchGrouperApp/BunchGroups"
    elif cmdline.target == "file":
        isObjName = "BunchGrouperApp_FS/BunchGroups_FS"
    else:
        parser.print_help()
        print(f"Could not interpret argument --target {cmdline.target}. Will do nothing")
        return 1

    if cmdline.loop:
        # keep running in a loop to upload whenever the file changes

        # get initial timestamp or None if file not exist
        timestamp: Optional[float] = None
        if os.path.exists(cmdline.file):
            timestamp = os.stat(cmdline.file).st_ctime
            print(f"Waiting for file {cmdline.file} to change")
        else:
            print(f"Waiting for file {cmdline.file} to appear")

        # create an empty object
        bgs_is = ISObject(cmdline.partition, f'Monitoring.{isObjName}', 'TrigConfL1BunchGroups') # type: ignore
        bgs_is.checkin()
        
        # start waiting
        while True:
            uploadRequired = False
            if os.path.exists(cmdline.file):
                newTimestamp = os.stat(cmdline.file).st_ctime
                if newTimestamp != timestamp:
                    uploadRequired = True
                    timestamp = newTimestamp
            if uploadRequired and timestamp is not None:
                modification = datetime.fromtimestamp(timestamp)
                print(f"Will write to IS since file was modified at {modification}")
                bgset = mockBunchgroupFromFillingScheme(cmdline.file)
                createBunchgroupISObjects(cmdline.partition, bgset, isObjName, writeBunchgroupKey = False)

            time.sleep(2)

    else:
        # read and upload ones
        bgset = createBunchgroupset(cmdline.file)
        createBunchgroupISObjects(cmdline.partition, bgset, isObjName, writeBunchgroupKey = True)

    return 0

if __name__=="__main__":
    sys.exit(main())
