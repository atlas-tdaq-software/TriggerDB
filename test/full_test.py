#!/usr/bin/env python

import argparse
from contextlib import chdir
import json
import os
from pathlib import Path
import subprocess as sp
import sys
from typing import Any, Callable, Literal

source_db="TRIGGERDB_RUN3"
dest_dbr="TRIGGERDB_STELZER_R"
dest_dbw="TRIGGERDB_STELZER_W" # to use a non-official db, one needs to set
                              # CORAL_AUTH_PATH_DEV to a path containing the extended authentication files
exec_dir: str = "dbiotest"
download_dir: str = "download"
download_dir2: str = "download2"
vlevel: int = 0
logarg: str = "-l INFO"
dry_run: bool = False
print_commands: bool = False

def print_status(success: bool, testname: str, command: str) -> None:
    global dry_run
    global print_commands
    cmdstr = ""
    if print_commands:
        cmdstr = f" ({command})"
    if dry_run:
        return
    if success:
        print(f"{testname} ==> \033[92mSUCCESS\033[0m{cmdstr}")
    else:
        print(f"{testname} ==> \033[1m\033[91mFAILURE\033[0m{cmdstr}")


def execute_script(test_command:str, execution_dir:str = "") -> bool:
    """
    Execute a script on a remote machine via SSH after creating a directory for execution.

    Args:
        remote_script_path (str): The full path to the script on the remote machine.
        remote_execution_dir (str): The directory to create for script execution.

    Returns:
        bool: True if command executed successfully
    """
    global dry_run
    if dry_run:
        print(test_command)
        return True
    
    if execution_dir == "":
        global exec_dir
        execution_dir = exec_dir

    os.makedirs(execution_dir,exist_ok=True)
    with chdir(execution_dir):
        try: 
            # Run the SSH command
            result: sp.CompletedProcess[str] = sp.run(
                test_command, 
                shell=True, 
                capture_output=(vlevel == 0), # if vlevel = 0 no output on screen (clean test, except errors)
                text=True
            )
                    
            # Check if the command was successful
            if result.returncode != 0:
                print(f"Command executed with return code {result.returncode}\n{test_command}")
                print(result.stdout.strip())
                print(result.stderr.strip())
            return result.returncode == 0
        except Exception as e:
            print(f"\033[1m\033[91mException\033[0m: {str(e)}")
            return False


def get_last_upload_record_file(path: str, base:Literal['menu', 'bgs', 'prescales', 'mongroups', 'alias']) -> Path:
    record_list: list[Path] = []
    for file in Path(path).iterdir():
        if file.name.startswith(f"upload_record_{base}"):
            s: os.stat_result = file.stat()
            t = s.st_ctime
            record_list += [ file ]
    if not record_list:
        raise RuntimeError(f"No '{base}' upload record found.")
    last_record_file: Path = sorted(record_list, key=lambda f: f.stat().st_ctime)[-1] # sort by creation time and get the last one
    return last_record_file


def get_last_upload_record(base:Literal['menu', 'bgs', 'prescales', 'mongroups', 'alias']) -> dict:
    upload_record = get_last_upload_record_file(exec_dir, base)
    with upload_record.open() as fp:
        upload_record = json.load(fp)
    return upload_record


def test_menu_io(source_keys: dict[str,int]) -> bool:
    # 1st download from run 3 DB
    test: str = (
        f"trgdb_menu_download.py --db {source_db} --smk {source_keys['smk']}"
        f" --outdir {download_dir} {logarg}"
    )
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  menu download", test)
    if not success:
        return False

    # upload
    l1menu: str = f"{download_dir}/L1Menu_{source_keys['smk']}.json"
    hltmenu: str = f"{download_dir}/HLTMenu_{source_keys['smk']}.json"
    hltjo: str = f"{download_dir}/HLTJO_{source_keys['smk']}.json"
    hltmon = f"{download_dir}/MonGroups_smk{source_keys['smk']}.json"
    test: str = (
        f"trgdb_menu_upload.py --db {dest_dbw} -l1 {l1menu}"
        f" -hlt {hltmenu} -jo {hltjo} -mon {hltmon} {logarg}"
        f" --comment \'Test menu upload script with origin from Run 3 SMK {source_keys['smk']}\'"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  menu upload", test)
    if not success:
        return False

    # 2nd download
    upload_record2 = get_last_upload_record('menu')
    test: str = (
        f"trgdb_menu_download.py --db {dest_dbr} --smk {upload_record2['smk']}"
        f" --outdir {download_dir2} {logarg}"
    )
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  second menu download", test)
    if not success:
        return False

    # Comparison of 1st and 2nd download
    smk: int = source_keys['smk']
    smk2: int = upload_record2['smk']
    diff_cmd_template: str = f"diff {download_dir}/{{stem}}{smk}.json {download_dir2}/{{stem}}{smk2}.json"
    for stem in ["L1Menu_", "HLTMenu_", "HLTJO_", "MonGroups_smk"]:
        diff_cmd = diff_cmd_template.format(stem=stem)
        success_cmp: bool = execute_script(diff_cmd, exec_dir)
        print_status(success_cmp, f"  comparison of files {stem}*", diff_cmd)
        success &= success_cmp
    return success


def test_bunchgroup_io(source_keys: dict[str,int]) -> bool:

    bgsk_orig: int = source_keys['bgsk']

    # download
    test: str = (
        f"trgdb_bunchgroupset_download.py --db {source_db} --bgsk {bgsk_orig}"
        f" --outdir {download_dir} {logarg}"
    )
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  bunchgroupset download", test)
    if not success:
        return False

    # upload
    bgs_file_orig: str = f"{download_dir}/BunchGroupSet_{bgsk_orig}.json"
    test: str = (
        f"trgdb_bunchgroupset_upload.py --db {dest_dbw} --bgs {bgs_file_orig} {logarg}"
        f" --comment \'Test bunchgroup upload script with origin from Run 3 BGK {bgsk_orig}\'"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  bunchgroupset upload", test)
    if not success:
        return False

    # 2nd download
    upload_record = get_last_upload_record('bgs')
    test: str = (
        f"trgdb_bunchgroupset_download.py --db {dest_dbr} --bgsk {upload_record['bgsk']}"
        f" --outdir {download_dir2} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second bunchgroupset download", test)
    if not success:
        return False
    
    # Comparison of 1st and 2nd download
    bgsk1: int = upload_record['bgsk']
    diff_cmd: str = f"diff {download_dir}/BunchGroupSet_{bgsk_orig}.json {download_dir2}/BunchGroupSet_{bgsk1}.json"
    success = execute_script(diff_cmd, exec_dir)
    print_status(success, f"  comparison of BunchGroupSet files", diff_cmd)

    # second upload
    bgs_file_orig: str = f"{download_dir}/BunchGroupSet_{bgsk_orig}.json"
    test: str = (
        f"trgdb_bunchgroupset_upload.py --db {dest_dbw} --bgs {bgs_file_orig} {logarg}"
        f" --comment \'Test second bunchgroup upload script with origin from Run 3 BGK {bgsk_orig}\'"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second bunchgroupset upload", test)
    if not success:
        return False

    # comparison of first and second upload keys
    upload_record2 = get_last_upload_record('bgs')
    bgsk2 = upload_record2['bgsk']
    success = (bgsk1 == bgsk2) and (upload_record2['history'] == "existed")
    print_status(success, "  second upload identical", "internal comparison")
    if not success:
        return False

    # modify bunchgroup and upload again
    with chdir(exec_dir):
        with open(bgs_file_orig) as fp:
            bgs = json.load(fp)
        bgs["bunchGroups"]["BGRP1"]["bcids"][0]["length"] += 1
        with open(bgs_file_orig, 'w') as fp:
            json.dump(bgs, fp, indent=4)
    test: str = (
        f"trgdb_bunchgroupset_upload.py --db {dest_dbw} --bgs {bgs_file_orig} {logarg}"
        f" --comment \'Third bunchgroup upload with modified BGRP1\'"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  third bunchgroupset upload with modified bgrp1", test)
    if not success:
        return False

    # compare upload keys
    upload_record3 = get_last_upload_record('bgs')
    bgsk3: int = upload_record3['bgsk']
    success = (bgsk1 != bgsk3)
    print_status(success, "  third upload results in different bgs key", "internal comparison")
    if not success:
        return False

    return success


def test_prescales_io(source_keys: dict[str,int]) -> bool:
    # download
    test: str = (
        f"trgdb_prescales_download.py --db {source_db} --l1psk {source_keys['l1psk']}"
        f" --hltpsk {source_keys['hltpsk']} --outdir {download_dir} {logarg}"
    )
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  prescale sets download", test)
    if not success:
        return False

    # upload
    uploaded_smk: int = get_last_upload_record('menu')['smk']
    l1ps: str = f"{download_dir}/L1Prescale_{source_keys['l1psk']}.json"
    hltps: str = f"{download_dir}/HLTPrescale_{source_keys['hltpsk']}.json"
    test: str = (
        f"trgdb_prescales_upload.py --db {dest_dbw} --smk {uploaded_smk}"
        f" --l1ps {l1ps} --hltps {hltps} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  prescale sets upload", test)
    if not success:
        return False

    # 2nd download
    upload_record = get_last_upload_record('prescales')
    l1psk2: int = upload_record['l1']['key']
    hltpsk2: int = upload_record['hlt']['key']
    test: str = (
        f"trgdb_prescales_download.py --db {dest_dbr} --l1psk {l1psk2} --hltpsk {hltpsk2}"
        f" --outdir {download_dir2} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second prescale download", test)
    if not success:
        return False
    
    # Comparison of 1st and 2nd download
    l1psk: int = source_keys['l1psk']
    hltpsk: int = source_keys['hltpsk']
    l1_diff_cmd: str = f"diff {download_dir}/L1Prescale_{l1psk}.json {download_dir2}/L1Prescale_{l1psk2}.json"
    hlt_diff_cmd: str = f"diff {download_dir}/HLTPrescale_{hltpsk}.json {download_dir2}/HLTPrescale_{hltpsk2}.json"
    l1_diff_success = execute_script(l1_diff_cmd, exec_dir)
    print_status(l1_diff_success, f"  comparison of L1Prescale files", l1_diff_cmd)
    success &= l1_diff_success
    hlt_diff_success = execute_script(hlt_diff_cmd, exec_dir)
    print_status(hlt_diff_success, f"  comparison of HLTPrescale files", hlt_diff_cmd)
    success &= hlt_diff_success

    return success


def test_mongroups_io(source_keys: dict[str,int]):
    # download
    test: str = (
        f"trgdb_mongroups_download.py --db {source_db} --smk {source_keys['smk']}"
        f" --outdir {download_dir} {logarg}"
    )
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  mongroups download", test)
    if not success:
        return False

    # upload
    uploaded_smk: int = get_last_upload_record('menu')['smk']
    mongroup: str = f"{download_dir}/MonGroups_smk{source_keys['smk']}.json"
    test: str = (
        f"trgdb_mongroups_upload.py --db {dest_dbw} --smk {uploaded_smk} --mongroup {mongroup} {logarg}"
        f" --comment \'Test mongroup upload script with origin from Run 3 SMK {source_keys['smk']}\'"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  mongroups upload", test)
    if not success:
        return False

    # 2nd download
    upload_record = get_last_upload_record('mongroups')
    smk2: int = upload_record['smk']
    test: str = (
        f"trgdb_mongroups_download.py --db {dest_dbr} --smk {smk2}"
        f" --outdir {download_dir2} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second mongroups download", test)
    if not success:
        return False

    # Comparison of 1st and 2nd download
    smk: int = source_keys['smk']
    diff_cmd: str = f"diff {download_dir}/MonGroups_smk{smk}.json {download_dir2}/MonGroups_smk{smk2}.json"
    success = execute_script(diff_cmd, exec_dir)
    print_status(success, f"  comparison of mongroups files", diff_cmd)

    return success


def test_l1ctfiles_io(source_keys: dict[str,int]):
    # download
    test: str = (
        f"trgdb_l1ctfiles_download.py --db {source_db} --smk {source_keys['smk']}"
        f" --outdir {download_dir}/l1ctfiles {logarg}"
    )
    if vlevel>0:
        print(test)
    success: bool = execute_script(test, exec_dir)
    print_status(success, "  L1CT files download", test)
    if not success:
        return False

    # upload
    uploaded_smk: int = get_last_upload_record('menu')['smk']
    test: str = (
        f"trgdb_l1ctfiles_upload.py --db {dest_dbw} --smk {uploaded_smk}"
        f" --topdir {download_dir}/l1ctfiles {logarg}"
    )
    if vlevel>0:
        print(test)
    success = execute_script(test, exec_dir)
    print_status(success, "  L1CT files upload", test)
    if not success:
        return False

    return success


def diff_alias_files(file1:str, file2:str, exclude_psks:bool = True) -> bool:
    with open(file1) as fp1:
        data1 = json.load(fp1)
    with open(file2) as fp2:
        data2 = json.load(fp2)
    if exclude_psks:
        for data in (data1, data2): # remove prescale keys before comparison
            for x in data["aliasrows"]:
                x.pop('l1psk', None) 
                x.pop('hltpsk', None)
    return data1 == data2

def test_alias_io(alias_keys, orig_keys: dict[str,int]) -> bool:
    global dry_run
    # download prescales
    upload_bgs_record = get_last_upload_record('bgs')
    uploaded_bgk: int = upload_bgs_record['bgsk']
    uploaded_bgs_nb = upload_bgs_record['nfilled']
    alias_info: dict[str,Any] = {
        "filetype": "alias",
        "type": "PHYSICS",
        "name": f"PHYSICS_BGRP{uploaded_bgk}",
        "comment": f"PHYSICS for BGRP {uploaded_bgk}. From run {orig_keys['run']}, run 3, bgk {orig_keys['bgsk']}",
        "bgk": uploaded_bgk,
        "aliasrows": []
    }
    success: bool = True
    lasttest = ""
    for l1psk, hltpsk, lmin, lmax in alias_keys:
        test: str = (
            f"trgdb_prescales_download.py --db {source_db} --l1psk {l1psk} --hltpsk {hltpsk}"
            f" --outdir {download_dir}/alias {logarg}"
        )
        lasttest = test
        success = execute_script(test, exec_dir)
        if dry_run:
            continue
        if not success:
            break
        # rename prescales files according to convention
        l1 = Path(f"{exec_dir}/{download_dir}/alias/L1Prescale_{l1psk}.json")
        hlt = Path(f"{exec_dir}/{download_dir}/alias/HLTPrescale_{hltpsk}.json")
        l1new = l1.with_stem(f"L1Prescale_{lmin}_{lmax}e32_{uploaded_bgs_nb}b")
        hltnew = hlt.with_stem(f"HLTPrescale_{lmin}_{lmax}e32_{uploaded_bgs_nb}b")
        l1.rename(l1new)
        hlt.rename(hltnew)
        alias_info['aliasrows'] += [{
            "lmin": lmin,
            "lmax": lmax,
            "lbase": "e32",
            "l1file": l1new.name,
            "hltfile": hltnew.name
        }]
    print_status(success, "  alias prescales download", lasttest)
    if not success:
        return False

    # write alias file
    with Path(f'{exec_dir}/{download_dir}/alias/Alias_PHYSICS.json').open('w') as fp:
        json.dump(alias_info, fp, indent=4)

    # upload
    uploaded_smk: int = get_last_upload_record('menu')['smk']
    alias_dir: str = f"{download_dir}/alias"
    test: str = (
        f"trgdb_alias_upload.py --db {dest_dbw} --smk {uploaded_smk} -d {alias_dir} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  alias upload", test)
    if not success:
        return False

    # download
    test: str = (
        f"trgdb_alias_download.py --db {dest_dbr} --smk {uploaded_smk} --download-prescales"
        f" -o {download_dir2}/alias {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second alias download", test)

    # Comparison of created/uploaded with downloaded
    upload_alias_record = get_last_upload_record('alias')
    alias_key: str = upload_alias_record['key']
    alias_type: str = upload_alias_record['type']
    alias_name: str = upload_alias_record['name']
    file1: str = f"{download_dir}/alias/Alias_PHYSICS.json"
    file2: str = f"{download_dir2}/alias/Alias_{alias_type}_name{alias_name}_ak{alias_key}.json"
    with chdir(exec_dir):
        success = diff_alias_files(file1, file2)
    print_status(success, f"  comparison of alias files", "internal comparison")

    # second upload
    test: str = (
        f"trgdb_alias_upload.py --db {dest_dbw} --smk {uploaded_smk} -d {alias_dir} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  second alias upload", test)
    if not success:
        return False

    # check for identical upload keys
    upload_alias_record2 = get_last_upload_record('alias')
    akey1 = upload_alias_record['key']
    akey2 = upload_alias_record2['key']
    success = (akey1 == akey2) and (upload_alias_record2['history'] == "existed")
    print_status(success, "  second upload identical", "internal comparison")
    if not success:
        return False

    # modify single prescale and upload again
    with chdir(exec_dir):
        with open(f"{download_dir}/alias/Alias_PHYSICS.json") as fp:
            l1file: str = json.load(fp)["aliasrows"][0]["l1file"]
        with open(f"{download_dir}/alias/{l1file}") as fp:
            l1pss = json.load(fp)
        trigger = l1pss["cutValues"]["L1_RD0_FILLED"]
        if trigger["cut"] != 1:
            trigger["cut"] = 1
        else:
            l1pss["L1_RD0_FILLED"]["cut"] = 16776577
        with open(f"{download_dir}/alias/{l1file}", 'w') as fp:
            json.dump(l1pss, fp, indent=4)
    test: str = (
        f"trgdb_alias_upload.py --db {dest_dbw} --smk {uploaded_smk} -d {alias_dir} {logarg}"
    )
    success = execute_script(test, exec_dir)
    print_status(success, "  third alias upload with modified prescale", test)
    if not success:
        return False

    # compare upload keys
    upload_alias_record3 = get_last_upload_record('alias')
    akey1: int = upload_alias_record['key']
    akey3: int = upload_alias_record3['key']
    default3 = upload_alias_record3["dbrecord"]["DEFAULT"]
    success = (akey1 != akey3)
    print_status(success, "  third upload results in different alias key", "internal comparison")
    success = (default3 == "0")
    print_status(success, "  third upload is not default", "internal comparison")
    if not success:
        return False

    return success

def cmdl():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="count", default=0, help="verbose")
    parser.add_argument("-e", "--exec-dir", type=str, default="dbiotest",
                        help="execution dir (default 'dbiotest')")
    parser.add_argument("-d", "--dry-run", action="store_true", help="Only prints the commands")
    parser.add_argument("-c", "--print-commands", action="store_true", help="Prints the commands of each step")
    return parser.parse_args()

def main() -> int:
    global exec_dir
    global vlevel
    global logarg
    global dry_run
    global print_commands
    args = cmdl()
    exec_dir = args.exec_dir
    dry_run = args.dry_run
    vlevel = args.verbose
    print_commands = args.print_commands
    if vlevel>=2:
        logarg = "-l DEBUG"

    source_keys: dict[str, int] = { # run 477048, lb 54
        "run": 477048,
        "smk":3347,
        "bgsk":2861,
        "l1psk":13595,
        "hltpsk":10454
    }
    alias_keys: list[tuple[int, int, float, float]] = [
        (13566, 10458, 160.0, 170.0),
        (13565, 10457, 170.0, 180.0),
        (13564, 10456, 180.0, 200.0),
        (13563, 10455, 200.0, 220.0)]

    print(f"Executing tests in directory {exec_dir}/")

    success: bool = True

    tests: dict[str, tuple[Callable[...,bool],dict[str, Any]]] = {
        "Menu I/O" : (test_menu_io, {'source_keys': source_keys}),
        "Bunchgroup I/O" : (test_bunchgroup_io, {'source_keys': source_keys}),
        "Prescales I/O" : (test_prescales_io, {'source_keys': source_keys}),
        "Mongroup I/O" : (test_mongroups_io, {'source_keys': source_keys}),
        "L1CT files I/O" : (test_l1ctfiles_io, {'source_keys': source_keys}),
        "Alias I/O" : (test_alias_io, {'alias_keys': alias_keys, 'orig_keys': source_keys}),
    }

    for test_name, test in tests.items():
        print(f"Test {test_name}")
        print('-----' + '-'*len(test_name))
        test_fnc, test_pars = test
        test_success: bool = test_fnc(**test_pars)
        print_status(test_success, test_name, "")
        print()
        success &= test_success

    print_status(success, "All tests", "")
    return 0 if success else 1

if __name__ == "__main__":
    sys.exit(main())
