# Instructions for testing the Bunchgroup reader and uploader

[TOC]

## Testing with a provided filling scheme file

To test the `ReadBunchGroup.py` functionality one needs to things

* A mock partition that where the bunchgroups appear in IS (representing the initialL1CT partition of P1)
* A small script `WriteBG2IS.py` that simulates the actions of the CTP BunchGrouper application, generating a bunchgroup set in IS, when a file fillingscheme.(csv or json) is put in a certain location

### Step-by-step instructions

Setup a small partition, consisting of an ipc, an rdb and an IS server. This will provide the IS service to publish and read from the bunchgroup.

```sh
export TDAQ_PARTITION=part_<your username>_testbg
source TinyPartition.sh
```

Next start the script that observes the filling scheme file location and writes a simple buncgroup set to IS (simulating the CTPBunchgrouper application).

```sh
./WriteBGS2IS.py --target file --loop -f ctp/fillingscheme.csv &
```

Also get a filling scheme for the test

```sh
wget https://lpc.web.cern.ch/fillingSchemes/2023/candidates/25ns_2374b_2362_1704_1662_236bpi_13inj_hybrid_2INDIV.csv
# or
wget https://lpc.web.cern.ch/fillingSchemes/2023/candidates/25ns_1982b_1973_1245_1261_164bpi_15inj_hybrid_2INDIV_bs250ns.csv
```

Now one can test `ReadBunchGroup.py` with a filling scheme file. When run in a different shell (recommendet to distinguish the action better) don't forget to setup the tdaq release again

```sh
ReadBunchGroup.py --fromFile --ctp-dir ctp -p ${TDAQ_PARTITION} --file 25ns_2374b_2362_1704_1662_236bpi_13inj_hybrid_2INDIV.csv --write-json
```

The option `--write-json` creates a bunchgroup set json file and provides instructions how to upload it to the database

## Testing with of uploading to the DB

First load a bunchgroup set into IS of the test partition (see above)

```sh
extractBunchGroupSet.py --bgsk 2604 --dbalias TRIGGERDB_RUN3
./WriteBGS2IS.py -f BunchGroupSet_2604.json -p ${TDAQ_PARTITION} --target lhc
```

Then run

```sh
ReadBunchGroup.py --fromLHC -p ${TDAQ_PARTITION} -u --db TRIGGERDBDEV1_I8
```

You should see

```console
2023-May-11 16:26:06,812 INFO [getBunchGroupFromIS at ReadBunchGroup.py:173] Reading TrigConfL1BunchGroups from Monitoring.BunchGrouperApp/BunchGroups in partition part_stelzer_testbg
Bunchgroup set LHC_2361bunches_10trains_1indivs_hybrid
 0 - BCRVeto: 3543 bunches, 2 groups
 1 - Paired: 2361 bunches, 121 groups
 2 - CalReq: 91 bunches, 1 groups
 3 - Empty: 344 bunches, 14 groups
 4 - IsolatedUnpaired: 10 bunches, 2 groups
 5 - NonIsolatedUnpaired: 16 bunches, 3 groups
 6 - EmptyBeforeAfterPaired: 700 bunches, 132 groups
 7 - InTrain: 1640 bunches, 120 groups
 8 - FirstInTrain: 11 bunches, 11 groups
 9 - AfterGlow: 0 bunches, 0 groups
10 - ALFA: 4 bunches, 4 groups
11 - EmptyBeforePaired: 121 bunches, 121 groups
12 - AllWithoutCalreq: 3452 bunches, 3 groups
13 - UnpairedBeam1: 13 bunches, 2 groups
14 - UnpairedBeam2: 13 bunches, 2 groups
15 - PCC_AfterGlow: 733 bunches, 132 groups
Attempting to authenticate via /afs/cern.ch/user/a/attrgcnf/.dbauth/run3/write/
Opening DB connection ATLAS_CONF_TRIGGER_DEV1_W@INT8R
The bunch group set LHC_2361bunches_10trains_1indivs_hybrid is already in the database with index 22. Using existing entry.
Closing DB connection
```

## Partition cleanup

To cleanup the test partition run

```sh
ipc_rm -i ".*" -n ".*" -f -p ${TDAQ_PARTITION}
```
