

# Download menu, precales, bunchgroupset, and mongroups (from smk) 
# from the Run 3 Trigger DB and store it in a local directory
# source taken from run 477048, lb 54 on 4 June 2024

SOURCE_DB=TRIGGERDBDEV2_I8
DOWNLOAD_DIR="aliases"

SOURCE_SMK=3000
SOURCE_BGSK=2

DOWNLOAD_PARAM=" --outdir ${DOWNLOAD_DIR}"

(

    testdir=download_test_alias
    rm -rf ${testdir}
    mkdir -p ${testdir}
    cd ${testdir}

    extractAlias.py --db ${SOURCE_DB} --smk ${SOURCE_SMK} --download-prescales ${DOWNLOAD_PARAM}
    RETURN=$?
    echo -n "extractAlias.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo
 
)