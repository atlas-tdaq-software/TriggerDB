# run this script if you start from scratch
# if the partition, RDB, and IS server are already setup then only the python script needs to run

# setup TDAQ release
[[ -n $TDAQ_INST_PATH ]] || source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-10-00-00

export TDAQ_PARTITION=${TDAQ_PARTITION:-part_stelzer_testbg}

ipc_ls -Pl | grep ${TDAQ_PARTITION} > /dev/null
system=$?

if [[ $system == 0 ]]
then

    echo "partition ${TDAQ_PARTITION} is already running"
    echo "if you like to stop it, do"
    echo ipc_rm -i '".*"' -n '".*"' -f -p ${TDAQ_PARTITION}

else

    echo "starting partition ${TDAQ_PARTITION}"

    # start partition
    ipc_server -s -p ${TDAQ_PARTITION} &
    sleep 3

    # unset TDAQ_DB_REPOSITORY is needed at P1, otherwise it starts checking out the git repository and work with that one
    unset TDAQ_DB_REPOSITORY 

    # insert the xml data path from the software repository for is.xml
    # export TDAQ_DB_PATH=/sw/atlas/tdaq/tdaq-10-00-00/installed/share/data/:${TDAQ_DB_PATH}

    # start RDB server with the extra lumi object
    rdb_server -p ${TDAQ_PARTITION} -d ISRepository -S TTCInfo/ttc_is_info.xml &
    sleep 3

    # start IS server
    is_server -p ${TDAQ_PARTITION} -n Monitoring &
    is_server -p ${TDAQ_PARTITION} -n RunParams &
    sleep 3

fi

