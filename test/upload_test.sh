

# Upload menu, precales, bunchgroupset, and mongroups (from smk) 
# to the DEV2 Trigger DB 
# source taken from downloaded directory

DEST_DB=TRIGGERDBDEV2_I8
SOURCE_DIR="../download_test/run_477048_lb_54"
SDL=srcdir

(

    testdir=upload_test
    rm -rf ${testdir}
    mkdir -p ${testdir}
    cd ${testdir}
    ln -s ${SOURCE_DIR} ${SDL}

    insertMenu.py --db ${DEST_DB} -c "test upload of SMK 3347" \
    -l1 ${SDL}/L1Menu_3347.json -hlt ${SDL}/HLTMenu_3347.json -jo ${SDL}/HLTJO_3347.json -mon ${SDL}/MonGroups_smk3347.json            
    RETURN=$?
    echo -n "insertMenu.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    insertBunchGroupSet.py --db ${DEST_DB} -c "test upload of BGS 2861" --bgs ${SDL}/BunchGroupSet_2861.json             
    RETURN=$?
    echo -n "insertBunchGroupSet.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    insertPrescales.py --db ${DEST_DB} -c "Prescales upload testing" --smk 3004 \
    --l1ps ${SDL}/L1Prescale_13595.json --hltps ${SDL}/HLTPrescale_10454.json
    RETURN=$?
    echo -n "insertPrescales.py L1 and HLT ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo

    insertMonitoringGroups.py --db ${DEST_DB} --smk 3004  --mongroup ${SDL}/MonGroups_smk3347.json
    RETURN=$?
    echo -n "insertMonitoringGroups.py ==> "
    if [ $RETURN -eq 0 ]; then
        echo "SUCCESS"
    else
        echo "FAILURE"
    fi
    echo
 
)