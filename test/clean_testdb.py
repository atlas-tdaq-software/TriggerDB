#!/usr/bin/env python

from argparse import ArgumentParser
import oracledb
oracledb.init_oracle_client() 

def cmdl():
    parser = ArgumentParser()
    parser.add_argument("--user", type=str, default="atlas_trig_stelzer",
                        help="test db (default='atlas_trig_stelzer')")
    parser.add_argument("--server", type=str, default="devdb19u",
                        help="test db oracle server where test db is hosted (default='devdb19u')")
    parser.add_argument("--pw", type=str, required=True,
                        help="test db password (required)")
    parser.add_argument("--schema", type=str,
                        help="test db schema (by default same as user)")
    return parser.parse_args()


if __name__=="__main__":
    args = cmdl()
    user = args.user
    server = args.server
    schema = args.schema if args.schema else user
    pw = args.pw
    tables: list[str] = [
        'PRESCALE_ALIAS_ROW', 'PRESCALE_ALIAS',
        'L1_PRESCALE_SET', 'HLT_PRESCALE_SET', 'L1_BUNCH_GROUP_SET',
        'SUPER_MASTER_TABLE',
        'L1_MENU', 'L1_CTP_FILES', 'L1_MUCTPI_FILES', 'L1_CTP_SMX', 'L1_TMC_SIGNALS',
        'HLT_MONITORING_GROUPS', 'HLT_MENU', 'HLT_JOBOPTIONS'
        ]
    with oracledb.Connection(f"{user}/{pw}@{server}") as conn:
        conn.current_schema = f"{schema}"
        cursor: oracledb.Cursor = conn.cursor()
        for table in tables:
            query:str = f"TRUNCATE TABLE {table} REUSE STORAGE"
            try:
                print(f"Going to delete {table} => ", end="", flush=True)
                cursor.execute(query)
                cursor.connection.commit()
                print("done")
            except oracledb.DatabaseError as e:
                error, = e.args
                print(f"Error: {error.message}")