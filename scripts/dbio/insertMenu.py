#!/usr/bin/env python
"""
Description
-----------
Upload the trigger menu (L1, HLT, JobOptions) from files to the ATLAS TriggerDB

Usage instructions:

Uploads any new menus to the correct tables, and links them together to create a
super-master key. If an existing key is provided, or an identical menu file is
found to already be contained in the database, it will be used.
"""
import argparse
import sys
import traceback
from typing import Union

from TriggerDB.Logger import log
from TriggerDB.MenuUploader import MenuUploader


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("-l1", "--l1menu", required=True, type=str,
                        help="L1 menu json file name or an integer specifying an existing L1 menu ID to be linked (required).")
    parser.add_argument("-hlt", "--hltmenu", required=True, type=str,
                        help="HLT menu json file, an integer specifying an existing HLT menu ID to be linked, or the string 'off' for an upload without an HLT menu (required).")
    parser.add_argument("-jo", "--hltjo", required=True, type=str,
                        help="Job options json file, an integer specifying an existing HLT JO ID to be linked, or the string 'off' for an upload without HLT joboptions (required).")
    parser.add_argument("-mon", "--mongroup", required=True, type=str,
                        help="Monitoring groups json file, an integer specifying an existing Monitoring Groups ID to be linked, or the string 'off' for an upload without Monitoring Groups (required).")
    parser.add_argument("-c", "--comment", type=str, nargs="+",
                        help="Comment to be uploaded with the record (optional).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    # Check that --l1menu is provided
    l1menu: Union[str, int]
    if args.l1menu is None or args.l1menu == "off":
        raise RuntimeError("No L1 menu. An L1 menu file/index must be provided.")
    try:
        l1menu = int(args.l1menu)
    except ValueError:
        l1menu = args.l1menu


    # Check that --hltmenu is provided or explicitly set to off
    hltmenu: Union[str, int, None]
    if args.hltmenu == "off":
        hltmenu = None
    elif args.hltmenu is None:
        raise RuntimeError("No HLT menu. An HLT menu file/index must be provided, unless '--hltmenu off' is explicitly specified.")
    else:
        # Determine whether --hltmenu is provided as index or filename
        try:
            hltmenu = int(args.hltmenu)
        except ValueError:
            hltmenu = args.hltmenu

    # Check that --hltjo is provided or explicitly set to off
    hltjo: Union[str, int, None]
    if args.hltjo == "off":
        hltjo = None
    elif args.hltjo is None:
        raise RuntimeError("No HLT joboptions. An HLT joboptions file/index must be provided, unless '--hltjo off' is explictly specified.")
    else:
        # Determine whether --hltjo is provided as index or filename
        try:
            hltjo = int(args.hltjo)
        except ValueError:
            hltjo = args.hltjo

    # Check that --mongroup is provided or explicitly set to off
    mongroup: Union[str, None]
    if args.mongroup == "off":
        mongroup = None
    else:
        mongroup = args.mongroup

    # Upload menu files
    with MenuUploader(args.dbalias) as uploader:
        uploader.upload(l1menu=l1menu, hltmenu=hltmenu, hltjo=hltjo, mongroup=mongroup, comment=args.comment)
        uploader.print_record()
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
