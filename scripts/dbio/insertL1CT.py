#!/usr/bin/env python
"""
Upload L1CT hardware files to a given SMK to the TriggerDB

Files must be provided in a well defined directory structure.

By default uploads are only done if there are no hardware files present 
in the database. However, an overwrite can be forced.
"""
import argparse
import sys
import traceback
from TriggerDB.L1CTFilesUploader import L1CTFilesUploader
from TriggerDB.Logger import log


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--topdir", type=str, required=True,
                        help="This should be the top level directory, where to find the L1CT and MUCTPI files for uploading (required).",)
    parser.add_argument("--smk", type=int, required=True,
                        help="SMK to which the L1CT and MUCTPI files will be linked (required).")
    parser.add_argument("--force", type=str, metavar="table", nargs='*',
                        choices=['ctp', 'smx', 'muctpi', 'tmc', 'all'], default=[],
                        help="force update of particular table (choices are 'ctp', 'smx', 'muctpi', or 'tmc').")
    parser.add_argument("--skipTargetCheck", action="store_true", default=False,
                        help="skips the check if the target SMK matches the menu the files were generated with (experts only).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    with L1CTFilesUploader(**dict(args._get_kwargs())) as uploader:
        success = uploader.upload(topdir=args.topdir, smk=args.smk)
        log.info(f"Upload success: {success}")
        uploader.writeUploadRecordFile("l1ctfiles")
    return 0 if success else 1


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
