#!/usr/bin/env python
"""
Description
-----------
  Set which Monitoring Group id associated to a
  given super-master key should be in use with others off in the ATLAS TriggerDB.
  
  Contains the definition of a monitoring group setter class, and all the 
  necessary functions to set the monitoring groups IN_USE column.
  
  WHEN RUN AS MAIN:
  
  Takes three required arguments `--dbalias`, specifying the alias for which
  instance of TriggerDB to connect to,  `--mongroup` for the monitoring group to 
  be switched on and `--smk` for the super-master key this should be for
  
  Prints out the required infomation
"""
import argparse
import sys
import traceback

from TriggerDB.Logger import log
from TriggerDB.MonGroupUploader import MonGroupUploader

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--mgk", type=int, required=True,
                        help="The monitoring group key which is to be set as in-use")
    parser.add_argument("--smk", type=int, default=0,
                        help="The SMK for which the mon groups should be set. If a mongroup key is specified this is just to cross-check, because the MGK already determines the HLT menu/SMK.")
    parser.add_argument("--deactive-all", action="store_true",
                        help="Deactive all mongroups for the given SMK (which must specified and different from 0).")    
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2'.")
    return parser.parse_args()


def main()-> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    if args.deactivate_all and args.smk<=0:
        log.error("To deactivate all monitoring group keys of a certain SMK, it must be specified")

    with MonGroupUploader(args.dbalias) as mguploader:
        if args.deactivate_all:
            success: bool = mguploader.deactivateMonGroups(smk=args.smk)
        else:
            success: bool = mguploader.activateMonGroup(mgk=args.mgk, smk=args.smk)

    return 0 if success else 1


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
