#!/usr/bin/env python
"""
Description
-----------
  This module contains code to list the Monitoring Group ids associated to a
  given super-master key in the ATLAS TriggerDB.

Contains the definition of a monitoring group reader class, and all the 
necessary functions to get the monitoring groups.

WHEN RUN AS MAIN:

Takes two required arguments `--dbalias`, specifying the alias for which
instance of TriggerDB to connect to and `--smk` for the super-master key to 
look up

Prints out the required infomation
"""
import argparse
import sys
import traceback
from typing import Any

from TriggerDB.Logger import log
from TriggerDB.MonGroupDownloader import MonGroupDownloader


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the mon groups should be listed")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    with MonGroupDownloader(args.dbalias) as reader:
        mongroups: list[dict[str, Any]] = reader.getMonGroups(args.smk)
    print(f"\nThe SMK {args.smk} is associated to {len(mongroups)} monitoring groups")
    if len(mongroups)>0:
        print("  ID | in use? | comment")
        print("------------------------")
        for mongrp in mongroups:
            print("{ID:4} |{IN_USE:8} | {COMMENT}".format(**mongrp))
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
