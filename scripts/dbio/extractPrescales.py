#!/usr/bin/env python
"""
Description
-----------
  Download one or more L1 and/or HLT prescale sets from the TriggerDB and save them as json files.

  The files are saved by default to a local directory `./prescales` as
  `./L1Prescale_<L1PSK>.json`, `./HLTPrescale_<HLTPSK>.json`.
  
"""

import argparse
import sys
import traceback
from TriggerDB.PrescaleSetDownloader import HLTPrescaleSetDownloader, L1PrescaleSetDownloader
from TriggerDB.Logger import log


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--l1psk", type=int, nargs='*', action='extend',
                        help="The key(s) of the L1 prescale set(s) to download (optional).")
    parser.add_argument("--hltpsk", type=int, nargs='*', action='extend',
                        help="The key(s) of the HLT prescale set(s) to download (optional).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    parser.add_argument("-o", "--outdir", type=str, default="./prescales",
                        help="Output directory for downloaded files. By default './prescales'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    
    outdir: str = args.outdir.format(**dict(args._get_kwargs()))
    if outdir != "" and not outdir.endswith('/'):
        outdir += "/"

    if args.l1psk is not None:
        with L1PrescaleSetDownloader(args.dbalias) as downloader:
            for l1psk in args.l1psk:
                downloader.download(l1psk)
                downloader.writeFile(f"{outdir}L1Prescale_{l1psk}.json")

    if args.hltpsk is not None:
        with HLTPrescaleSetDownloader(args.dbalias) as downloader:
            for hltpsk in args.hltpsk:
                downloader.download(hltpsk)
                downloader.writeFile(f"{outdir}HLTPrescale_{hltpsk}.json")
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)