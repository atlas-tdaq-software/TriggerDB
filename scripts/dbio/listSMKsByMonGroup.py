#!/usr/bin/env python
"""
Description
-----------
  Print information about given monitoring group
"""
import traceback
from typing import Any
import argparse
import sys

from TriggerDB.Logger import log
from TriggerDB.MonGroupDownloader import MonGroupDownloader

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--mgk", type=int, required=True,
                        help="The monitoring group key for which the SMKs should be listed")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. `TRIGGERDB_RUN3`.")
    return parser.parse_args()


def main() -> int:
    args = cmdl()

    with MonGroupDownloader(args.dbalias) as reader:
        result: dict[str, Any] = reader.getMongroupInfo(args.mgk)
    print( f"Information about the Monitoring Group {args.mgk}:")
    print( f"   SMKs: {result['SMKs']}")
    print( f"   HLT menu: {result['HMG_HLT_MENU_ID']}")
    print( f"   name: {result['HMG_NAME']}")
    print( f"   comment: {result['HMG_COMMENT']}")
    print( f"   in use: {result['HMG_IN_USE']}")
    print( f"   hidden: {result['HMG_HIDDEN']}")
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
