#!/usr/bin/env python
"""
Print the hash of the provided JSON file, identical 
with the hash string in the database.
"""

from argparse import ArgumentParser
import sys
import traceback
from TriggerDB.Logger import log
from TriggerDB.hashHelper import getFileHash

def cmdl():
    parser = ArgumentParser(epilog=__doc__)
    parser.add_argument("--jsonfile", dest="jsonfile", required=True,
                        help="The filepath of the JSON file to be hashed (required)")
    return parser.parse_args()


def main():
    args = cmdl()
    print(getFileHash(args.jsonfile))
    return 0
    
if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
