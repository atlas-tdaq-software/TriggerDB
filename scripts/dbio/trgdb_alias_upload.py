#!/usr/bin/env python
"""
Batch upload of set of L1/HLT keys differing by lumi point. 
The luminosity is extracted from the individual JSON names.
"""

import argparse
import sys
import traceback

from TriggerDB.Logger import log
from TriggerDB.AliasUploader import AliasUploader


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument('-d', '--target_dir', type=str, required=True,
                        help='Directory containing alias file and L1 and HLT PS files created by the Rulebook')
    parser.add_argument('--smk', type=str, required=True,
                        help='The SMK to which the prescale keys should be linked (required)')
    parser.add_argument('--tp-name', type=str,
                        help="Name to appear in the IGUI TriggerPanel. By default the 'description' from the alias file is used.")
    parser.add_argument("-c", "--comment", type=str, nargs="+", 
                        help="Comment to be uploaded with the record (optional, by default the comment from the alias file is taken).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2'.")          
    parser.add_argument('--exec', action='store_true',
                        help='Execute the upload, default behaviour will print the commands')
    parser.add_argument('--do-table', default=False, action="store_true", 
                        help='Prepare jira and wb tables')
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args

def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    with AliasUploader(args.dbalias) as uploader:
        uploader.upload(alias_ps_file_dir=args.target_dir, smk=args.smk, tp_name=args.tp_name, comment=args.comment)
        if args.do_table:
            # uploader.write_table()
            pass
        uploader.print_record()
        uploader.writeUploadRecordFile("alias")
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
