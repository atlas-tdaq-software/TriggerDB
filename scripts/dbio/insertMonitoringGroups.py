#!/usr/bin/env python
"""
Description
-----------
  This module contains code to insert HLT monitoring group files into the ATLAS TriggerDB.

  Takes a command-line argument specifying the filepath to the monitoring groups
  file, `--mongroup`.


  Uploads the monitoring groups to the correct table, linking to the specified
  super-master key. If an existing monitoring group is found to be identical for any given
  menu, will attempt to use the existing monitoring group rather than uploading duplicates.
"""

import argparse
import sys
import traceback
from TriggerDB.MonGroupUploader import MonGroupUploader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--mongroup", type=str, required=True,
                        help="Monitoring Groups json file.")
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK to link prescale sets to.")
    parser.add_argument("-c", "--comment", type=str, nargs="+",
                        help="Comment to be uploaded with the record (optional).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    with MonGroupUploader(dbalias=args.dbalias) as uploader:
        uploader.upload(args.mongroup, args.smk, args.comment)
        uploader.print_record()
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
