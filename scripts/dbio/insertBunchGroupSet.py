#!/usr/bin/env python
"""
Description
-----------
Upload the bunchgroup set from a file to the ATLAS TriggerDB

If an identical bunchgroup set already exists in the database, the script 
will not upload the file content but provide a reference to the existing
entry. This avoids duplicates.
"""

import argparse
import sys
import traceback
from TriggerDB.BunchGroupSetUploader import BunchGroupSetUploader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--bgs", type=str, required=True,
                        help="Bunchgroup set json file.")
    parser.add_argument("-c", "--comment", type=str, nargs="+",
                        help="Comment to be uploaded with the record (optional).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args

def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    with BunchGroupSetUploader(args.dbalias) as uploader:
        uploader.upload(args.bgs, comment=args.comment)
        uploader.print_record()
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
