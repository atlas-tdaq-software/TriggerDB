#!/usr/bin/env python
"""
Description
-----------
  Executable to  insert prescale set files into the ATLAS TriggerDB,
  for both the L1 and HLT triggers.

WHEN RUN AS MAIN:

Uploads the prescale sets to the correct tables, linking them to the specified
super-master key. If an existing prescale set is found to be identical for any given
menu, will attempt to use the existing prescale set rather than uploading duplicates.
"""

import argparse
import sys
import traceback
from TriggerDB.PrescaleSetUploader import PrescaleSetUploader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--l1ps", type=str,
                        help="L1 prescale set json file (at least one of --l1ps or --hltps must be provided).")
    parser.add_argument("--hltps", type=str,
                        help="HLT prescale set json file(at least one of --l1ps or --hltps must be provided).")
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK to link prescale sets to.")
    parser.add_argument("-c", "--comment", type=str, nargs="+",
                        help="Comment to be uploaded with the record (optional).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    # Check that one of --l1ps and --hltps is provided
    if args.l1ps is None and args.hltps is None:
        log.error("No prescale set files specified")
        return 1

    with PrescaleSetUploader(args.dbalias) as uploader:
        uploader.upload(l1ps=args.l1ps, hltps=args.hltps, smk=args.smk, comment=args.comment)
        uploader.createUploadRecord()
        uploader.print_record()

    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
