#!/usr/bin/env python
"""
Description
-----------
  Print the menu keys associated to a given super-master key.
"""
import argparse
import sys
import traceback
from TriggerDB.MenuDownloader import MenuDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the menu IDs should be listed")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    with MenuDownloader(args.dbalias) as reader:
        l1menu_id, hltmenu_id, hltjo_id = reader.getLinkedIDsFromSMK(args.smk)
        log.info(f"For SMK {args.smk} attached are: L1Menu {l1menu_id}, HLT menu {hltmenu_id}, HLT joboptions {hltjo_id}")
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
