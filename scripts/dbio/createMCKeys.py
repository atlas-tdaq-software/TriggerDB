#!/usr/bin/env python
import subprocess
import argparse
import sys
import traceback

from TriggerDB.Logger import log

def cmdl():
    # Define command-line arguments
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__)
    parser.add_argument("--directory", type=str, required=True,
                        help=f"directory of files (required)")
    parser.add_argument("-c", "--comment", required=True,
                        help="comment to show in DB (required)")
    parser.add_argument("-d", "--dry-run", action="store_true",
                        help="Only print the upload command")
    return parser.parse_args()

def main():

    args = cmdl()

    #command runs setup script and the upload  
    command = "source /afs/cern.ch/user/a/attrgcnf/TriggerTool/Run3/current/installed/setup.sh; "
    command += "insertAll.py --dbalias TRIGGERDBMC_RUN3 --hltjo off --mongroup off "
    command += f" --directory {args.directory} --comment {args.comment}" 

    print(f"Command:\n{command}")
    if args.dry_run:
        print("Dry-run specified, will not execute")
    else:
        ret = subprocess.run(command, stdout=subprocess.PIPE, shell=True)
        print(ret.stdout.decode())
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
