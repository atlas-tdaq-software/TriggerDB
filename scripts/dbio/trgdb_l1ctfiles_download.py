#!/usr/bin/env python
"""
Description
-----------
  Download a complete set for a given SMK from the TriggerDB and save to files.

  The files are saved by default to a local directory `./l1ctfiles/`
"""

import argparse
import sys
import traceback
from TriggerDB.L1CTFilesDownloader import L1CTDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__)
    parser.add_argument("--smk", type=int, required=True,
                        help="the SMK for which the menu IDs should be listed. If smk is 0, all entries in the database are listed.")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2'.")
    parser.add_argument("-o", "--outdir", type=str,
                        help="Output directory for downloaded files.")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()

def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    with L1CTDownloader(args.dbalias) as downloader:
        downloader.download(args.smk, outputdir=args.outdir)
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)