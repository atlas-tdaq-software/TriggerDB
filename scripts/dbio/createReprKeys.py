#!/usr/bin/env python
"""Uploads trigger keys which were produced by a nightly build test
    
    Test that created the JSON files, examples of existing options with dump-config, dump-config-exit or dump-config-reload:
    For 2024 EB data
       test_trigP1_v1Dev_dumpConfig_build               : Dev_pp_run3_v1_HLTReprocessing_prescale; doL1Sim=True; rewriteLVL1=True;
       test_trigP1_v1PhysP1_build                       : PhysicsP1_pp_run3_v1_HLTReprocessing_prescale; doL1Sim=True; rewriteLVL1=True;
       test_trigP1_v1Dev_noL1Sim_dumpConfig_build       : Dev_pp_run3_v1_HLTReprocessing_prescale
       test_trigP1_v1PhysP1_noL1Sim_dumpConfig_build.py : PhysicsP1_pp_run3_v1_HLTReprocessing_prescale
     For 2018 EB data
       test_trigP1_v1Dev_dumpConfig_run2_build          - setMenu='Dev_pp_run3_v1_HLTReprocessing_prescale';doL1Sim=True;rewriteLVL1=True;
       test_trigP1_v1PhysP1_dumpConfig_run2_build       - setMenu='PhysicsP1_pp_run3_v1_HLTReprocessing_prescale';doL1Sim=True;rewriteLVL1=True;
"""

from pathlib import Path
import subprocess
import argparse
import sys
import traceback

from TriggerDB.Logger import log

import argparse
import sys

def cmdl(defaults: dict[str,str]):
    # Define command-line arguments
    parser = argparse.ArgumentParser(
                formatter_class=argparse.RawDescriptionHelpFormatter,
                epilog=__doc__)
    parser.add_argument("-r", "--release", type=str, required=True, dest="nightly",
                        help="release to be used in reprocessing, e.g. 2025-01-31T2101 (required)")
    parser.add_argument("--jira", type=str,
                        help="Jira reference for comment (optional)")
    parser.add_argument("-b", "--releaseBranch", type=str, default=defaults['branch'], dest="branch",
                        help=f"release branch (default '{defaults['branch']}')")
    parser.add_argument("-t", "--buildTag", type=str, default="x86_64-el9-gcc13-opt", dest="tag",
                        help="release build tag (default x86_64-el9-gcc13-opt).")
    parser.add_argument("-f", "--testFiles", type=str, default=defaults['test'], dest="test",
                        help=f"nightly test which produces the JSON files (default '{defaults['test']}')")
    parser.add_argument("-d", "--dry-run", action="store_true",
                        help="Only print the upload command")
    return parser.parse_args()

def main() -> int:
    
    defaults: dict[str,str] = {
        "branch": "24.0",
        "test": "test_trigP1_v1PhysP1_build"
    }
    
    args = cmdl(defaults)

    #EOS location of files
    directory = Path(f"/eos/user/a/atrvshft/www/ART_build/{args.branch}/Athena/{args.tag}/{args.nightly}/TrigP1Test/{args.test}")
    if not directory.exists():
        log.error(f"Directory {directory} does not exist")
        return 1
    
    # comment for DB upload (only saved if new SMK record is created)"
    jirastr: str = "" if args.jira is None else f" {args.jira}"
    comment = f'"Reprocessing for {args.nightly},{args.branch} from {args.test}{jirastr}"'

    #command runs setup script and the upload  
    command = "source /afs/cern.ch/user/a/attrgcnf/TriggerTool/Run3/current/installed/setup.sh; "
    command += f"insertAll.py --dbalias TRIGGERDBREPR_RUN3 --comment {comment} --directory {directory}"

    print(f"Command:\n{command}")
    if args.dry_run:
        print("Dry-run specified, will not execute")
    else:
        ret = subprocess.run(command, stdout=subprocess.PIPE, shell=True)
        print(ret.stdout.decode())
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
