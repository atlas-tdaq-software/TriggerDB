#!/usr/bin/env python
"""
Description
-----------
This module contains code to list the prescale keys in the ATLAS TriggerDB
associated to a given super-master key.

Contains the definition of a menu reader class, and all the necessary
functions to get the prescale keys.

WHEN RUN AS MAIN:

Takes two arguments, `--smk`, specifying the SMK for which to list the prescale keys,
and `--dbalias`, specifying the alias for which instance of TriggerDB to connect to
(both required).

Prints out the required information.
"""
import argparse
import sys
import traceback

from TriggerDB.MenuDownloader import MenuDownloader
from TriggerDB.Logger import log


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the menu IDs should be listed (required).")
    parser.add_argument("--l1-only", action="store_true",
                        help="To print only the L1 prescales")
    parser.add_argument("--hlt-only", action="store_true",
                        help="To print only the HLT prescales")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    with MenuDownloader(args.dbalias) as reader:
        # Get keys and comment
        l1menu_id, hltmenu_id, hltjo_id = reader.getLinkedIDsFromSMK(args.smk)

        # Get prescale keys
        l1_keys: list[tuple[int, str, str]] = []
        if l1menu_id is not None and not args.hlt_only:
            l1_keys = reader.l1menu_downloader.getPrescaleKeys(l1menu_id)

        hlt_keys: list[tuple[int, str, str]] = []
        if hltmenu_id is not None and not args.l1_only:
            hlt_keys: list[tuple[int, str, str]] = reader.hltmenu_downloader.getPrescaleKeys(hltmenu_id)

    if args.l1_only:
        log.info(f"The SMK {args.smk} has {len(l1_keys)} L1 prescale keys associated.")
    elif args.hlt_only:
        log.info(f"The SMK {args.smk} has {len(hlt_keys)} HLT prescale keys associated.")
    else:
        log.info(f"The SMK {args.smk} has {len(l1_keys)} L1 prescale keys and {len(hlt_keys)} HLT prescale keys associated.")

    if len(l1_keys)>0:
        print()
        print(f"{'L1 psk':10} {'name':50}   comment")
        print(150*"-")
        for key, name, comment in l1_keys:
            print(f"{key:<10} {name:<50}   {comment}")
    if len(hlt_keys)>0:
        print()
        print(f"{'HLT psk':10} {'name':50}   comment")
        print(150*"-")
        for key, name, comment in hlt_keys:
            print(f"{key:<10} {name:<50}   {comment}")

    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
