#!/usr/bin/env python
"""
Description
-----------
  This module provides inspection of all L1 CTP and MUCTPI related information
"""
from __future__ import print_function
import argparse
import sys
import traceback
from TriggerDB.L1CTFilesDownloader import L1CTDownloader
from TriggerDB.Logger import log


def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the menu IDs should be listed. If smk is 0, all entries in the database are listed.")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2'.")
    parser.add_argument("--download", action="store_true", default=False,
                        help="Download the L1Menu and L1CT and MUCTPI hardware files. Requires SMK.")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    with L1CTDownloader(args.dbalias) as reader:
        reader.printL1KeyInfo(smk=args.smk)
        if args.download and args.smk>0:
            reader.download(key = args.smk)
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
