#!/usr/bin/env python
"""
Description
-----------
  Download a bunchgroup set for a given bunchgroup set key from the TriggerDB and save to file.

  The file is saved by default to a local directory `./bunchgroupsets/` as
  `BunchGroupSet_<bgsk>.json`
"""

import argparse
import sys
import traceback
from TriggerDB.BunchGroupSetDownloader import BunchGroupSetDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--bgsk", type=int, required=True,
                        help="The bunch group set key for which the bunch group set is to be extracted (required).")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    parser.add_argument("-o", "--outdir", type=str, default="./bunchgroupsets",
                        help="Output directory for downloaded files. By default './bunchgroupsets'")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    # create the output directory
    outdir: str = args.outdir.format(smk=args.bgsk, dbalias=args.dbalias)
    if outdir != "" and not outdir.endswith('/'):
        outdir += "/"

    with BunchGroupSetDownloader(args.dbalias) as downloader:
        downloader.download(args.bgsk)
        downloader.writeFile(f"{outdir}BunchGroupSet_{args.bgsk}.json")
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)