import argparse
import cx_Oracle
import json
import hashlib
import sys

"""
This scripts is used to recalculate hashes with Python3 script to be sure
we do have correct ones (there's difference between calculating these
with Python2 and Python3)
"""


def calculate_hash_and_reformat_data(data):
    """
    Reformats JSON string and calculates hash
    :param data: JSON string
    :return: (hash, formatted_data)
    """
    formatted_data = json.dumps(
        data, sort_keys=True, indent=4, separators=(",", ": ")
    ).encode("utf-8")
    hash_object = hashlib.md5(formatted_data)
    return hash_object.hexdigest()


def main():
    """
    Main function of script, which is used to migrate Python2 hashes into Python3
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--dbUser",
        dest="dbUser",
        help="The user of Database",
        default=None,
    )

    parser.add_argument(
        "--dbPassword",
        dest="dbPassword",
        help="The password of Database",
        default=None,
    )

    parser.add_argument(
        "--dbNA",
        dest="dbNA",
        help="The NA of Database",
        default=None,
    )

    parser.add_argument(
        "--dbSchema",
        dest="dbSchema",
        help="The Schema of Database",
        default=None,
    )

    parser.add_argument(
        "--withBunchgroups",
        dest="withBunchgroups",
        help="Whether migrate bunch groups of not",
        default=None,
    )

    cmdLineArgs = parser.parse_args()

    if cmdLineArgs.dbUser is None or cmdLineArgs.dbPassword is None or cmdLineArgs.dbNA is None \
            or cmdLineArgs.dbSchema is None or cmdLineArgs.withBunchgroups is None:
        raise RuntimeError("Please provide credentials to the database")

    if cmdLineArgs.withBunchgroups == "y":
        withBunchgroups = True
    else:
        withBunchgroups = False

    oracle_connection_string = "{username}/{password}@{na}"
    connection_string_formatted = oracle_connection_string.format(
        username=cmdLineArgs.dbUser,
        password=cmdLineArgs.dbPassword,
        na=cmdLineArgs.dbNA)

    oracle_connection = cx_Oracle.Connection(connection_string_formatted)
    oracle_cursor = cx_Oracle.Cursor(oracle_connection)

    print("Connection to the database established.")

    # HLT Prescales
    print("Starting migration of HLT Prescales...")

    oracle_cursor.execute("SELECT * FROM %s.HLT_PRESCALE_SET" % (
        cmdLineArgs.dbSchema,
        )
    )
    hlt_prescales = oracle_cursor.fetchall()

    print(f"Found {len(hlt_prescales)} HLT Prescale items")

    for hlt_prescale in hlt_prescales:
        new_hash = calculate_hash_and_reformat_data(json.loads(hlt_prescale[2].read().decode("utf-8")))
        print(f"HLT_Prescale: {hlt_prescale[0]}, HASH: {hlt_prescale[3]}, NEW_HASH: {new_hash}")
        query = "UPDATE %s.HLT_PRESCALE_SET SET HPS_HASH = '%s' WHERE HPS_ID = %s" % (
            cmdLineArgs.dbSchema,
            new_hash,
            hlt_prescale[0]
        )

        oracle_cursor.execute(query)
    # END HLT Prescales

    # HLT Menu
    print("Starting migration of HLT Menu...")

    oracle_cursor.execute("SELECT * FROM %s.HLT_MENU" % (
            cmdLineArgs.dbSchema,
        )
    )
    hlt_menus = oracle_cursor.fetchall()

    print(f"Found {len(hlt_menus)} HLT Menu items")

    for hlt_menu in hlt_menus:
        new_hash = calculate_hash_and_reformat_data(json.loads(hlt_menu[2].read()))
        print(f"HLT_Menu: {hlt_menu[0]}, HASH: {hlt_menu[3]}, NEW_HASH: {new_hash}")
        query = "UPDATE %s.HLT_MENU SET htm_hash = '%s' WHERE htm_id = %s" % (
            cmdLineArgs.dbSchema,
            new_hash,
            hlt_menu[0]
        )
        oracle_cursor.execute(query)
    # END HLT Menu

    # HLT JobOptions
    print("Starting migration of HLT Job Options...")

    oracle_cursor.execute("SELECT * FROM %s.HLT_JOBOPTIONS" % (
            cmdLineArgs.dbSchema,
        )
    )
    hlt_job_options = oracle_cursor.fetchall()

    print(f"Found {len(hlt_job_options)} HLT Job Options items")

    for hlt_job_option in hlt_job_options:
        new_hash = calculate_hash_and_reformat_data(json.loads(hlt_job_option[1].read()))
        print(f"HLT_Job_Option: {hlt_job_option[0]}, HASH: {hlt_job_option[2]}, NEW_HASH: {new_hash}")
        query = "UPDATE %s.HLT_JOBOPTIONS SET hjo_hash = '%s' WHERE hjo_id = %s" % (
            cmdLineArgs.dbSchema,
            new_hash,
            hlt_job_option[0]
        )
        oracle_cursor.execute(query)
    # END HLT JobOptions

    # L1 Prescales
    print("Starting migration of L1 Prescales...")

    oracle_cursor.execute("SELECT * FROM %s.L1_PRESCALE_SET" % (
            cmdLineArgs.dbSchema,
        )
    )
    l1_prescales = oracle_cursor.fetchall()

    print(f"Found {len(l1_prescales)} L1 Prescale items")

    for l1_prescale in l1_prescales:
        new_hash = calculate_hash_and_reformat_data(json.loads(l1_prescale[2].read()))
        print(f"L1_Prescale: {l1_prescale[0]}, HASH: {l1_prescale[3]}, NEW_HASH: {new_hash}")
        query = "UPDATE %s.L1_PRESCALE_SET SET l1ps_hash = '%s' WHERE l1ps_id = %s" % (
            cmdLineArgs.dbSchema,
            new_hash,
            l1_prescale[0]
        )
        oracle_cursor.execute(query)
    # END L1 Prescales

    # L1 BunchGroups
    if withBunchgroups:
        print("Starting migration of L1 Bunch groups...")

        oracle_cursor.execute("SELECT * FROM %s.L1_BUNCH_GROUP_SET" % (
                cmdLineArgs.dbSchema,
            )
        )
        l1_bunchgroups = oracle_cursor.fetchall()

        print(f"Found {len(l1_bunchgroups)} L1 BunchGroups items")

        for l1_bunchgroup in l1_bunchgroups:
            new_hash = calculate_hash_and_reformat_data(json.loads(l1_bunchgroup[2].read()))
            print(f"L1_BunchGroup: {l1_bunchgroup[0]}, HASH: {l1_bunchgroup[3]}, NEW_HASH: {new_hash}")
            query = "UPDATE %s.L1_BUNCH_GROUP_SET SET l1bgs_hash = '%s' WHERE l1bgs_id = %s" % (
                cmdLineArgs.dbSchema,
                new_hash,
                l1_bunchgroup[0]
            )
            oracle_cursor.execute(query)

    # END L1 BunchGroups

    # L1 Menu
    print("Starting migration of L1 Menu...")

    oracle_cursor.execute("SELECT * FROM %s.L1_MENU" % (
            cmdLineArgs.dbSchema,
        )
    )
    l1_menus = oracle_cursor.fetchall()

    print(f"Found {len(l1_menus)} L1 Menu items")

    for l1_menu in l1_menus:
        new_hash = calculate_hash_and_reformat_data(json.loads(l1_menu[2].read()))
        print(f"L1_Menu: {l1_menu[0]}, HASH: {l1_menu[3]}, NEW_HASH: {new_hash}")
        query = "UPDATE %s.L1_MENU SET l1tm_hash = '%s' WHERE l1tm_id = %s" % (
            cmdLineArgs.dbSchema,
            new_hash,
            l1_menu[0]
        )
        oracle_cursor.execute(query)

    # END L1 Menu

    oracle_cursor.connection.commit()

    print("Migration succeeded!")


if __name__ == "__main__":
    if sys.version_info[0] < 3:
        raise Exception("Must be using Python 3")
    main()
