#!/usr/bin/env python
"""
Description
-----------
  Download a L1 and HLT menu, job-options, and monitoring groups for a given SMK from the TriggerDB and save to files.

  The files are saved by default to a local directory menu_smk<smk> as
  `L1Menu_<SMK>.json`, `HLTMenu_<SMK>.json`, `HLTJO_<SMK>.json`, and `MonGroups_<SMK>.json`.
"""

import argparse
import sys
import traceback

from TriggerDB.MonGroupDownloader import MonGroupDownloader
from TriggerDB.MenuDownloader import MenuDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the menu files should be downloaded")
    parser.add_argument("--l1only", action="store_true", default=False,
                        help="Only download L1Menu")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    parser.add_argument("-o", "--outdir", type=str, default="./menu_smk{smk}",
                        help="Output directory for downloaded files. By default './menu_smk{smk}'")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    # Set output directory for saving the files to and create of non-existent
    outdir: str = args.outdir.format(smk=args.smk, dbalias=args.dbalias)
    if outdir != "" and not outdir.endswith('/'):
        outdir += "/"

    with MenuDownloader(args.dbalias) as downloader:
        downloader.download(args.smk)
        downloader.l1menu_downloader.writeFile(f"{outdir}L1Menu_{args.smk}.json")
        if not args.l1only:
            downloader.hltmenu_downloader.writeFile(f"{outdir}HLTMenu_{args.smk}.json")
            downloader.joboptions_downloader.writeFile(f"{outdir}HLTJO_{args.smk}.json")

    if not args.l1only:
        with MonGroupDownloader(args.dbalias) as downloader:
            downloader.downloadMonGroupsLinkedToSMK(smk=args.smk)
            downloader.writeFile(f"{outdir}MonGroups_smk{args.smk}.json")

    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)