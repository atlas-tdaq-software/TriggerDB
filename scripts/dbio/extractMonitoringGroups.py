#!/usr/bin/env python
"""
Description
-----------
  Download the monitoring group set from the TriggerDB and save to file.

  Takes either the monitoring group key, an SMK or the HLT menu ID as input.

  The files are saved by default to a local directory `./mongroups` as
  `MonGroup_<mgk>.json`
"""

import argparse
import sys
import traceback
from TriggerDB.MonGroupDownloader import MonGroupDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--mongroup", type=int, metavar='mgk',
                        help="The Monitoring groups key for which the file should be downloaded.")
    parser.add_argument("--hltmenu", type=int,
                        help="The HLT menu ID for which the Monitoring Groups file should be downloaded.")
    parser.add_argument("--smk", type=int,
                        help="The SMK for which the Monitoring Groups file should be downloaded.")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3'.")
    parser.add_argument("-o", "--outdir", type=str, default="./mongroups",
                        help="Output directory for downloaded files, e.g. `./mongroups_{smk} (the default is `./mongroups`)")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    count: int = (0 if args.mongroup is None else 1) + (0 if args.hltmenu is None else 1) + (0 if args.smk is None else 1)
    if count != 1:
        log.error(f"Must specify exactly one of monitoring group key, HLT menu ID or SMK, but found {count} matching arguments")
        return 1

    outdir: str = args.outdir.format(**dict(args._get_kwargs()))
    if outdir != "" and not outdir.endswith('/'):
        outdir += "/"

    with MonGroupDownloader(args.dbalias) as downloader:
        # Download monitoring groups based on which id is given and then write to file
        if args.mongroup is not None:
            downloader.download(args.mongroup)
            downloader.writeFile(f"{outdir}MonGroups_{args.mongroup}.json")
        elif args.hltmenu is not None:
            downloader.downloadMonGroupsLinkedToHltMenu(args.hltmenu)
            downloader.writeFile(f"{outdir}MonGroups_hltid{args.hltmenu}.json")
        elif args.smk is not None:
            downloader.downloadMonGroupsLinkedToSMK(args.smk)
            downloader.writeFile(f"{outdir}MonGroups_smk{args.smk}.json")
    
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)