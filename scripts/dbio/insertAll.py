#!/usr/bin/env python
"""
This module contains code to insert a full set of trigger configurations into the ATLAS
TriggerDB.

Contains the definition of functions to parse the JSON files within a folder, and
determine the ATLAS filetype of these files.

WHEN RUN AS MAIN:

Takes two command-line options, `--directory`, specifying the directory containing the
configuration files, and `--dbalias`, specifying the alias for which instance of
TriggerDB to connect to (both required; see
gitlab.cern.ch/atlas-tdaq-software/TriggerDB#database-alias-options
for the available alias options).

Takes several additional optional arguments:

* `--comment`: This allows the user to add a comment with the upload
(to the super-master table and prescale tables).
* `--l1menu`, `--hltmenu`, `--hltjo`, and `--mongroup`: These function as for `insertMenu.py`
and `insertMonitoringGroups.py`, and can be set to 'off' or to a numerical key specifying an 
existing entry. This overrides any file found in the specified directory, e.g. if 
`--l1menu 1` is specified, the existing L1 menu entry 1 will be used rather than any files 
found in the directory. If `--hltmenu off` is specified, any HLT menu, Monitoring Groups or 
prescale files found in the directory will be ignored.

Ties together the functionality of `insertMenu.py` (including call to `insertMonitoringGroups.py`,
and `insertPrescales.py`, uploading the trigger menu files within the directory to 
create a super-master key. If the directory contains any number of prescale sets, all of 
these are uploaded and linked to this super-master key.
"""
import argparse
from collections import defaultdict
import itertools
import json
from pathlib import Path
import sys
import traceback
from typing import Union

from TriggerDB.Logger import log
from TriggerDB.MenuUploader import MenuUploader
from TriggerDB.PrescaleSetUploader import PrescaleSetUploader


def getFilesByType(directory:str) -> dict[str,list[Path]]:
    """Get the filenames of JSON files in a directory.
    
    Parameters:
        directory (str): the directory for which to get filenames

    Returns:
        list[Path]: JSON files in the directory
    """
    if directory is None:
        return {}
    dirpath =  Path(directory)
    if not dirpath.exists():
        raise RuntimeError(f"Directory {dirpath} does not exist")
    if not dirpath.is_dir():
        raise RuntimeError(f"{dirpath} is not a directory")
        
    files_by_type = defaultdict(list)
    # Iterate through the files and subdirectories and find json files
    for item in Path(directory).iterdir():
        if not (item.is_file() and item.suffix==".json"):
            continue
        
        with item.open() as fp:
            data = json.load(fp)
    
        if (ft := data.get("filetype")) is not None:
            files_by_type[ft].append(item)
            
    return dict(files_by_type)

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', "--directory", type=str,
                        help="The directory containing the menu and prescale JSON files to be uploaded. Folder must contain L1, HLT, JO and MonGroup JSON files, unless they are manually specified via --l1menu, --hltmenu, --hltjo, or --mongroup.")
    parser.add_argument("-l1", "--l1menu", type=str,
                        help="L1 menu json file name or an integer specifying an existing L1 menu ID to be linked (required).")
    parser.add_argument("-hlt", "--hltmenu", type=str,
                        help="HLT menu json file, an integer specifying an existing HLT menu ID to be linked, or the string 'off' for an upload without an HLT menu (required).")
    parser.add_argument("-jo", "--hltjo", type=str,
                        help="Job options json file, an integer specifying an existing HLT JO ID to be linked, or the string 'off' for an upload without HLT joboptions (required).")
    parser.add_argument("-mon", "--mongroup", type=str,
                        help="Monitoring groups json file, an integer specifying an existing Monitoring Groups ID to be linked, or the string 'off' for an upload without Monitoring Groups (required).")
    parser.add_argument("-db", "--dbalias", type=str,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDBDEV2_I8'.")
    parser.add_argument("-c", "--comment", type=str, nargs="+",
                        help="Comment to be uploaded with the record (optional)")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    args = parser.parse_args()
    if args.comment is not None:
        args.comment = " ".join(args.comment)
    return args
    return parser.parse_args()

def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)

    files_by_type: dict[str, list[Path]] = getFilesByType(directory=args.directory)
    for k,v in files_by_type.items():
        log.debug(f"{k} ({len(v)}): {[f.name for f in v]}")

    # Check that the directory or command-line arguments result in exactly one L1 menu file
    l1menu: Union[Path,int,None]
    if args.l1menu is None: # get the l1menu from the directory
        if (n_l1:=len(files_by_type.get('l1menu',[]))) != 1:
            raise RuntimeError(f"Expect exactly one L1 menu in the specified directory, but found {n_l1}")
        l1menu = files_by_type['l1menu'][0]
    elif args.l1menu == "off":
        raise RuntimeError("An L1 menu file must be provided.")
    else:
        try:
            l1menu = int(args.l1menu)
        except ValueError:
            l1menu = Path(args.l1menu)
            if not l1menu.exists():
                raise RuntimeError(f"The file {l1menu.as_posix()} does not exist")
    
    # Check that the directory or command-line arguments result in exactly one HLT menu file,
    # or that hltmenu is specifically set to off
    hltmenu: Union[Path,int,None]
    if args.hltmenu is None: # get the hltmenu form the directory
        if (n_hlt:=len(files_by_type.get('hltmenu',[]))) != 1:
            raise RuntimeError(f"Expect exactly one L1 menu in the specified directory, but found {n_hlt}")
        hltmenu = files_by_type['hltmenu'][0]
    elif args.hltmenu == "off":
        hltmenu = None
    else:
        try:
            hltmenu = int(args.hltmenu)
        except ValueError:
            hltmenu = Path(args.hltmenu)
            if not hltmenu.exists():
                raise RuntimeError(f"The file {hltmenu.as_posix()} does not exist")

    # Check that the directory or command-line arguments result in exactly one HLT joboptions file,
    # or that hltjo is specifically set to off
    # There is an exception if a second file is found called HLTJobOptions.db.json,
    # which will be selected over the first joboptions file found
    hltjo: Union[Path,int,None]
    if args.hltjo is None: # get joboptions from directory
        n_jo: int = len(files_by_type.get('joboptions',[]))
        if (n_jo<1) or (n_jo>2):
            raise RuntimeError(f"Expect exactly 1 or 2 HLT joboptions in the specified directory, but found {n_jo}")
        elif n_jo == 2:
            filtered = [file for file in files_by_type['joboptions'] if file.name == "HLTJobOptions.db.json"]
            if len(filtered) == 1:
                hltjo = filtered[0]
            else:
                raise RuntimeError("Found 2 HLT joboptions files in the specified directory, but neither is names HLTJobOptions.db.json")
        else:
            hltjo = files_by_type['joboptions'][0]
    elif args.hltjo == "off":
        hltjo = None
    else:
        try:
            hltjo = int(args.hltjo)
        except ValueError:
            hltjo = Path(args.hltjo)
            if not hltjo.exists():
                raise RuntimeError(f"The file {hltjo.as_posix()} does not exist")

    # Check that the directory or command-line arguments result in exactly one Monitoring Group
    # file, or that mongroup is specifically set to off
    mongroup: Union[Path,None]
    if args.mongroup is None: # get mongroup from directory
        if (n_mg:=len(files_by_type.get('hltmonitoringsummary',[]))) != 1:
            raise RuntimeError(f"Expect one Monitoring Group file in the specified directory but found {n_mg}")
        mongroup = files_by_type['hltmonitoringsummary'][0]
    elif args.mongroup == "off":
        # Check this has not been disabled when a menu is being given
        if hltmenu is not None:
            raise RuntimeError("Monitoring Groups can not be turned off if an HLT menu is being provided. Please retry with monitoring groups enabled")
        mongroup = None
    else:
        mongroup = Path(args.mongroup)
        if not mongroup.exists():
                raise RuntimeError(f"The file {mongroup.as_posix()} does not exist")


    # Upload menus
    with MenuUploader(args.dbalias) as menuUploader:
        smk, upload_record = menuUploader.upload(
            l1menu=l1menu, hltmenu=hltmenu, hltjo=hltjo, mongroup=mongroup, comment=args.comment
        )

        # Upload prescales (same DB connection)
        with PrescaleSetUploader(existingConnection=menuUploader.dbconnection) as prescaleUploader:
            # Create lists to hold the created prescale keys
            l1pskList: list[int] = []
            hltpskList: list[int] = []
            # Upload L1 prescales
            for l1ps_file in files_by_type['l1prescale']:
                prescaleUploader.upload(
                    l1ps=l1ps_file, hltps=None, smk=smk, comment=args.comment)
                l1pskList.append(prescaleUploader.l1_psk)                
            # Upload HLT prescales
            for hltps_file in files_by_type['hltprescale']:
                prescaleUploader.upload(
                    l1ps=None, hltps=hltps_file, smk=smk, comment=args.comment)
                hltpskList.append(prescaleUploader.hlt_psk)

        # Print table with summary of keys that have been uploaded/linked
        print("Key Summary:")
        print(" SMK | L1PSK | HLTPSK | Comment")
        print("-------------------------------")
        for (l1_psk, hlt_psk) in itertools.zip_longest(l1pskList, hltpskList, fillvalue=None):
            # Create a new row for each set of prescale keys
            l1str: str = "       " if l1_psk is None else f"{l1_psk:6} "
            hltstr: str = "        " if hlt_psk is None else f"{hlt_psk:7} "
            print(f"{smk:4} |{l1str}|{hltstr}| {args.comment}")
    return 0

if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)
