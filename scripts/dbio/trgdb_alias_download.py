#!/usr/bin/env python
"""
Description
-----------
  Download alias definitions for a given SMK from the TriggerDB and save to files. Optionally the prescale sets can also be downloaded.

  The files are saved by default to a local directory `./aliases_<dbalias>_smk<smk>/`.
"""

import argparse
import sys
import traceback

from TriggerDB.AliasDownloader import AliasDownloader
from TriggerDB.Logger import log

def cmdl() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=__doc__
        )
    parser.add_argument("--smk", type=int, required=True,
                        help="The SMK for which the alias files should be downloaded (required).")
    parser.add_argument("--download-prescales", action='store_true',
                        help="Also download the prescale files.")
    parser.add_argument("-db", "--dbalias", type=str, required=True,
                        help="Trigger DB connection alias, e.g. 'TRIGGERDB_RUN3' (required).")
    parser.add_argument("-o", "--outdir", type=str, default="./aliases_{dbalias}_smk{smk}",
                        help="Output directory for downloaded files. Default './aliases_{dbalias}_smk{smk}'")
    parser.add_argument("-l", "--loglevel",  dest="loglevel", choices=['DEBUG', 'INFO', 'WARNING'],
                        help="Log level (default INFO)", default = "INFO")
    return parser.parse_args()


def main() -> int:
    args = cmdl()
    log.setLevel(args.loglevel)
    outdir: str = args.outdir.format(smk=args.smk, dbalias=args.dbalias)
    if outdir != "" and not outdir.endswith('/'):
        outdir += "/"

    with AliasDownloader(args.dbalias) as downloader:
        downloader.downloadForSMK(smk=args.smk, download_prescales=args.download_prescales)
        downloader.writeFiles(outdir=outdir)
    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as exc:
        log.error(f"Exception {type(exc).__qualname__} caught: {exc}")
        print(traceback.format_exc())
        sys.exit(1)