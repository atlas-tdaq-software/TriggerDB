#!/usr/bin/env python
"""
This module contains code to generate a bunch group set JSON file.

Can be created from a provided csv file or directly from the LHC

WHEN RUN AS MAIN:

The two arguments define usage:
* `--fromLHC` to read directly from the LHC information in IS, otherwise reads from file
* `--name` used to set the JSON name property and filename

To generate from csv file `runReadFileBunchGroup.sh <fillingscheme>.csv` must be used

Bunch group set JSON file is saved to the local directory ready to be uploaded,
printout summary is provided in the terminal.
"""

from TriggerDB.BunchGroupSetUploader import BunchGroupSetUploader

import filecmp
import logging
import argparse
import os
import pwd
import re
import shutil
import stat
import textwrap
import time
import sys
import json
from itertools import groupby
from datetime import datetime
from ipc import IPCPartition # type: ignore
from ispy import ISObject # type: ignore
import ers # type: ignore

from TriggerDB.BunchGroupSetUploader import BunchGroupSetUploader


# code based on:
# https://gitlab.cern.ch/atlas-tdaq-software/BunchGroupUpdate/-/blob/master/java/bunchgroupupdate/BunchGroupSetUploadCmd.java
# https://gitlab.cern.ch/atlas-tdaq-software/BunchGroupUpdate/-/blob/master/java/bunchgroupupdate/BGHelper.java
# https://gitlab.cern.ch/atlas/athena/-/blob/master/Control/AthenaConfiguration/python/AutoConfigOnlineRecoFlags.py
# https://gitlab.cern.ch/atlas/athena/-/blob/master/Trigger/TriggerCommon/TriggerMenuMT/python/L1/Base/BunchGroupSet.py


class ErsException( ers.Issue ):
    def __init__( self, msg, args, cause = None):
        ers.Issue.__init__( self, msg, args, cause )

class MissingArgument( ErsException ):
    def __init__( self, details = "" ):
        super().__init__(
            f"Missing argument in the commandline: {details}", args = {"details" : details}
            )

class WrongArgument( ErsException ):
    def __init__( self, details = "" ):
        super().__init__(
            f"Wrong argument in the commandline: {details}", args = {"details" : details}
            )

class CantOpenFile( ErsException ):
    def __init__( self, fname, err, cause = None ):
        super().__init__(
            f'Can not open {fname} file, error = {err}', { 'filename' : fname, 'error' : err }, cause
            )

class FileCopyError( ErsException ):
    def __init__( self, src, dest, err, cause = None ):
        super().__init__(
            f'Can not copy file {src} to {dest}, error = {err}', { 'source' : src, 'destination': dest, 'error' : err }, cause
            )

class FileExistsAndIsDifferentError( ErsException ):
    def __init__( self, src, destdir, msg):
        super().__init__(
            msg=f'File {src} exists in {destdir} but the content is different. {msg}', args={ 'source' : src, 'destination': destdir, 'msg' : msg }, cause=None
            )


class ISAccessException( ErsException ):
    def __init__( self, exc, action, cause = None ):
        super().__init__(
            f'An IS error occoured: {exc}. {action}', { 'action' : action, 'exc' : exc }, cause
        )

class MissingPartition( ErsException ):
    def __init__( self, partition, cause = None ):
        super().__init__(
            f'Partition {partition} not found', { 'partition' : partition }, cause
        )

#create the logger
log = logging.getLogger("ReadBunchGroup.py")
log.setLevel(logging.INFO)

#add ers handler
ers.addLoggingHandler("ReadBunchGroup.py")

def normalize(bcids):
    """ turn list of bunches into trains """
    normalized = []
    bunches = sorted(list(set(bcids))) # uniq and sort
    if any(map(lambda bcid : bcid<0 or bcid >= 3564, bunches)):
        raise RuntimeError("Found bcid outside range 0..3563 in bunchgroup")
    for k,g in groupby(enumerate(bunches), lambda x : x[1]-x[0]):
        train = list(g)
        normalized += [ (train[0][1], len(train)) ]
    return normalized

def bgjson(name, idnum, bcids):
    """ create json blob for a bunch group """
    trains=normalize(bcids)
    # remove _FS for when reading File Based Groups
    if name.endswith("_FS"):
        name = name.rpartition("_FS")[0]
    if name.endswith("_FS_json"):
        name = name.rpartition("_FS_json")[0]
    bg = {
        "name" : name,
        "id"   : idnum,
        "info" : "%i bunches, %i groups" % (len(bcids), len(trains)),
        "bcids": [ {"first": first, "length": length} for (first,length) in trains]
    }
    return bg

def printBgsSummary(bgsetName, bgset):
    # display top level summary of bunch group
    print(f"Bunchgroup set {bgsetName}")
    for bg in bgset.values():
        print("%2s - %s: %s" % (bg["id"], bg["name"], bg["info"]))

def writeJSON(fullBBset, outputFile, destdir="./", pretty=True):
    # write json file
    outputFile = destdir.rstrip('/') + '/' + outputFile
    with open( outputFile, mode="wt" ) as fh:
        json.dump(fullBBset, fh, indent = 4 if pretty else None, separators=(",", ": "))
    log.info("Wrote bunchgroup to %s", outputFile)
    return outputFile

def readISObject(part_name, obj_name, type_name):
    """ read object from IS """
    partition = IPCPartition(part_name)
    if not partition.isValid():
        err = MissingPartition(partition=part_name)
        log.error(err)
        raise err

    try:
        obj = ISObject(partition, obj_name, type_name)
        obj.checkout()
        return obj
    except ErsException as exc:
        err = ISAccessException(exc, "Abort")
        # print('Cannot read %s from partition %s, exception: %s' % (obj_name, part_name, e))
        raise err

def buildBgsetName(bgset):
    """
    Build the name of the bunchgroup set based on the Paired bunches (BGRP1)

    The name contains the number of colliding bunches, trains and indivual 
    colliding bunches and the ending '_hybrid' if the 8b4e pattern is found.
    It has the folowing form:
    
    'LHC_<#bunches>bunches_<#trains>trains_<#indivs>indivs_hybrid'

    Note that the timestamp is not used, as the bunchgroup name is used in the hash-value that
    determines the uniqueness of a bunchgroup set
    """
    bg1 = bgset["BGRP1"]["bcids"]
    bgUnpaired1 = bgset["BGRP13"]["bcids"]
    bgUnpaired2 = bgset["BGRP14"]["bcids"]

    nBgUnpaired1 = sum([train['length'] for train in bgUnpaired1])
    nBgUnpaired2 = sum([train['length'] for train in bgUnpaired2])

    bg1Str = 3564*['0']
    for train in bg1:
        for bcid in range(train['first'], train['first']+train['length']):
            bg1Str[bcid]='1'
    bg1Str = ''.join(bg1Str)

    nCollidingBunches = bg1Str.count('1')
    nFilledBunches = nCollidingBunches + max(nBgUnpaired1, nBgUnpaired2)

    # for trains count the occurances of 5x to 10x 8b4e, than 3-8 0's, the 2-5 trains of length 32-40
    nTrainsHybrid = len(re.findall("0(?:111111110000){5,10}0{3,8}(?:1{32,40}0{4,16}){2,5}", bg1Str))
    nIndivs = bg1Str.count('010')
    if nTrainsHybrid>0:
        bgsName = f"LHC_{nFilledBunches}b_{nCollidingBunches}_{nTrainsHybrid}trains_{nIndivs}indivs_hybrid"
    else:
        nTrains = len(re.findall("0*1{12,48}0{10}", bg1Str))
        bgsName = f"LHC_{nFilledBunches}b_{nCollidingBunches}_{nTrains}trains_{nIndivs}indivs"
    return bgsName

def getISObjectNameDefaults(target):
    # use the default name if targer lhc, csv, or json is specified, otherwise return the specified target
    objectNameDefaults = {
        "lhc": "BunchGrouperApp/BunchGroups",
        "csv": "BunchGrouperApp_FS/BunchGroups_FS",
        "json": "BunchGrouperApp_FS_json/BunchGroups_FS_json"
    }
    return objectNameDefaults[target] if target in objectNameDefaults else target

def getBunchGroupFromIS(partition, objectNameSpec):
    """
    Reads bunchgroup definition from IS where it is written by the CTP
    Input
    -----
    partion: str partition name
    objectName: str name of IS object, either "lhc", "csv", "json", or an explicit object name like "BunchGrouperApp/BunchGroups" (LHC info)

    Returns
    -------
    bgsName: str name of the bunchgroup determined from fill pattern
    bgset: dictionary of bunchgroup defintions
    modificatinTime: datetime time when information was written to IS by CTP
    hash: int hash of the serialized bgset
    """
    def getBunchgroupSetHash(bgset):
        def serial(obj):
            if type(obj) == dict:
                return tuple([(k,serial(v)) for k,v in obj.items()])
            elif type(obj) == list:
                return tuple([serial(v) for v in obj])
            else:
                return obj
        return hash(serial(bgset))

    serverName = "Monitoring" # default server, will not change
    objectName = getISObjectNameDefaults(objectNameSpec)
    log.info(f"Reading TrigConfL1BunchGroups from {serverName}.{objectName} in partition {partition}")
    try:
        isinfo = readISObject(partition, serverName + "." + objectName, 'TrigConfL1BunchGroups')
        x = getattr(isinfo, f"BCID_Group_1")
        ts = datetime.fromtimestamp(isinfo.time().c_time())
        log.info(f"Got {len(x)} paired bunches, updated on {ts}")
    except ers.Issue as exc:
        err = ISAccessException(exc, "Will abort.")
        log.error(f"{err}")
        raise err

    modificationTime = datetime.fromtimestamp(isinfo.time().c_time()) # the time the IS object was updated

    bgset = {}
    for bgid in range(16):
        bgset[f'BGRP{bgid}'] = bgjson(getattr(isinfo, f"Name_Group_{bgid}"), bgid, getattr(isinfo, f"BCID_Group_{bgid}"))

    bgsetName = buildBgsetName(bgset)
    bgsetHash = getBunchgroupSetHash(bgset)

    return bgsetName, bgset, modificationTime, bgsetHash

def getBunchGroupFromFile(filename, ctpDirectory, partition, maxWaitSeconds = 300, singleRead=True):
    """
    This function steers the creation of a bunchgroup set from a fillingscheme.csv/.json file. The actual transformation of
    the LHC fill pattern into a bunchgroup set is done by the CTP, which has implemented the rules of bunchgroup creation.
    The IS object that the CTP uses for publishing the bunchgroup it creates is named "BunchGrouperApp_FS/BunchGroups_FS"

    In detail this function does
    1) get the current bunchgroup set suggestion from IS as a reference to understand when the new file was picked up
    2) copy the .csv or .json file to the location the CTP expects it 
    3) check for @maxWaitSeconds (once every 5 seconds) if the bunchgroup was updated (using the timestamp of the IS publication)
    4) return the name of the bunchgroup set and the bunchgroup set

    Uploading the bunchgroup set to the database or writing it as a json file happens afterwards outside this function

    Input
    -----
    filename: str name of the csv or json file with the filling scheme
    ctpDirectory: str directory where the CTP listens to newly appearing filling schemes to turn them into a bunchgroup set
    partition: str name of the partition where the CTP publishes bunchgroup sets in IS (at P1 always initialL1CT)
    objectName: str name of IS object, either BunchGrouperApp/BunchGroups (LHC info)
                    or BunchGrouperApp_FS/BunchGroups_FS (from filling scheme)

    Returns
    -------
    bgsName: str name of the bunchgroup determined from fill pattern
    bgset: dictionary of bunchgroup defintions
    """

    # check input file existence
    if not os.path.exists(filename):
        err = CantOpenFile(filename, "Input file does not exist")
        log.error(err)
        raise err

    # before copying the file, check if it exists and is identical
    baseName = os.path.basename(os.path.realpath(filename))
    targetFilename = os.path.join(ctpDirectory, baseName)
    if os.path.isfile(targetFilename):
        # file exists in ctp archive directory, now check if it is identical
        targetIsEqual = filecmp.cmp(filename,targetFilename, shallow=False)
        if targetIsEqual:
            fstat = os.stat(targetFilename)
            creatorName = pwd.getpwuid(fstat.st_uid).pw_gecos
            creationTime = time.gmtime(fstat.st_ctime)
            creationTime = time.strftime("on %d %b %Y at %T", creationTime)
            log.info(f"File {baseName} with identical content already exists in {ctpDirectory}. It was created by {creatorName} {creationTime}.")
        else:
            raise FileExistsAndIsDifferentError(src=filename, destdir=ctpDirectory, msg="Please investigate and if understood delete the old file and run again.")
    else:
        # file does not exist in ctp archive directory
        # copy file to ctp directory for archiving (the source can be a symlink, the actual file will be copied)
        log.info(f"Copying file {filename} to {targetFilename}")
        try:
            shutil.copyfile(filename, targetFilename, follow_symlinks=True)
            os.chmod(targetFilename,stat.S_IREAD|stat.S_IWRITE|stat.S_IRGRP|stat.S_IWGRP|stat.S_IROTH|stat.S_IWOTH) # making sure that 
        except Exception as exc:
            raise FileCopyError(filename, targetFilename, exc)


    # create symlink fillingscheme.(csv|json) for the CTP bunchgrouper to start
    fnEnding = baseName.rsplit('.')[-1]
    linkName = os.path.join(ctpDirectory, f"fillingscheme.{fnEnding}")
    if os.path.exists(linkName):
        os.remove(linkName)
    timeOfFileLink = datetime.now()
    timOfFirstUpdate = None
    log.info(f"Creating symlink {linkName} to {baseName} in {ctpDirectory} at {timeOfFileLink}.")
    os.symlink(baseName, linkName)

    objectName = getISObjectNameDefaults(fnEnding)
    log.info(f"Waiting for update of {partition} Monitoring.{objectName} for the next {maxWaitSeconds} seconds." )

    # keep looping and check the timestamp of the IS publication every two second
    while True:
        time.sleep(2)
        bgsetName, bgset, modificationTime, _ = getBunchGroupFromIS(partition, objectNameSpec=fnEnding)

        # wasUpdated = bgsetHash != bgsetHashBeforeFileLinkUpdate
        wasUpdated = ( modificationTime > timeOfFileLink )
        wasUpdatedSecondTime = timOfFirstUpdate is not None and ( modificationTime > timOfFirstUpdate )

        if wasUpdated and timOfFirstUpdate is None:
            # first update
            timOfFirstUpdate = modificationTime
            log.info(f"Detected a new bunchgroup entry in IS at {modificationTime}")
            if singleRead:
                log.info("single-read was specified, will use this publication")
                break
            else:
                log.info("single-read was not specified, will wait for second publication")

        if wasUpdatedSecondTime:
            # second update and following ones
            timOfFirstUpdate = modificationTime
            log.info(f"Detected a second new bunchgroup entry in IS at {modificationTime}")
            break
        
        if (datetime.now()-timeOfFileLink).seconds >= maxWaitSeconds:
            log.error(f"No new bunchgroup appeared in {partition} Monitoring.{objectName} after {maxWaitSeconds} seconds.")
            log.info("This should not be the case and needds to be reported to the experts.")
            raise RuntimeError(f"No new bunchgroup appeared in {partition} Monitoring.{objectName} after {maxWaitSeconds} seconds.")
        
    return bgsetName, bgset
    
def getCurrentBunchgroupKeyFromIS(partition):
    # read current bunchgroup key from IS server RunParams (by default in the ATLAS partition)

    if partition is None:
        partition='ATLAS'

    try:
        bgk_is = readISObject(partition, 'RunParams.TrigConfL1BgKey', 'TrigConfL1BgKey')
    except ers.Issue as exc:
        log.warning(f"{exc}. Returning 0.")
        return 0
        
    if bgk_is is None:
        log.warning(f"Unable to find the current bunchgroup key as partition {partition} or object 'RunParams.TrigConfL1BgKey' is not available. Returning 0.")
        return 0

    try:
        bgk = bgk_is.getAttributeValue('L1BunchGroupKey')
    except ErsException as e:
        log.warning(f"Cannot access 'L1BunchGroupKey' from 'RunParams.TrigConfL1BgKey' in partition {partition}. Returning 0.")
        return 0

    return bgk

def parseArguments():
    """
    Define command-line arguments
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description = textwrap.dedent(f"""
            Turn LHC fill information into a bunchgroup and upload to the Trigger DB.
            These are the two common use cases for creating a bunchgroup key in the Trigger DB:

                1) {os.path.basename(sys.argv[0])} --fromFile --file <fillingscheme.json|.csv> --upload
                2) {os.path.basename(sys.argv[0])} --fromLHC --upload
        """))

    parser.add_argument(
        '-v', '--version', action="store_true", default=False, help="Print the version"
    )
    parser.add_argument(
        '--bgsname', dest="bgsName", default=None,
        help=(
            "Name of the bunchgroup set (optional). For normal operation this should not be given and the default should be used"
            "By default a name of the form 'LHC_<nBunches>b_<nCollBunches>_10<nTrains>trains_<nIndivs>indivs(_<hybrid>)' is composed from the filled bunch pattern."
        )
    )

    args_fromLhc = parser.add_argument_group("Input from current LHC fill")
    args_fromLhc.add_argument(
        "--fromLHC", action="store_true", dest="fromLHC", default=False,
        help="Read from IS object containing bunch groups based on LHC fill."
    )    
    args_fromLhc.add_argument(
        '--compare', dest="compare", default=False, action="store_true",
        help="Compare bunchgroup set suggestion based on LHC with currently loaded."
    )

    args_fromFile = parser.add_argument_group("Input from filling scheme file")
    args_fromFile.add_argument(
        '--fromFile', action="store_true", dest="fromFile", default=False,
        help="Read from IS object containing bunch groups based on a file scheme file."
    )
    args_fromFile.add_argument(
        '-f', '--file', dest="fileName", default=None,
        help="Read from <fillingScheme>.csv or <fillingScheme>.json file (via IS), the filename should contain the filling scheme. Must also specify --fromFile."
    )

    args_output = parser.add_argument_group("Output")
    args_output.add_argument(
        '-w', '--write-json', action="store_true", dest="writeToJson", default=False,
        help="Write bunchgroup set to json file (default is False)."
    )
    args_output.add_argument(
        '-u', '--upload', action="store_true", dest="upload", default=False,
        help="Upload bunchgroup set to TriggerDB (default is False)."
    )
    args_output.add_argument(
        '--db', dest="dbalias", default="TRIGGERDB_RUN3",
        help="Trigger DB alias (default TRIGGERDB_RUN3)."
    )
    args_output.add_argument(
        '-c', '--comment', dest="comment", default="%FN% uploaded on %D% at %T%",
        help=(
            "Comment when uploading to the trigger database. Placeholders "
            "%%FN%% (filename), %%N%% (bgs name), %%D%% (upload date), %%T%% (upload time), %%U%% (upload user) are available "
            "(default is '%%FN%%/%%N%% uploaded on %%D%% at %%T%%')."
        )
    )
    
    args_testing = parser.add_argument_group("Testing")
    args_testing.add_argument(
        '-p', dest="testPartition", default=None,
        help="test partition name, should be changed only for testing purposes."
    )
    args_testing.add_argument(
        '--ctp-dir', dest="testCtpDirectory", default=None,
        help="test directory for the filling scheme file, should be changed only for testing purposes."
    )

    cmdline = parser.parse_args()
    if cmdline.version:
        return cmdline

    if cmdline.fileName is not None:
        cmdline.fromFile = True

    if not cmdline.fromFile and not cmdline.fromLHC:
        parser.print_help()
        err = MissingArgument("Need to specify --fromLHC or --fromFile")
        raise err

    if cmdline.fromFile and not cmdline.fileName:
        parser.print_help()
        err = MissingArgument("When specifying --fromFile a file name must be provided with --file ")
        raise err

    if cmdline.fromFile and cmdline.fromLHC:
        parser.print_help()
        raise MissingArgument("Please specify only one of the options --fromFile or --fromLHC")

    if cmdline.fileName is not None:
        fnEnding = cmdline.fileName.rsplit('.')[-1]
        if fnEnding not in ['csv', 'json']:
            parser.print_help()
            raise WrongArgument("When uploading a filling scheme file the --fileName argument must end in '.csv' or '.json'")
    return cmdline

def main() -> int:
    
    cmdline = parseArguments()
    if cmdline.version:
        version = "1.0, Sat August 19 2023 (includes reading from json files)"
        print(f"Version: {version}")
        return 0

    # defaults for operation at P1, where test parameters should not be specified
    bgsPartition = 'initialL1CT' # default partition where to find the bunchgroup set
    bgkPartition = 'ATLAS' # default partition where to find the current bunchgroup key

    ctpDir = '/det/ctp/fillingschemes/' # default location for the fillling schemes for the CTP bunchgrouper app

    # for testing purpose one can override the partition and the CTP directory (see README)
    if cmdline.testPartition is not None:
        bgsPartition = cmdline.testPartition
        bgkPartition = cmdline.testPartition
    if cmdline.testCtpDirectory is not None:
        ctpDir = cmdline.testCtpDirectory
        if not os.path.exists(ctpDir):
            log.info(f"Creating directory {ctpDir}")
            os.makedirs(ctpDir, exist_ok=True)

    if cmdline.fromLHC:
        # read LHC filling scheme
        bgsName, bgset, _, _ = getBunchGroupFromIS(partition=bgsPartition, objectNameSpec="lhc")

    elif cmdline.fromFile:
        # read from File (via IS)
        bgsName, bgset = getBunchGroupFromFile(filename=cmdline.fileName, ctpDirectory=ctpDir, partition=bgsPartition)
    else:
        return 0

    # the name of the bunchgroup set is determined in the IS access functions, 
    # but can be overwritten from the command line
    if cmdline.bgsName is not None:
        bgsName = cmdline.bgsName

    if len(bgsName)>50:
        bgsName = bgsName[0:50]
        log.warning("Original BG name too long for DB, truncated to 50 characters: %s", bgsName)

    printBgsSummary(bgsName, bgset)

    fullBGset = {
        "name": bgsName,
        "filetype": "bunchgroupset",
        "bunchGroups": bgset
    }

    # write json bunchgroup file if requested
    if cmdline.writeToJson:
        fileName = f"BunchGroupSet_{bgsName}.json"
        writeJSON(fullBGset, fileName)
        log.info("To upload the bunchgroup set  to the Trigger database use the option '--upload'. Alternatively, to manually upload run this command: insertBunchGroupSet.py --dbalias <DB alias> --bgs %s", fileName)

    # upload json bunchgroup structure to database if requested
    if cmdline.upload:
        print(f"Going to upload the bunchgroup set to the database {cmdline.dbalias}")
        with BunchGroupSetUploader(cmdline.dbalias) as uploader:
            # Upload bunch group set
            bgsk, uploadRecord = uploader.upload(bgset=fullBGset, comment=cmdline.comment)

        currentBGKey = getCurrentBunchgroupKeyFromIS(partition=bgkPartition)

        # ATTENTION: do not change the following output lines as they are interpreted by the P1 bunch group upload panel
        print("======")
        print(f"= bgs key: {bgsk}")
        print(f"= bgs name: {uploadRecord['dbrecord']['NAME']}")
        print(f"= bgs comment: {uploadRecord['dbrecord']['COMMENT']}")
        print(f"= upload time: {uploadRecord['dbrecord']['MODIFIED_TIME']}")
        print(f"= history: {uploadRecord['history']}")
        print("------")
        print(f"= active bgs key: {currentBGKey}")
        print("======")

    # compare bunchgroup set from LHC fill pattern with the one that is loaded
    if cmdline.fromLHC and cmdline.compare:
        # work in progress
        # still needed is getting the BGS from the databbase and comparing with what we get from IS
        # log.info(f"Current bunchgroup set key is {currentBGKey}")
        log.info("This feature is work in progress, the goal is that --compare tells you how the current fill is different from the bunchgroup that is loaded.")

    return 0

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        log.error("Exiting with code 1, due to the following exception")
        ers.error(e)
        sys.exit(1)
    sys.exit(0)
