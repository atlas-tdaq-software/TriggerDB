if test ! $1; then
    echo "Argument missing. Usage:"
    echo "$0 <fillPattern.csv>"
    exit
fi

fillPattern=$1

if test ! -f $fillPattern; then
    echo "Can't find file $fillPattern"
    exit
fi

# copy filling scheme file to fillingschemes for archiving
cp $fillPattern /det/ctp/fillingschemes/
chmod a+w /det/ctp/fillingschemes/`basename $fillPattern`

# link fillingscheme.csv (this is used by the BunchGrouper_FS)
ln -sf `basename $fillPattern` /det/ctp/fillingschemes/fillingscheme.csv

# name of bunch group set
bgsName=LHCFS_`readlink /det/ctp/fillingschemes/fillingscheme.csv | xargs -I % basename % .csv`

# the BunchGrouper needs some time before picking up the change
echo $bgsName
echo ""
echo "Did create link to "`basename $1`
echo "Will need to wait (80s max) for the BunchGrouper to update IS,"
echo "then please check if the bunch groups are consistent with what you expect"

# upload a new BunchGroupSet 
ReadBunchGroup.py --name $bgsName
