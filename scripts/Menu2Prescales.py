#!/usr/bin/env python

import sys
import json
from collections import OrderedDict as odict


def createL1Prescales(data):
    pso = odict()
    pso['filetype'] = 'l1prescale'
    pso['name'] = data['name']
    pso['cutValues'] = odict()
    ps = pso['cutValues']
    for name,item in sorted(data['items'].items()):
        ps[name] = odict([
            ("cut", 1),
            ("enabled", True),
            ("info", "prescale: 1")
        ])

    return pso


def createHLTPrescales(data):
    pso = odict()
    pso['filetype'] = 'hltprescale'
    pso['name'] = data['name']
    pso['prescales'] = odict()
    ps = pso['prescales']
    for name,ch in sorted(data['chains'].items()):
        ps[name] = odict([
            ("name", name),
            ("hash", ch['nameHash']),
            ("prescale", 1),
            ("enabled", True)
        ])
        if "express" in ch['streams']:
            ps[name]['prescale_express'] = 1
            ps[name]['enabled_express'] = True
    return pso

def main():
    if len( sys.argv ) <= 1:
        print("Need to specify L1 or HLT menu json file as argument, e.g.")
        print(sys.argv[0],"HLTMenu.json")
        return 1


    menuFN = sys.argv[1]
    psFN = menuFN.replace('Menu','Prescale')
    if menuFN == psFN:
        print("Could not come up with a proper filename")
        return 1

    pso = None
    with open(sys.argv[1],'r') as fh:
        data = json.load(fh)
        if data['filetype'] == 'hltmenu':
            pso = createHLTPrescales(data)
        elif data['filetype'] == 'l1menu':
            pso = createL1Prescales(data)
        
    if pso:
        with open(psFN, 'w') as outfile:
            print("Wrote prescales into:",psFN)
            json.dump(pso, outfile, indent = 4)
    
    return 0

if __name__ == "__main__":
    sys.exit(main())
