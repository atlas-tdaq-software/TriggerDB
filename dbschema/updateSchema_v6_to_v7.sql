CREATE TABLE HLT_MONITORING_GROUPS (
    HMG_ID                  NUMBER(10),
    HMG_NAME                VARCHAR2(65),
    HMG_DATA                BLOB,
    HMG_HASH                VARCHAR2(50),
    HMG_VERSION             NUMBER(11),
    HMG_COMMENT             VARCHAR2(200),
    HMG_HLT_MENU_ID         NUMBER(10),
    HMG_USERNAME            VARCHAR2(50),
    HMG_MODIFIED_TIME       TIMESTAMP, 
    HMG_HIDDEN              NUMBER(1),    
    HMG_IN_USE              NUMBER(1),
    CONSTRAINT              HMG_PK              PRIMARY KEY (HMG_ID),
    CONSTRAINT              HMG_NMVER           UNIQUE (HMG_NAME, HMG_VERSION),
    CONSTRAINT              HMG_ID_NN           CHECK ( HMG_ID IS NOT NULL),
    CONSTRAINT              HMG_NAME_NN         CHECK ( HMG_NAME IS NOT NULL),
    CONSTRAINT              HMG_DATA_NN         CHECK ( HMG_DATA IS NOT NULL),
    CONSTRAINT              HMG_VERSION_NN      CHECK ( HMG_VERSION IS NOT NULL),
    CONSTRAINT              HMG_HLT_MENU_ID_NN  CHECK ( HMG_HLT_MENU_ID IS NOT NULL),
    CONSTRAINT              HMG_FK_HMT          FOREIGN KEY (HMG_HLT_MENU_ID)
                                                REFERENCES HLT_MENU(HTM_ID),
    CONSTRAINT              HMG_DATA_IS_JSON    CHECK (HMG_DATA IS JSON (STRICT))
)
LOB(HMG_DATA) STORE AS SECUREFILE hlt_monitoring_groups_lob(
    DEDUPLICATE
    COMPRESS HIGH
    CACHE
    NOLOGGING
);

GRANT SELECT ON HLT_MONITORING_GROUPS TO ATLAS_CONFTRIG_READ_ROLE;

UPDATE trigger_schema 
SET ts_tag = 'Trigger-Run3-Schema-v7', ts_modified_time = CURRENT_TIMESTAMP 
WHERE rowid IN (select MAX(rowid) FROM trigger_schema);

